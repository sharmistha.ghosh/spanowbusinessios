import axios from 'axios';
import base from '../Api/config';
import AsyncStorage from "@react-native-community/async-storage";


let headers = {
'Accept': 'application/json',
// 'Content-Type': 'application/json',
}

const resolver = () => AsyncStorage.getItem('userToken', (err, result) => {
//console.log("resolve method in api page");

    if (result) {
    result = JSON.parse(result);
    //headers.Authorization = 'Bearer '+result.id
    }

})


class Api {

static getApi(endPoint) {
    return new Promise((resolve, reject) => {
        resolver().then(() => {
            fetch(base.baseUrl + endPoint, {
                method: 'GET',
                headers: headers
            }).then(response => {
                if (response.status === 200) {
                    resolve(response.json());
                } else {
                    if (response.status === 204) {
                        resolve();
                    } else {
                        reject({ "err": "401 found" })
                    }
                }
            }).catch(error => error)
        }).catch(error => error)
    })
}




static postApi(endPoint, data) {
     //console.log("post data :", data);
     //console.log("endPoint :", endPoint);
    return new Promise((resolve, reject) => {
        resolver().then(() => {
            console.log(base.baseUrl + endPoint);
            fetch(base.baseUrl + endPoint, {
                method: 'POST',
                headers: headers,
                body: data
            }).then(response => {
                console.log("response in post api in api page",response);
                
            if (response.status === 200) {
                resolve(response.json());
            } else {
            if (response.status === 204) {
                resolve();
            } else {
                reject({ "err": "401 found" })
            }
            }
            }).catch(error => {
            console.log("api post catch block with error msg" + error)
            })
        }).catch(error => error)
    })
};



static country_getApi(endPoint) {

    return new Promise((resolve, reject) => {
    resolver().then(() => {
    axios.get(base.baseUrl).then((res) => {
    // console.log(res);
    console.log(base.countryUrl + endPoint);
    fetch(base.countryUrl + endPoint, {
    method: 'GET',
    headers: headers
    }).then(response => {
    if (response.status === 200) {
    resolve(response.json());
    } else {
    if (response.status === 204) {
    resolve();
    } else {
    reject({ "err": "401 found" })
    }
    }
    }).catch(error => error)

    }).catch(error => error)
    }).catch(error => error)
    });
}



static city_getApi(endPoint) {

    return new Promise((resolve, reject) => {
    resolver().then(() => {
    axios.get(base.baseUrl).then((res) => {
    // console.log(res);
    console.log(base.cityUrl + endPoint);
    fetch(base.cityUrl + endPoint, {
    method: 'GET',
    headers: headers
    }).then(response => {
    if (response.status === 200) {
    resolve(response.json());
    } else {
    if (response.status === 204) {
    resolve();
    } else {
    reject({ "err": "401 found" })
    }
    }
    }).catch(error => error)

    }).catch(error => error)
    }).catch(error => error)
    })

}

}

export default Api