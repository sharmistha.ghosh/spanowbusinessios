import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import {Avatar, Icon, CheckBox} from 'react-native-elements';
import styles from './style';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import {connect} from 'react-redux';
import {
  spaGallaryFetchImages,
  spaGallaryDeleteImages,
} from './redux/SpaOwnerAction';

class gallery extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      spa_owner_id: '',
      status: '',
      visible1: false,
      popupMsg: '',
      spinner: true,
      deleteImageId: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps', nextProps);
    this.setState({spinner: false});
  }

  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      console.log('focus', this.state.spa_owner_id);
      if (this.state.spa_owner_id) {
        console.log('focused working');
        let userDetails = new FormData();
        userDetails.append('spa_owner_id', this.state.spa_owner_id);
        console.log("userDetails",userDetails);
        this.props.spaGallaryFetchImages(userDetails);
        console.log('in focus');
      }
    });
    try {
      const value = await AsyncStorage.getItem('login_id');
      console.log('loginId in myaccount page', value);
      if (value) {
        this.setState({spa_owner_id: value});
      }
    } catch (error) {
      console.log(error);
    }
    let userDetails = new FormData();
    userDetails.append('spa_owner_id', this.state.spa_owner_id);
    console.log("userDetails",userDetails);
    this.props.spaGallaryFetchImages(userDetails);
  }

  componentDidUpdate(prevProps) {
    console.log('componentDidUpdate visible', this.state.visible1);
    if (prevProps.deleteGalleryImage !== this.props.deleteGalleryImage) {
      console.log('change------------');
      this.setState({spinner: false})
      const addDetails = this.props.deleteGalleryImage;
      if (this.props.error && this.props.error !== undefined) {
        // if (this.state.visible1 === false) {
        //   this.setState({visible1: true, popupMsg: addDetails.msg});
        // }
        // this.setState({spinner: false});
        Alert.alert('Something went wrong. Please try again later.');
      } else {
        console.log('popup------');

        if (addDetails.Ack === 1) {
          // Alert.alert('done');
          let userDetails = new FormData();
          userDetails.append('spa_owner_id', this.state.spa_owner_id);
          this.props.spaGallaryFetchImages(userDetails);
          // this.setState({
          //   status: 1,
          // });
          // if (this.state.visible1 === false) {
          //   console.log("open");

          //   this.setState({
          //     visible1: true,
          //     popupMsg: addDetails.msg,
          //   });
          // }
        } else {
          // this.setState({
          //   spinner: false,
          // });
          // if (this.state.visible1 === false) {
          //   this.setState({
          //     visible1: true,
          //     popupMsg: 'Something went wrong. Please try again later.',
          //   });
          // }
          Alert.alert('Something went wrong. Please try again later.');
        }
      }
    }
  }

  imageDelete = arrayKey => {
    console.log('deleted image key', arrayKey);

    this.setState({
      deleteImageId: this.props.spaImageList.spaGallaryImageIds[arrayKey],
      visible1: true,
      popupMsg: 'Are you sure you want to delete the image from spa gallery',
    });
  };

  closePopupbox = () => {
    this.setState({
      visible1: false,
      spinner: true,
    });
    let userDetails = new FormData();
    userDetails.append('deleted_image_id', this.state.deleteImageId);
    console.log('deleted image id', userDetails);
    this.props.spaGallaryDeleteImages(userDetails);
    if (this.props.deleteGalleryImage.Ack == 1) {
    }
  };

  closePopUp = () => {
    this.setState({
      visible1: false,
    });
  };

  render() {
    console.log('PROPS IN RENDER', this.props);

    var image_array = [];
    image_array = this.props.spaImageList.spaGallaryImages;
    const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Spa Gallery</Text>
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('spaGallery')}>
            <Icon
              name="md-add"
              type="ionicon"
              color="#e25cff"
              style={{fontSize: 24}}
            />
          </TouchableOpacity>
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <Dialog
          visible={this.state.visible1}
          dialogAnimation={
            new SlideAnimation({
              slideFrom: 'bottom',
            })
          }
          onTouchOutside={() => {
            this.closePopupbox();
          }}
          dialogStyle={{width: '80%'}}
          footer={
            <DialogFooter>
              <DialogButton
                textStyle={{
                  fontSize: 14,
                  color: '#333',
                  fontWeight: '700',
                }}
                text="YES"
                onPress={() => {
                  this.closePopupbox();
                }}
              />
              <DialogButton
                textStyle={{
                  fontSize: 14,
                  color: '#333',
                  fontWeight: '700',
                }}
                text="NO"
                onPress={() => {
                  this.closePopUp();
                }}
              />
            </DialogFooter>
          }>
          <DialogContent>
            <Text style={styles.popupText}>{this.state.popupMsg}</Text>
          </DialogContent>
        </Dialog>
       
        <ScrollView>
          {image_array && image_array.length ? (
            <View style={[styles.justifyrow, {flexWrap: 'wrap'}]}>
              {image_array.map((data, key) => {
                return (
                  <View>
                    {/* <TouchableOpacity style={styles.justifyavt}>
                        <Avatar
                          rounded
                          source={require('../../assets/images/avt3.png')}
                          size={70}
                          containerStyle={styles.avtstyle}
                        />
                      </TouchableOpacity> */}

                    <Avatar
                      rounded
                      source={{uri: `${data}`}}
                      size={80}
                      containerStyle={styles.avtstyle}
                    />
                    <TouchableOpacity
                      style={styles.custedit}
                      onPress={() => this.imageDelete(key)}
                      // onPress={this.imageDelete.bind()}
                      //onPress={this.imageDelete.bind(key)}>
                    >
                      <Image
                        source={require('../../assets/images/close.png')}
                        resizemode="contain"
                        style={{width: 30, height: 30}}
                      />
                    </TouchableOpacity>
                  </View>
                );
              })}
            </View>
          ) : null}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner,
});
export default connect(mapStateToProps, {
  spaGallaryFetchImages,
  spaGallaryDeleteImages,
})(gallery);
