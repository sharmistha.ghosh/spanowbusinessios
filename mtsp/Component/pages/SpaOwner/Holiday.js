import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  TextInput,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert
} from "react-native";
import { Avatar, Icon, CheckBox } from "react-native-elements";
import styles from "./style";
import { connect } from "react-redux";
import { spaHolidayAdd } from "./redux/SpaOwnerAction";
import Spinner from "react-native-loading-spinner-overlay";
import CalendarPicker from "react-native-calendar-picker";
import moment from "moment";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";

class Holiday extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      holiday: "",
      comment: "",
      visible1: false,
      popupMsg: "",
      holiday_error: "",
      comment_error: "",
      spa_owner_id: ""
    };
  }

  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem("login_id");
      if (value) {
        this.setState({ spa_owner_id: value });
      }
    } catch (error) {
      console.log(error);
    }
    console.log("spa_owner_id state", this.state.spa_owner_id);
  }

  componentDidUpdate(prevProps) {
    console.log("componentDidUpdate", this.props);
    if (prevProps.addSpaHoliday !== this.props.addSpaHoliday) {
      this.setState({ spinner: false });
      const addDetails = this.props.addSpaHoliday;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({ visible1: true, popupMsg: addDetails.msg });
        }
        //this.setState({spinner: false});
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg
            });
          }
        } else {
          // this.setState({
          //   spinner: false,
          // });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: "Something went wrong. Please try again later."
            });
          }
        }
      }
    }
  }

  closePopupbox = () => {
    this.setState({
      visible1: false
    });
    console.log("status", this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate("spaAccount");
    }
  };

  //--select spa holiday date--//
  onDateChange = date => {
    var increaseMonth = date._i.month + 1;

    let service_date = date._i.year + "-" + increaseMonth + "-" + date._i.day;

    this.setState({
      holiday: service_date,
      holiday_error: ""
    });
  };

  comment = text => {
    this.setState({
      comment_error: "",
      comment: text
    });
  };

  submit = () => {
    console.log("jckjskjkjdkjsk", this.state);

    if (this.state.holiday == "") {
      this.setState({
        holiday_error: "Enter a date"
      });
    } else if (this.state.comment == "") {
      this.setState({
        comment_error: "Enter comment"
      });
    } else {
      let userDetails = new FormData();
      userDetails.append("spa_owner_id", this.state.spa_owner_id);
      userDetails.append("holiday", this.state.holiday);
      userDetails.append("comment", this.state.comment);
      console.log("userDetails", userDetails);
      this.setState({spinner: true})
      this.props.spaHolidayAdd(userDetails);
    }
  };

  render() {
    console.log("state value in render", this.state);

    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("spaAccount")}
            >
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Add Spa Holiday</Text>
          </View>
        </View>
        <ScrollView>
          <View>
            <View style={{ backgroundColor: "#fff", padding: 10 }}>
              <CalendarPicker onDateChange={this.onDateChange} />
            </View>
            <Text style={styles.error}>{this.state.holiday_error}</Text>

            <TextInput
              style={styles.forminput}
              name="name"
              placeholder="Comment"
              placeholderTextColor="#9590a9"
              onChangeText={text => this.comment(text)}
            />
            <Text style={styles.error}>{this.state.comment_error}</Text>

            <TouchableOpacity
              style={styles.pinkbtn}
              onPress={() => this.submit()}
            >
              <Text style={styles.btntext}>Submit</Text>
            </TouchableOpacity>
            <Spinner
              visible={this.state.spinner}
              textContent={"Loading..."}
              textStyle={styles.spinnerTextStyle}
            />
            <Dialog
              visible={this.state.visible1}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              onTouchOutside={() => {
                this.closePopupbox();
              }}
              dialogStyle={{ width: "80%" }}
              footer={
                <DialogFooter>
                  <DialogButton
                    textStyle={{
                      fontSize: 14,
                      color: "#333",
                      fontWeight: "700"
                    }}
                    text="OK"
                    onPress={() => {
                      this.closePopupbox();
                    }}
                  />
                </DialogFooter>
              }
            >
              <DialogContent>
                <Text style={styles.popupText}>{this.state.popupMsg}</Text>
              </DialogContent>
            </Dialog>

            {/* {this.state.timeStatus ? (
              <View>
                <Text style={styles.h1}>Choose Time Slot</Text>

                <View style={styles.flexwrap}>
                  {this.state.timing.map((item, key) => (
                    <TouchableOpacity
                      style={
                        this.state.itemNo === key ? changeStyle : StaticStyle
                      }
                      onPress={() => this.selectBox(key, item)}
                      value={item}>
                      <Image
                        source={require('../../assets/images/clock.png')}
                        resizeMode="contain"
                        style={{width: 20, height: 20}}
                      />
                      <Text style={styles.grtext}>{item}</Text>
                    </TouchableOpacity>
                  ))}
                </View>
              </View>
            ) : (
              <Text style={styles.h1}>{this.state.timingMsg}</Text>
            )} */}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner
});
export default connect(mapStateToProps, { spaHolidayAdd })(Holiday);
