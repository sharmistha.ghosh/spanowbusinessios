import React, {Component} from 'react';
import {
  Text,
  View,
  Alert,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  TextInput,
} from 'react-native';
import styles from './style';
import {Avatar, Icon, ListItem} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import {connect} from 'react-redux';
import {forgotPassword} from './redux/forgotPasswordAction';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';

class ForgotPassword extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      email_error: '',
      userId: '',
      code: '',
      visible1: false,
      popupMsg: '',
    };
  }

  componentDidMount() {
    console.log(this.props);
  }

  emailHandler = email => {
    this.setState({email: email});
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!reg.test(email)) {
      this.setState({
        email_error: 'Enter valid email id',
      });
    } else {
      this.setState({
        email_error: '',
      });
    }
  };

  handleSubmit = () => {
    if (this.state.email == '') {
      this.setState({
        email_error: 'Enter email id',
      });
    } else {
      this.setState({spinner: true});
      let userData = new FormData();
      userData.append('email', this.state.email);
      this.props.forgotPassword(userData);
    }
  };

  async componentDidUpdate(prevProps) {
    console.log(this.props);
    if (prevProps !== this.props) {
      const addDetails = this.props.forgotPasswordUserDetails;
      if (this.props.error && this.props.error !== undefined) {
        //Alert.alert('Something went wrong');
        if (this.state.visible1 == false) {
          this.setState({visible1: true, popupMsg: addDetails.msg});
        }
        this.setState({spinner: false});
      } else {
        if (addDetails.ack === 1) {
          this.setState({spinner: false});
          await AsyncStorage.setItem('UserId', String(addDetails.userId));
          this.props.navigation.navigate('ResetPassword');
        } else {
          this.setState({
            spinner: false,
            email_error: addDetails.msg,
          });
        }
      }
    }
  }

  async storedata() {
    try {
      console.log('testid1', this.state.userId);

      await AsyncStorage.setItem('UserId', this.state.userId.toString());
      //this.props.navigation.navigate('ResetPassword',{userId:this.state.userId})
      console.log('testid2');
      //this.props.navigation.navigate('ResetPassword');
    } catch (error) {
      console.log('testid3');
      console.log(error);
      // Error saving data
    }
  }

  render() {
    return (
      <SafeAreaView>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />

        <ImageBackground
          source={require('../../assets/images/mainbg.jpg')}
          style={styles.bodymain}>
          <KeyboardAvoidingView enabled behavior="padding">
            <ScrollView>
                <View >
                  <TouchableOpacity  style={styles.forgetPassword} onPress={() => this.props.navigation.navigate('Login')}>
                    <Icon name="md-arrow-back" type="ionicon" color="#fff" />
                  </TouchableOpacity>
                </View>
              <View style={[styles.topbar]}>
              </View>
              <View style={[styles.container, {paddingTop: 30}]}>
                <Spinner
                  visible={this.state.spinner}
                  textContent={'Loading...'}
                  textStyle={styles.spinnerTextStyle}
                />
                <TouchableOpacity style={styles.logo}>
                  <Image
                    source={require('../../assets/images/logo.png')}
                    resizeMode="contain"
                    style={{width: 280, height: 80}}
                  />
                </TouchableOpacity>

                <View style={[styles.mainform, {marginTop: 50}]}>
                  <Text style={styles.heading}>Forgot Password</Text>

                  <TextInput
                    style={styles.forminput}
                    placeholder="Enter your email"
                    placeholderTextColor="#9590a9"
                    value={this.state.email}
                    onChangeText={email => this.emailHandler(email)}
                  />
                  <Text style={styles.error}>{this.state.email_error}</Text>
                  <View style={styles.container}>
                    <Dialog
                      visible={this.state.visible1}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: 'bottom',
                        })
                      }
                      onTouchOutside={() => {
                        this.setState({visible1: false});
                      }}
                      dialogStyle={{width: '80%'}}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: '#333',
                              fontWeight: '700',
                            }}
                            text="OK"
                            onPress={() => {
                              this.setState({visible1: false});
                            }}
                          />
                        </DialogFooter>
                      }>
                      <DialogContent>
                        <Text style={styles.popupText}>
                          {this.state.popupMsg}
                        </Text>
                      </DialogContent>
                    </Dialog>
                  </View>
                  <TouchableOpacity
                    style={styles.pinkbtn}
                    onPress={() => this.handleSubmit()}>
                    <Text style={styles.btntext}>SUBMIT</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.forgotPassword,
});

export default connect(mapStateToProps, {forgotPassword})(ForgotPassword);
