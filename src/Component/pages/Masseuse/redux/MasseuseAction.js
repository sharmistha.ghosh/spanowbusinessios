import {
  FETCH_MASSEUSE_DETAILS,
  MASSEUSE_APPOINTMENT_HISTORY,
  MASSEUSE_UPCOMMING_APPOINTMENT,
  EDIT_MASSEUSE_INFORMATION,
} from '../../../../redux/actions/types';
import Api from '../../../../Api/Api';


//----FETCH MASSEUSE DETAILS----//
export const fetchMasseuseDetails = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/userProfileView',
      userDetails,
    );

    if (response) {
      console.log("MASSEUSE_DETAILS",response);
      
      dispatch({type: FETCH_MASSEUSE_DETAILS, payload: response.profile_data});
    }
  } catch (error) {
    console.log("MASSEUSE_details_ERROR",error);
    dispatch({type: FETCH_MASSEUSE_DETAILS, payload: {response: error}});
  }
};


//----EDIT MASSEUSE INFORMATION----//
export const MasseuseProfileEdit = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/userUpdateProfile',
      userDetails,
    );

    if (response) {
      console.log("EDIT_MASSEUSE_INFORMATION",response);
      
      dispatch({type: EDIT_MASSEUSE_INFORMATION, payload: response});
    }
  } catch (error) {
    console.log("EDIT_MASSEUSE_INFORMATION_ERROR",error);
    dispatch({type: EDIT_MASSEUSE_INFORMATION, payload: {response: error}});
  }
};


//----FETCH APPOINTMENT HISTORY OF MASSEUSE----//
export const masseuseAppointmentHistory = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/bookings/bookingMasseuseHistory',
      userDetails,
    );

    if (response) {
      console.log("Appointment_History",response.booking_history);
      
      dispatch({type: MASSEUSE_APPOINTMENT_HISTORY, payload: response.booking_history});
    }
  } catch (error) {
    console.log("Appointment_History_ERROR",error);
    dispatch({type: MASSEUSE_APPOINTMENT_HISTORY, payload: {response: error}});
  }
};

//----FETCH UPCOMMING APPOINTMENT OF MASSEUSE----//
export const masseuseUpcommingAppointment = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/bookings/upcomingMasseuseBookings',
      userDetails,
    );

    if (response) {
      console.log("fetchUpcommingAppointment",response);
      
      dispatch({type: MASSEUSE_UPCOMMING_APPOINTMENT, payload: response.upcoming_bookings});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: MASSEUSE_UPCOMMING_APPOINTMENT, payload: {response: error}});
  }
};

