import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  AsyncStorage,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  TextInput
} from "react-native";
import styles from "../SpaOwner/style";
import Spinner from "react-native-loading-spinner-overlay";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import { connect } from "react-redux";
import moment from 'moment';
import { inputDiscount } from "./redux/SpaOwnerAction";
import { Icon } from "react-native-elements";
import { TextField } from "react-native-material-textfield";
import DatePicker from "react-native-datepicker";

class AddSpaDiscount extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      start_date: "",
      end_date: "",
      discount: "",
      discount_error: "",
      spa_owner_id: "",
      visible1: false,
      popupMsg: ""
    };
    let date = new Date();
  }

  //--FETCH LOGIN ID FROM ASYNCSTORAGE--//
  componentDidMount = async () => {
    // var day = new Date().getDate();
    // var month = new Date().getMonth() + 1;
    // var year = new Date().getFullYear();


    // console.log("month",new Date().getMonth());
    
    // let datee = month + "-" + day + "-" + year;
    // console.log("date",datee);
    

    // this.setState({
    //   start_date: month + "-" + day + "-" + year,
    //   end_date: month + "-" + day + "-" + year
    // });
    

    try {
      const value = await AsyncStorage.getItem("login_id");
      console.log("loginId in myaccount page", value);
      if (value) {
        this.setState({ spa_owner_id: value });
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
  };

  //--CLOSE POPUP BOX--//
  closePopupbox = () => {
    this.setState({
      visible1: false
    });
    if (this.props.spadiscount.Success === 1) {
      this.props.navigation.navigate("spaAccount");
    }
  };

  componentDidUpdate(prevProps) {
    console.log(this.props);

    if (prevProps.spadiscount !== this.props.spadiscount) {
      this.setState({ spinner: false });
      const addDetails = this.props.spadiscount;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({ visible1: true, popupMsg: addDetails.msg });
        }
      } else {
        if (addDetails.Ack === 1) {
          this.setState({ spinner: false, status: 1 });
          if (this.state.visible1 === false) {
            this.setState({ visible1: true, popupMsg: addDetails.msg });
          }
        } else {
          // this.setState({
          //   spinner: false,
          // });
          if (this.state.visible1 === false) {
            this.setState({ visible1: true, popupMsg: addDetails.msg });
          }
        }
      }
    }
  }

  discountHandler = value => {
    this.setState({ discount: value });
    this.forceUpdate();
    var regex = /^[0-9]+(\.[0-9]{1,2})?$/;
    if (regex.test(value)) {
      this.setState({ discount_error: "" });
    } else {
      this.setState({
        discount_error: "Enter discount correctly"
      });
    }
  };

  handleSubmit = () => {
    if(this.state.start_date == ""){
      this.setState({startDateError : "Enter Discount Start Date"})
    }
    else if(this.state.end_date == ""){
      this.setState({endDateError : "Enter Discount Start Date"})
    }
    else if (this.state.discount == "") {
      this.setState({
        discount_error: "Enter discount"
      });
    } else if (this.state.discount_error != "") {
      this.setState({
        discount_error: "Enter discount correctly"
      });
    } else {
      this.setState({ spinner: true });
      let todayDate = moment(new Date()).format('MM-DD-YYYY');
      console.log("todayDate====",todayDate);
      let discount = new FormData();
      discount.append("spa_owner_id", this.state.spa_owner_id);
      discount.append("discount_start_date", this.state.start_date);
      discount.append("discount_end_date", this.state.end_date);
      discount.append("discount", this.state.discount);
      discount.append("todayDate", todayDate);

      console.log("discount111111", discount);

      this.props.inputDiscount(discount);
    }
  };

  render() {
    const { goBack } = this.props.navigation;
    console.log("rernder",this.state.start_date);
    
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <KeyboardAvoidingView enabled behavior="height">
          <View style={[styles.topbar]}>
            <View style={styles.justifyrow}>
              <TouchableOpacity onPress={() => goBack()}>
                <Icon name="md-arrow-back" type="ionicon" color="#fff" />
              </TouchableOpacity>
              <Text style={styles.backheading}>Add Spa Discount</Text>
            </View>
          </View>
          <ScrollView>
            <View style={{ position: "relative", paddingBottom: 40 }}>
              <View style={styles.graycontainer}>
                <Spinner
                  visible={this.state.spinner}
                  textContent={"Loading..."}
                  textStyle={styles.spinnerTextStyle}
                />
                <Dialog
                  visible={this.state.visible1}
                  dialogAnimation={
                    new SlideAnimation({
                      slideFrom: "bottom"
                    })
                  }
                  onTouchOutside={() => {
                    this.closePopupbox();
                  }}
                  dialogStyle={{ width: "80%" }}
                  footer={
                    <DialogFooter>
                      <DialogButton
                        textStyle={{
                          fontSize: 14,
                          color: "#333",
                          fontWeight: "700"
                        }}
                        text="OK"
                        onPress={() => {
                          this.closePopupbox();
                        }}
                      />
                    </DialogFooter>
                  }
                >
                  <DialogContent>
                    <Text style={styles.popupText}>{this.state.popupMsg}</Text>
                  </DialogContent>
                </Dialog>
                <View style={styles.graycard}>
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text
                      style={{
                        marginBottom: 10,
                        fontWeight: "bold",
                        color: "#ffff",
                        fontSize: 16
                      }}
                    >
                      Discount Start Date
                    </Text>
                  </View>
                  <DatePicker
                    style={{ width: 260 }}
                    date={this.state.start_date}
                    mode="date"
                    placeholder="MM-DD-YYYY"
                    format="MM-DD-YYYY"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: "absolute",
                        left: 0,
                        top: 4
                      },
                      dateInput: {
                        marginLeft: 60,
                        backgroundColor: "#ffff"
                      }
                    }}
                    onDateChange={start_date => {
                      this.setState({ start_date: start_date, startDateError : "" });
                    }}
                  />
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text
                      style={{
                        marginBottom: 10,
                        fontWeight: "bold",
                        color: "red",
                        fontSize: 16,
                        marginTop: 8,
                        marginLeft: 60
                      }}
                    >
                     {this.state.startDateError}
                    </Text>
                  </View>
                </View>

                <View style={styles.graycard}>
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text
                      style={{
                        marginBottom: 10,
                        fontWeight: "bold",
                        color: "#ffff",
                        fontSize: 16
                      }}
                    >
                      Discount End Date
                    </Text>
                  </View>

                  <DatePicker
                    style={{ width: 260 }}
                    date={this.state.end_date}
                    mode="date"
                    placeholder="MM-DD-YYYY"
                    format="MM-DD-YYYY"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: "absolute",
                        left: 0,
                        top: 4
                      },
                      dateInput: {
                        marginLeft: 60,
                        backgroundColor: "#ffff"
                      }
                    }}
                    onDateChange={end_date => {
                      this.setState({ end_date: end_date, endDateError : "" });
                    }}
                  />
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text
                      style={{
                        marginBottom: 10,
                        fontWeight: "bold",
                        color: "red",
                        fontSize: 16,
                        marginTop: 8,
                        marginLeft: 60
                      }}
                    >
                     {this.state.endDateError}
                    </Text>
                  </View>
                </View>
                <View style={styles.graycard}>
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text
                      style={{
                        marginBottom: 10,
                        fontWeight: "bold",
                        color: "#ffff",
                        fontSize: 16
                      }}
                    >
                      Discount Percentage
                    </Text>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <TextInput
                        style={styles.discountinput}
                        placeholder="Enter discount"
                        placeholderTextColor="#9590a9"
                        keyboardType={"numeric"}
                        value={this.state.discount}
                        onChangeText={discount =>
                          this.discountHandler(discount)
                        }
                      />
                      <Text
                        style={{
                          fontSize: 20,
                          fontWeight: "bold",
                          marginLeft: 12,
                          color: "#ffff",
                          marginTop: 8
                        }}
                      >
                        %
                      </Text>
                    </View>

                    <Text
                      style={[
                        styles.error,
                        { marginLeft: -16, marginTop: 8, fontSize: 16 }
                      ]}
                    >
                      {this.state.discount_error}
                    </Text>
                  </View>
                </View>

                <TouchableOpacity
                  style={[styles.pinkbtn, { marginTop: 30 }]}
                  onPress={() => this.handleSubmit()}
                >
                  <Text style={styles.btntext}>Submit</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner
});
export default connect(mapStateToProps, {
  inputDiscount
})(AddSpaDiscount);
