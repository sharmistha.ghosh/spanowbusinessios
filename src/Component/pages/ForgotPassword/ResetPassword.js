import React, {Component} from 'react';
import {
  Text,
  View,
  Alert,
  StatusBar,
  SafeAreaView,
  AsyncStorage,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  TextInput,
} from 'react-native';
import styles from './style';
//import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import {connect} from 'react-redux';
import {resetPassword} from './redux/forgotPasswordAction';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';

class ResetPassword extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      otp_error: '',
      password1: '',
      password_error: '',
      confirm_passowrd: '',
      confirm_passowrd_error: '',
      userId: '',
      visible1: false,
      popupMsg: '',
      pin: '',
    };
  }

  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem('UserId');
      if (value) {
        this.setState({userId: value});
      }
    } catch (error) {
      // Error retrieving data
    }
  }
  otpHandler = otp => {
    this.setState({
      otp: otp,
    });
  };
  passwordHandler = password1 => {
    this.setState({
      password1: password1,
    });
    if (this.state.password1.length < 5) {
      this.setState({password_error: 'Password must be atleast 6 character'});
    } else {
      this.setState({password_error: ''});
    }
  };

  confirmpasswordHandler = confirm_passowrd => {
    this.setState({
      confirm_passowrd: confirm_passowrd,
    });
  };

  handleSubmit = () => {
    if (this.state.otp == '') {
      this.setState({
        otp_error: 'Enter Code',
      });
    }

    if (this.state.password == '') {
      this.setState({
        password_error: 'Enter New Password',
      });
    }
    if (this.state.confirm_passowrd == '') {
      this.setState({
        confirm_passowrd_error: 'Enter Confirm Password',
      });
    }

    if (this.state.password1 != this.state.confirm_passowrd) {
      this.setState({confirm_passowrd_error: 'Password not matched'});
    }

    if (this.state.password1.length < 5) {
      this.setState({password_error: 'Password must be atleast 6 character'});
    } else {
      this.setState({spinner: true});

      let newPassword = new FormData();

      newPassword.append('code', this.state.otp);
      newPassword.append('password', this.state.password1);
      newPassword.append('id', this.state.userId);

      this.props.resetPassword(newPassword);
      // Api.postApi('users/appresetpassword', newPassword)
      //   .then(addDetails => {
      //     console.log(addDetails);
      //     console.log(addDetails.msg);
      //     console.log(addDetails.ack);

      //     if (addDetails.ack === 1) {
      //       this.setState({spinner: false});
      //       Alert.alert(addDetails.msg);

      //       AsyncStorage.removeItem('UserId');

      //       this.props.navigation.navigate('Login');
      //       // this.props.navigation.dispatch(resetAction1);
      //     } else {
      //       this.setState({spinner: false});
      //       Alert.alert(addDetails.msg);
      //       //this.setState({ showLoader: false })
      //     }
      //   })
      //   .catch(err => {
      //     this.setState({spinner: false});
      //     // debugger
      //     Alert.alert(I18n('Something went wrong,please try again'));
      //   });
    }
  };

  //--CLOSE POPUP BOX--//
  closePopUp = () => {
    console.log('ggggg');

    this.setState({
      visible1: false,
      pin: 1,
    });
    
  };

  async componentDidUpdate(prevProps) {
    if (this.state.visible1 === false && this.state.pin === 1) {
      this.props.navigation.navigate('Login');
    }

    if (prevProps !== this.props) {
      const addDetails = this.props.resetPasswordUserDetails;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 == false) {
          this.setState({visible1: true, popupMsg: addDetails.msg});
        }
        this.setState({spinner: false});
      } else {
        if (addDetails) {
          if (addDetails.ack === 1) {
            this.setState({spinner: false});
            this.setState({spinner: false});
            console.log('ack 1 value of visible', this.state.visible1);

            if (this.state.visible1 === false) {
              this.setState({visible1: true, popupMsg: addDetails.msg});
            }

            await AsyncStorage.removeItem('UserId');

            //this.props.navigation.navigate('Login');
            // this.props.navigation.dispatch(resetAction1);
          } else {
            this.setState({spinner: false});
            if (this.state.visible1 === false) {
              this.setState({visible1: true, popupMsg: addDetails.msg});
            }
          }
        }
      }
    }
  }

  render() {
    return (
      <SafeAreaView>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <ImageBackground
          source={require('../../assets/images/mainbg.jpg')}
          style={styles.bodymain}>
          <KeyboardAvoidingView enabled behavior="padding">
            <ScrollView>
              <View style={[styles.container, {paddingTop: 30}]}>
                <Spinner
                  visible={this.state.spinner}
                  textContent={'Loading...'}
                  textStyle={styles.spinnerTextStyle}
                />
                <TouchableOpacity style={styles.logo}>
                  <Image
                    source={require('../../assets/images/logo.png')}
                    resizeMode="contain"
                    style={{width: 280, height: 80}}
                  />
                </TouchableOpacity>

                <View style={[styles.mainform, {marginTop: 50}]}>
                  <Text style={styles.heading}>Reset Password</Text>

                  <TextInput
                    style={styles.forminput}
                    placeholder="Code"
                    placeholderTextColor="#9590a9"
                    value={this.state.otp}
                    onChangeText={otp => this.otpHandler(otp)}
                  />
                  <Text style={styles.error}>{this.state.otp_error}</Text>

                  <TextInput
                    style={styles.forminput}
                    placeholder="New Password"
                    placeholderTextColor="#9590a9"
                    value={this.state.password1}
                    onChangeText={password1 => this.passwordHandler(password1)}
                    secureTextEntry={false}
                  />
                  <Text style={styles.error}>{this.state.password_error}</Text>

                  <TextInput
                    style={styles.forminput}
                    placeholder="Confirm New Password"
                    placeholderTextColor="#9590a9"
                    value={this.state.confirm_passowrd}
                    onChangeText={confirm_passowrd =>
                      this.confirmpasswordHandler(confirm_passowrd)
                    }
                    secureTextEntry={false}
                  />
                  <Text style={styles.error}>
                    {this.state.confirm_passowrd_error}
                  </Text>
                  <View style={styles.container}>
                    <Dialog
                      visible={this.state.visible1}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: 'bottom',
                        })
                      }
                      onTouchOutside={() => {
                        this.closePopUp();
                      }}
                      dialogStyle={{width: '80%'}}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: '#333',
                              fontWeight: '700',
                            }}
                            text="OK"
                            onPress={() => {
                              this.closePopUp();
                            }}
                          />
                        </DialogFooter>
                      }>
                      <DialogContent>
                        <Text style={styles.popupText}>
                          {this.state.popupMsg}
                        </Text>
                      </DialogContent>
                    </Dialog>
                  </View>
                  <TouchableOpacity
                    style={styles.pinkbtn}
                    onPress={() => this.handleSubmit()}>
                    <Text style={styles.btntext}>RESET</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.forgotPassword,
});

export default connect(mapStateToProps, {resetPassword})(ResetPassword);
