import Login from '../src/Component/pages/login/Login';
import Signup from '../src/Component/pages/login/Signup';
import service from '../src/Component/pages/SpaOwner/service';
import AddSpaDiscount from '../src/Component/pages/SpaOwner/AddSpaDiscount';
import AddSpecialOffer from '../src/Component/pages/SpaOwner/AddSpecialOffer';
import Holiday from '../src/Component/pages/SpaOwner/Holiday';
import dateTimeSelect from '../src/Component/pages/SpaOwner/dateTimeSelect';
import MasseuseList from '../src/Component/pages/SpaOwner/MasseuseList';
import gallery from '../src/Component/pages/SpaOwner/gallery';
import spaAccount from '../src/Component/pages/SpaOwner/spaAccount';
//import webView from '../src/Component/pages/SpaOwner/webView';
import spaGallery from '../src/Component/pages/SpaOwner/spaGallery';
import addMasseuse from '../src/Component/pages/SpaOwner/addMasseuse';
import editMasseuse from '../src/Component/pages/SpaOwner/editMasseuse';
import ViewMasseuse from '../src/Component/pages/SpaOwner/ViewMasseuse';


import spaInformation from '../src/Component/pages/SpaOwner/spaInformation';
import OwnerInformation from '../src/Component/pages/SpaOwner/OwnerInformation';
import AppointmentHistory from '../src/Component/pages/SpaOwner/AppointmentHistory';
import UpcommingAppointment from '../src/Component/pages/SpaOwner/UpcommingAppointment';
import cancelAppointment from '../src/Component/pages/SpaOwner/cancelAppointment';
import completeAppointment from '../src/Component/pages/SpaOwner/completeAppointment';
import Transection from '../src/Component/pages/SpaOwner/Transection';
import changePassword from '../src/Component/pages/ForgotPassword/changePassword';
import ForgotPassword from '../src/Component/pages/ForgotPassword/ForgotPassword';
import ResetPassword from '../src/Component/pages/ForgotPassword/ResetPassword';
//import imageUpload from '../src/Component/pages/SpaOwner/imageUpload';
import stripeDetails from '../src/Component/pages/SpaOwner/stripeDetails';

import MasseuseAccount from '../src/Component/pages/Masseuse/MasseuseAccount';
import MasseuseUpcommingAppointment from '../src/Component/pages/Masseuse/MasseuseUpcommingAppointment';
import MasseuseAppointmentHistory from '../src/Component/pages/Masseuse/MasseuseAppointmentHistory';
import MasseuseInformation from '../src/Component/pages/Masseuse/MasseuseInformation';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

export default createAppContainer(
  createStackNavigator(
    {
      //service: service,
     // spaAccount: spaAccount,
      
      
      Login: Login,
      Signup: Signup,
      changePassword: changePassword,
      ForgotPassword: ForgotPassword,
      ResetPassword: ResetPassword,

      //webView: webView,
      spaAccount: spaAccount,
      gallery: gallery,
      spaGallery: spaGallery,
      MasseuseList: MasseuseList,
      addMasseuse: addMasseuse,
      editMasseuse: editMasseuse,
      ViewMasseuse: ViewMasseuse,
      service: service,
      AddSpaDiscount: AddSpaDiscount,
      AddSpecialOffer: AddSpecialOffer,
      spaInformation: spaInformation,
      OwnerInformation: OwnerInformation,
      AppointmentHistory: AppointmentHistory,
      UpcommingAppointment: UpcommingAppointment,
      cancelAppointment: cancelAppointment,
      completeAppointment: completeAppointment,
      Transection: Transection,
      dateTimeSelect: dateTimeSelect,
      Holiday: Holiday,
      stripeDetails: stripeDetails,

      MasseuseAccount: MasseuseAccount,
      MasseuseUpcommingAppointment: MasseuseUpcommingAppointment,
      MasseuseAppointmentHistory: MasseuseAppointmentHistory,
      MasseuseInformation: MasseuseInformation,

      //imageUpload: imageUpload,
    },
    {
      headerMode: 'none',
    },
  ),
);
