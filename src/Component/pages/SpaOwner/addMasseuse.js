import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  AsyncStorage
} from "react-native";
import { TextInputMask } from 'react-native-masked-text'
import { Avatar, Icon, ListItem } from "react-native-elements";
import { TextField } from "react-native-material-textfield";
import RadioForm from "react-native-simple-radio-button";
import styles from "./style";
import { connect } from "react-redux";
import { masseuseRagistration } from "./redux/SpaOwnerAction";
import Spinner from "react-native-loading-spinner-overlay";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";

class addMasseuse extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      status: "",
      first_name: "",
      first_name_error: "",
      last_name: "",
      last_name_error: "",
      email: "",
      email_error: "",
      phone: "",
      phone_error: "",

      spinner: false,
      visible1: false,
      popupMsg: "",
      spa_owner_id: ""
    };
  }

  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem("login_id");
      console.log("loginId in myaccount page", value);
      if (value) {
        this.setState({ spa_owner_id: value });
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
  }
  componentDidUpdate(prevProps) {
    console.log("componentDidUpdate", this.props);
    if (prevProps.registerMasseuse !== this.props.registerMasseuse) {
      this.setState({ spinner: false });
      const addDetails = this.props.registerMasseuse;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({ visible1: true, popupMsg: addDetails.msg });
        }
        //this.setState({ spinner: false });
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg
            });
          }
          //setTimeout(()=>{this.props.navigation.navigate('myAccount')}, 1000);
        } else {
          // this.setState({
          //   spinner: false
          // });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: "Something went wrong. Please try again later."
            });
          }
        }
      }
    }
  }

  validate = (value, type) => {
    if (type === "first_name") {
      this.setState({ first_name: value, first_name_error: "" });
    }
    if (type === "last_name") {
      this.setState({ last_name: value, last_name_error: "" });
    }
    if (type === "email") {
      this.setState({ email: value });
      this.forceUpdate();
      var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (regex.test(value)) {
        this.setState({ email_error: "" });
      } else {
        this.setState({
          email_error: "You should provide proper email address."
        });
      }
    }

    if (type === "phone") {
      this.setState({ phone: value, phone_error: "" });
      this.forceUpdate();
      if( value.length < 14 ){
        this.setState({phone_error: 'Please enter proper ph number'});
      }
      // var regex = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

      // if (regex.test(value)) {
      //   this.setState({phone_error: ''});
      // } else {
      //   this.setState({
      //     phone_error: 'You should provide proper phone number',
      //   });
      // }
    }
  };

  closePopupbox = () => {
    this.setState({
      visible1: false
    });
    console.log("status", this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate("spaAccount");
    }
  };

  submit = () => {
    console.log("states", this.state);

    if (this.state.first_name == "") {
      this.setState({
        first_name_error: "Enter first Name",
        popupMsg: "Enter first Name",
        visible1: true
      });
    } else if (this.state.last_name == "") {
      this.setState({
        last_name_error: "Enter last email",
        popupMsg: "Enter last email",
        visible1: true
      });
    } else if (this.state.email == "") {
      this.setState({
        email_error: "Enter contact email",
        popupMsg: "Enter contact email",
        visible1: true
      });
    } else if (this.state.phone == "") {
      this.setState({
        phone_error: "Enter contact number",
        popupMsg: "Enter contact number",
        visible1: true
      });
    } else {
      console.log("this.state in edited masseuse data", this.state);

      let userDetails = new FormData();
      userDetails.append("spa_owner_id", this.state.spa_owner_id);
      userDetails.append(
        "first_name",
        this.state.first_name
          ? this.state.first_name
          : this.props.masseusedetails.first_name
      );
      userDetails.append(
        "last_name",
        this.state.last_name
          ? this.state.last_name
          : this.props.masseusedetails.last_name
      );
      userDetails.append(
        "email",
        this.state.email ? this.state.email : this.props.masseusedetails.email
      );
      userDetails.append(
        "phone",
        this.state.phone ? this.state.phone : this.props.masseusedetails.phone
      );
      userDetails.append("user_type", "M");

      console.log("send edited masseuse data", userDetails);
      this.setState({spinner: true})
      this.props.masseuseRagistration(userDetails);
    }
  };

  render() {
    const { goBack } = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Add Masseuse</Text>
          </View>
        </View>
        <ScrollView>
          <View style={{ flex: 1, paddingBottom: 60 }}>
            <ImageBackground
              source={require("../../assets/images/pbg.png")}
              style={styles.tprofile}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  paddingBottom: 70
                }}
              >
                <View style={{ position: "relative", top: 25 }}>
                  {/* <Text style={[styles.h3, {color: '#fff'}]}>
                      Masseuse Name
                    </Text> */}
                </View>

                <View style={styles.custsec}>
                  <Avatar
                    rounded
                    source={require("../../assets/images/download.png")}
                    size={90}
                    containerStyle={styles.custavtr}
                  />
                </View>
              </View>
            </ImageBackground>
            <Spinner
              visible={this.state.spinner}
              textContent={"Loading..."}
              textStyle={styles.spinnerTextStyle}
            />
            <Dialog
              visible={this.state.visible1}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              onTouchOutside={() => {
                this.closePopupbox();
              }}
              dialogStyle={{ width: "80%" }}
              footer={
                <DialogFooter>
                  <DialogButton
                    textStyle={{
                      fontSize: 14,
                      color: "#333",
                      fontWeight: "700"
                    }}
                    text="OK"
                    onPress={() => {
                      this.closePopupbox();
                    }}
                  />
                </DialogFooter>
              }
            >
              <DialogContent>
                <Text style={styles.popupText}>{this.state.popupMsg}</Text>
              </DialogContent>
            </Dialog>

            <View style={[styles.graycontainer, styles.formgrey]}>
              <View style={styles.graycard}>
                <TextField
                  label="First Name"
                  textColor={"#838389"}
                  baseColor={"#838389"}
                  labelFontSize={18}
                  activeLineWidth={1}
                  onChangeText={value => this.validate(value, "first_name")}
                />
                <Text style={styles.error}>{this.state.first_name_error}</Text>

                <TextField
                  label="Last Name"
                  textColor={"#838389"}
                  baseColor={"#838389"}
                  labelFontSize={18}
                  activeLineWidth={1}
                  onChangeText={value => this.validate(value, "last_name")}
                />
                <Text style={styles.error}>{this.state.last_name_error}</Text>

                <TextField
                  label="Email"
                  textColor={"#838389"}
                  baseColor={"#838389"}
                  labelFontSize={18}
                  activeLineWidth={1}
                  keyboardType={"email-address"}
                  onChangeText={value => this.validate(value, "email")}
                />
                <Text style={styles.error}>{this.state.email_error}</Text>
                <View style={{borderBottomColor: '#838389' , borderBottomWidth: 1, paddingBottom :8}}>
                <Text style={{paddingBottom: 8, color: '#838389', fontSize: 18 }}>Phone</Text>
                <TextInputMask
                  type={'cel-phone'}
                  options={{
                    maskType: 'BRL',
                    withDDD: true,
                    dddMask: '(999) 999-9999 '
                  }}
                  maxLength={14}
                  style={{color: '#838389'}}
                  value={this.state.phone}
                  onChangeText={value => this.validate(value, 'phone')}
                />
                </View>

                {/* <TextField
                  label="Phone"
                  textColor={"#838389"}
                  baseColor={"#838389"}
                  labelFontSize={18}
                  activeLineWidth={1}
                  keyboardType={"numeric"}
                  onChangeText={value => this.validate(value, "phone")}
                /> */}
                <Text style={styles.error}>{this.state.phone_error}</Text>
              </View>

              <TouchableOpacity
                style={styles.pinkbtn}
                onPress={() => this.submit()}
              >
                <Text style={styles.btntext}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner
});
export default connect(mapStateToProps, { masseuseRagistration })(addMasseuse);
