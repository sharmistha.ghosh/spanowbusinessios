import {LOGIN} from '../../../../redux/actions/types';
import {SIGNUP} from '../../../../redux/actions/types';
import {LOG_OUT} from '../../../../redux/actions/types';
import {LOG_OUT2} from '../../../../redux/actions/types';


const intialState = {
  userDetails: {},
  signUpDetails: {},
  isSignupIn: '',
  isLoggedIn: '',
  error: '',
  logout: '',
  logout2: ''
};

export default function(state = intialState, action) {
  //console.log(action);
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        userDetails: action.payload,
        isLoggedIn: action.payload.isLoggedIn,
        error: action.payload.response,
        // login_userid: action.payload.userId.toString(),
      };
      case SIGNUP:
      return {
        ...state,
        signUpDetails: action.payload,
        isSignupIn: action.payload.isSignupIn,
        error: action.payload.response,
      };

      case LOG_OUT:
      return {
        ...state,
        logout: action.payload,
      };

      case LOG_OUT2:
      return {
        ...state,
        logout2: action.payload,
      };

    default:
      return state;
  }
}
