import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  AsyncStorage,
} from 'react-native';
import {Avatar, Icon, ListItem} from 'react-native-elements';
import { TextInputMask } from 'react-native-masked-text'
import {TextField} from 'react-native-material-textfield';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import styles from './style';
import {connect} from 'react-redux';
import {
  fetchMasseuseDetails,
  MasseuseProfileEdit,
} from './redux/MasseuseAction';
import ImagePicker from 'react-native-image-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';

var radio_props = [{label: 'Male', value: 0}, {label: 'Female', value: 1}];

class MasseuseInformation extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      first_name: '',
      first_name_error: '',
      last_name: '',
      last_name_error: '',
      email: '',
      email_error: '',
      phone: '',
      phone_error: '',
      address: '',
      address_error: '',
      profile_image: '',

      gender: null,
      gender_error: '',
      masseuse_id: '',
      image_source: '',
      spinner: false,
      visible1: false,
      popupMsg: '',
    };
    this.fetchMasseuseData();
  }

  //----FETCH SPA OWNER DETAILS IN REDUX----//
  async fetchMasseuseData() {
    try {
      const value = await AsyncStorage.getItem('login_id');
      console.log('loginId in myaccount page', value);
      if (value) {
        this.setState({masseuse_id: value});
      }
    } catch (error) {
      console.log(error);
    }
    let userDetails = new FormData();
    userDetails.append('user_id', this.state.masseuse_id);
    this.props.fetchMasseuseDetails(userDetails);
    console.log('props in componentdidmount', this.props.masseusedetails);
    this.setState({
      first_name: this.props.masseusedetails.first_name,
      last_name: this.props.masseusedetails.last_name,
      email: this.props.masseusedetails.email,
      phone: this.props.masseusedetails.phone,
      address: this.props.masseusedetails.address,
      gender: this.props.masseusedetails.gender,
      image_source: this.props.masseusedetails.profile_image,
    });
  }

  componentDidUpdate(prevProps) {
    console.log('componentDidUpdate', this.props);
    if (prevProps.masseuseEdit !== this.props.masseuseEdit) {
      const addDetails = this.props.masseuseEdit;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({visible1: true, popupMsg: addDetails.msg});
        }
        this.setState({spinner: false});
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1,
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg,
            });
          }
          //setTimeout(()=>{this.props.navigation.navigate('myAccount')}, 1000);
        } else {
          this.setState({
            spinner: false,
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: 'Something went wrong. Please try again later.',
            });
          }
        }
      }
    }
  }

  handlechoosePhoto = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // let source = {uri: response.uri};

        this.setState({
          image_source: response.uri,
          profile_image: response.data,
        });
      }
    });
  };

  validate = (value, type) => {
    if (type === 'first_name') {
      this.setState({first_name: value, first_name_error: ''});
    }
    if (type === 'last_name') {
      this.setState({last_name: value, last_name_error: ''});
    }
    if (type === 'email') {
      this.setState({email: value});
      this.forceUpdate();
      var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (regex.test(value)) {
        this.setState({email_error: ''});
      } else {
        this.setState({
          email_error: 'You should provide proper email address.',
        });
      }
    }

    if (type === 'phone') {
      this.setState({phone: value, phone_error: ""});
      this.forceUpdate();
      if( value.length < 14 ){
        this.setState({phone_error: 'Please enter proper ph number'});
      }
     
    }

    if (type === 'address') {
      this.setState({address: value});
      this.forceUpdate();
    }

    if (type === 'gender') {
      this.setState({gender: String(value)});
    }
  };

  closePopupbox = () => {
    this.setState({
      visible1: false,
    });
    console.log('status', this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate('MasseuseAccount');
    }
  };

  submit = () => {
    console.log('states', this.state);

    if (this.state.first_name == '') {
      this.setState({
        first_name_error: 'Enter first Name',
        popupMsg: 'Enter first Name',
        visible1: true,
      });
    } else if (this.state.last_name == '') {
      this.setState({
        last_name_error: 'Enter last email',
        popupMsg: 'Enter last email',
        visible1: true,
      });
    } else if (this.state.email == '') {
      this.setState({
        email_error: 'Enter contact email',
        popupMsg: 'Enter contact email',
        visible1: true,
      });
    } else if (this.state.phone == '') {
      this.setState({
        phone_error: 'Enter contact number',
        popupMsg: 'Enter contact number',
        visible1: true,
      });
    } else if (this.state.address == '') {
      this.setState({
        address_error: 'Enter address',
        popupMsg: 'Enter address',
        visible1: true,
      });
    } else {
      console.log('this.state in edited masseuse data', this.state);

      let userDetails = new FormData();
      userDetails.append('id', this.state.masseuse_id);
      userDetails.append(
        'profile_image',
        this.state.profile_image
          ? this.state.profile_image
          : "",
      );
      userDetails.append(
        'first_name',
        this.state.first_name
          ? this.state.first_name
          : this.props.masseusedetails.first_name,
      );
      userDetails.append(
        'last_name',
        this.state.last_name
          ? this.state.last_name
          : this.props.masseusedetails.last_name,
      );
      userDetails.append(
        'email',
        this.state.email ? this.state.email : this.props.masseusedetails.email,
      );
      userDetails.append(
        'phone',
        this.state.phone ? this.state.phone : this.props.masseusedetails.phone,
      );
      userDetails.append(
        'address',
        this.state.address ? this.state.address : this.props.masseusedetails.address,
      );

      userDetails.append(
        'gender',
        this.state.gender == null
          ? this.props.masseusedetails.gender
          : this.state.gender,
      );

      console.log('send edited masseuse data', userDetails);
      this.props.MasseuseProfileEdit(userDetails);
    }
  };

  render() {
    console.log('states in render', this.state);
    console.log('PROPS in render', this.props);
    var logo;

    {
      this.state.image_source
        ? (logo = this.state.image_source)
        : (logo = this.props.masseusedetails.profile_image);
    }

    const {goBack} = this.props.navigation;

    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Masseuse Information</Text>
          </View>
        </View>

        <ScrollView>
          <View style={{flex: 1, paddingBottom: 60}}>
            <ImageBackground
              source={require('../../assets/images/pbg.png')}
              style={styles.tprofile}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingBottom: 70,
                }}>
                <View style={{position: 'relative', top: 25}}>
                  <Text style={[styles.h3, {color: '#fff'}]}>
                    {this.props.masseusedetails.name}
                  </Text>
                </View>

                <View style={styles.custsec}>
                  {logo ? (
                    <Avatar
                      rounded
                      source={{uri: `${logo}`}}
                      onPress={this.handlechoosePhoto}
                      size={90}
                      containerStyle={styles.custavtr}
                    />
                  ) : (
                    <Avatar
                      rounded
                      source={require('../../assets/images/download.png')}
                      onPress={this.handlechoosePhoto}
                      size={90}
                      containerStyle={styles.custavtr}
                    />
                  )}
                </View>
              </View>
            </ImageBackground>
            <Avatar
              rounded
              source={require('../../assets/images/download.png')}
              onPress={this.handlechoosePhoto}
              size={90}
              containerStyle={styles.custavtr}
            />

            <Spinner
              visible={this.state.spinner}
              textContent={'Loading...'}
              textStyle={styles.spinnerTextStyle}
            />
            <Dialog
              visible={this.state.visible1}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: 'bottom',
                })
              }
              onTouchOutside={() => {
                this.closePopupbox();
              }}
              dialogStyle={{width: '80%'}}
              footer={
                <DialogFooter>
                  <DialogButton
                    textStyle={{
                      fontSize: 14,
                      color: '#333',
                      fontWeight: '700',
                    }}
                    text="OK"
                    onPress={() => {
                      this.closePopupbox();
                    }}
                  />
                </DialogFooter>
              }>
              <DialogContent>
                <Text style={styles.popupText}>{this.state.popupMsg}</Text>
              </DialogContent>
            </Dialog>

            <View style={[styles.graycontainer, styles.formgrey]}>
              <View style={styles.graycard}>
                <TextField
                  label="First Name"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  // editable={false}
                  defaultValue={this.props.masseusedetails.first_name}
                  onChangeText={value => this.validate(value, 'first_name')}
                />
                <Text style={styles.error}>{this.state.first_name_error}</Text>

                <TextField
                  label="Last Name"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  defaultValue={this.props.masseusedetails.last_name}
                  onChangeText={value => this.validate(value, 'last_name')}
                />
                <Text style={styles.error}>{this.state.last_name_error}</Text>

                <TextField
                  label="Email"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  editable={false}
                  activeLineWidth={1}
                  defaultValue={this.props.masseusedetails.email}
                  keyboardType={'email-address'}
                  onChangeText={value => this.validate(value, 'email')}
                />
                <Text style={styles.error}>{this.state.email_error}</Text>

                <View style={{borderBottomColor: '#838389' , borderBottomWidth: 1, paddingBottom :8}}>
                <Text style={{paddingBottom: 8, color: '#838389', fontSize: 18 }}>Phone</Text>
                <TextInputMask
                  type={'cel-phone'}
                  options={{
                    maskType: 'BRL',
                    withDDD: true,
                    dddMask: '(999) 999-9999 '
                  }}
                  maxLength={14}
                  style={{color: '#838389'}}
                  value={this.state.phone}
                  onChangeText={value => this.validate(value, 'phone')}
                />
                </View>

                {/* <TextField
                  label="Phone"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  defaultValue={this.props.masseusedetails.phone}
                  //keyboardType={'numeric'}
                  onChangeText={value => this.validate(value, 'phone')}
                /> */}
                <Text style={styles.error}>{this.state.phone_error}</Text>

                <TextField
                  label="Address"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  defaultValue={this.props.masseusedetails.address}
                  onChangeText={value => this.validate(value, 'address')}
                />
                <Text style={styles.error}>{this.state.address_error}</Text>

                <View style={styles.custtxt}>
                  <Text style={styles.h3}>Gender</Text>
                  <View style={{marginTop: 20}}>
                    <RadioForm
                      radio_props={radio_props}
                      initial={Number(this.props.masseusedetails.gender)}
                      formHorizontal={true}
                      labelHorizontal={true}
                      buttonColor={'#3e3e49'}
                      buttonOuterColor={'#3e3e49'}
                      selectedButtonColor={'#e25cff'}
                      labelStyle={{
                        color: '#fefefe',
                        fontSize: 15,
                        marginRight: 15,
                      }}
                      buttonStyle={{
                        borderWidth: 1,
                      }}
                      buttonSize={10}
                      animation={true}
                      onPress={value => {
                        this.validate(value, 'gender');
                      }}
                    />
                  </View>
                </View>

                {/* <Text style={styles.error}>{this.state.wifi_error}</Text> */}
              </View>

              <TouchableOpacity
                style={styles.pinkbtn}
                onPress={() => this.submit()}>
                <Text style={styles.btntext}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.Masseuse,
});
export default connect(
  mapStateToProps,
  {fetchMasseuseDetails, MasseuseProfileEdit},
)(MasseuseInformation);
