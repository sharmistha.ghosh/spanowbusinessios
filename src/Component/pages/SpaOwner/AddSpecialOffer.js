import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  AsyncStorage,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  TextInput
} from "react-native";
import styles from "./style";
import Spinner from "react-native-loading-spinner-overlay";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import { connect } from "react-redux";
import moment from 'moment';
import { addSpecialDiscount } from "./redux/SpaOwnerAction";
import { Icon, CheckBox } from "react-native-elements";
import { TextField } from "react-native-material-textfield";
import DatePicker from "react-native-datepicker";

class AddSpecialOffer extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      spa_owner_id: "",
      visible1: false,
      popupMsg: "",
      special_discount: "",
      special_discount_date: "",
      specialtDateError: "",
      special_discount_messege: "",
      specialtMessegeError: "",
      specialDiscountError: ""
    };
    let date = new Date();
  }

  //--FETCH LOGIN ID FROM ASYNCSTORAGE--//
  componentDidMount = async () => {
    try {
      const value = await AsyncStorage.getItem("login_id");
      if (value) {
        this.setState({ spa_owner_id: value });
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
  };

  //--CLOSE POPUP BOX--//
  closePopupbox = () => {
    this.setState({
      visible1: false
    });
    if (this.props.specialOffer.Success === 1) {
      this.props.navigation.navigate("spaAccount");
    }
  };

  componentDidUpdate(prevProps) {
    console.log("didupdate",this.props);
    if (prevProps.specialOffer !== this.props.specialOffer) {
      this.setState({ spinner: false });
      const addDetails = this.props.specialOffer;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({ visible1: true, popupMsg: addDetails.msg });
        }
      } else {
        if (addDetails.Ack === 1) {
          this.setState({ spinner: false, status: 1 });
          if (this.state.visible1 === false) {
            this.setState({ visible1: true, popupMsg: addDetails.msg });
          }
        } else {
          // this.setState({
          //   spinner: false,
          // });
          if (this.state.visible1 === false) {
            this.setState({ visible1: true, popupMsg: addDetails.msg });
          }
        }
      }
    }
  }

  specialDiscountHandler = value => {
    this.setState({ special_discount: value });
    this.forceUpdate();
    var regex = /^[0-9]+(\.[0-9]{1,2})?$/;
    if (regex.test(value)) {
      this.setState({ specialDiscountError: "" });
    } else {
      this.setState({
        specialDiscountError: "Enter discount correctly"
      });
    }
  };

  handleSubmit = () => {
    
    if(this.state.special_discount_date == "")
    {
      this.setState({specialtDateError: "Enter Special Discount Percentage"})
    }
    else if(this.state.special_discount == ""){
      this.setState({specialDiscountError: "Enter Special Discount Percentage"})
    }
    else if(this.state.special_discount_messege == "")
    {
      this.setState({specialtMessegeError: "Enter Special Messeges"})
    }
    else if( this.state.specialDiscountError == "" && this.state.specialtDateError == "" && this.state.specialtMessegeError == ""){
      this.setState({ spinner: true });
      let todayDate = moment(new Date()).format('MM-DD-YYYY');
      let specialOffer = new FormData();
      specialOffer.append("spa_owner_id", this.state.spa_owner_id);
      specialOffer.append("specialDiscount", this.state.special_discount);
      specialOffer.append("specialDiscountDate", this.state.special_discount_date);
      specialOffer.append("SpecialDiscountMsg", this.state.special_discount_messege);
      specialOffer.append("todayDate", todayDate);

      console.log("discount add api call in add discount page", specialOffer);

      this.props.addSpecialDiscount(specialOffer);
    }
  };

  render() {
    const { goBack } = this.props.navigation;
    console.log(this.props);
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <KeyboardAvoidingView enabled behavior="height">
          <View style={[styles.topbar]}>
            <View style={styles.justifyrow}>
              <TouchableOpacity onPress={() => goBack()}>
                <Icon name="md-arrow-back" type="ionicon" color="#fff" />
              </TouchableOpacity>
              <Text style={styles.backheading}>Add Spa Discount</Text>
            </View>
          </View>
          <ScrollView>
            <View style={{ position: "relative", paddingBottom: 40 }}>
              <View style={styles.graycontainer}>
                <Spinner
                  visible={this.state.spinner}
                  textContent={"Loading..."}
                  textStyle={styles.spinnerTextStyle}
                />
                <Dialog
                  visible={this.state.visible1}
                  dialogAnimation={
                    new SlideAnimation({
                      slideFrom: "bottom"
                    })
                  }
                  onTouchOutside={() => {
                    this.closePopupbox();
                  }}
                  dialogStyle={{ width: "80%" }}
                  footer={
                    <DialogFooter>
                      <DialogButton
                        textStyle={{
                          fontSize: 14,
                          color: "#333",
                          fontWeight: "700"
                        }}
                        text="OK"
                        onPress={() => {
                          this.closePopupbox();
                        }}
                      />
                    </DialogFooter>
                  }
                >
                  <DialogContent>
                    <Text style={styles.popupText}>{this.state.popupMsg}</Text>
                  </DialogContent>
                </Dialog>

                <View  style={styles.graycard}>
                  <View
                  style={{ justifyContent: "center", alignItems: "center", marginBottom: 20}}
                >
                  <Text
                    style={{
                      marginBottom: 10,
                      fontWeight: "bold",
                      color: "#ffff",
                      fontSize: 16
                    }}
                  >
                    Special Discount Date
                  </Text>
                </View>
                <DatePicker
                  style={{ width: 260 }}
                  date={this.state.special_discount_date}
                  mode="date"
                  placeholder="MM-DD-YYYY"
                  format="MM-DD-YYYY"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 4
                    },
                    dateInput: {
                      marginLeft: 60,
                      backgroundColor: "#ffff"
                    }
                  }}
                  onDateChange={special_discount_date => {
                    this.setState({ special_discount_date: special_discount_date, specialtDateError: "" });
                  }}
                />
                {this.state.specialtDateError != "" &&
                <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        color: "red",
                        fontSize: 16,
                        marginTop: 8,
                      }}
                    >
                     {this.state.specialtDateError}
                    </Text>
                  </View>
                }
                </View>
                <View  style={styles.graycard}>
                <View
                    style={{ justifyContent: "center", alignItems: "center", marginBottom: 20 }}
                  >
                    <Text
                      style={{
                        marginBottom: 10,
                        fontWeight: "bold",
                        color: "#ffff",
                        fontSize: 16
                      }}
                    >
                      Special Discount Percentage
                    </Text>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <TextInput
                        style={styles.discountinput}
                        placeholder="Enter discount"
                        placeholderTextColor="#9590a9"
                        keyboardType={"numeric"}
                        value={this.state.special_discount}
                        onChangeText={special_discount =>
                          this.specialDiscountHandler(special_discount)
                        }
                      />
                      <Text
                        style={{
                          fontSize: 20,
                          fontWeight: "bold",
                          marginLeft: 12,
                          color: "#ffff",
                          marginTop: 8
                        }}
                      >
                        %
                      </Text>
                    </View>
                    {this.state.specialDiscountError!= "" &&
                    <Text
                      style={[
                        styles.error,
                        { marginLeft: -16, marginTop: 8, fontSize: 16 }
                      ]}
                    >
                      {this.state.specialDiscountError}
                    </Text>
                    }
                  </View>
                  </View>
                <View style={styles.graycard}>
                <View
                  style={{ justifyContent: "center", alignItems: "center", marginBottom: 20 }}
                >
                  <Text
                    style={{
                      marginBottom: 10,
                      fontWeight: "bold",
                      color: "#ffff",
                      fontSize: 16
                    }}
                  >
                    Special Discount Message
                  </Text>
                </View>
                  <TextInput
                    style={styles.speDiscountText}
                    placeholder="Enter Message"
                    placeholderTextColor="#9590a9"
                    value={this.state.special_discount_messege}
                    onChangeText={special_discount_messege => {
                      this.setState({ special_discount_messege: special_discount_messege, specialtMessegeError: "" })
                    }}
                  />
                  {this.state.specialtMessegeError != "" &&
                    <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        color: "red",
                        fontSize: 16,
                        marginTop: 8,
                      }}
                    >
                     {this.state.specialtMessegeError}
                    </Text>
                  </View>
                  }
                  </View>
                <TouchableOpacity
                  style={[styles.pinkbtn, { marginTop: 30, marginBottom: 30}]}
                  onPress={() => this.handleSubmit()}
                >
                  <Text style={styles.btntext}>Submit</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner
});
export default connect(mapStateToProps, {
  addSpecialDiscount
})(AddSpecialOffer);
