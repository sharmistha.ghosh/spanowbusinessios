import {combineReducers} from 'redux';
import authReducer from '../../Component/pages/login/redux/authReducer';
import forgotPasswordReducer from '../../Component/pages/ForgotPassword/redux/forgotPasswordReducer';
import SpaOwnerReducer from '../../Component/pages/SpaOwner/redux/SpaOwnerReducer';
import MasseuseReducer from '../../Component/pages/Masseuse/redux/MasseuseReducer';

// import postReducer from './postReducer';
// import authReducer from './AuthReducer';

export default combineReducers({
  auth: authReducer,
  forgotPassword: forgotPasswordReducer,
  SpaOwner: SpaOwnerReducer,
  Masseuse: MasseuseReducer,
});
