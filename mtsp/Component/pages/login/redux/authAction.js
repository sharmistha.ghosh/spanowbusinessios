import {LOGIN} from '../../../../redux/actions/types';
import {SIGNUP} from '../../../../redux/actions/types';
import {LOG_OUT} from '../../../../redux/actions/types';
import {LOG_OUT2} from '../../../../redux/actions/types';

import Api from '../../../../Api/Api';
import AsyncStorage from '@react-native-community/async-storage';

export const logOut = logOutDetails => async dispatch => {
  dispatch({type:LOG_OUT,payload:{...logOutDetails}})
};

export const logOut2 = logOutDetails => async dispatch => {
  dispatch({type:LOG_OUT2,payload:{...logOutDetails}})
};

export const login = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('users/applogin', userDetails);
    if (response) {
      await AsyncStorage.setItem('login_id', String(response.userId));
      await AsyncStorage.setItem('spa_id', String(response.userdetail.spa_id));
      dispatch({type: LOGIN, payload: {...response, isLoggedIn: true}});
      //console.log("Login", response);
      
    }
  } catch (error) {
    console.log(error);
    dispatch({type: LOGIN, payload: {response: error, isLoggedIn: false}});
  }
};
export const signUp = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('users/appsignup_name', userDetails);
    if (response) {
      dispatch({type: SIGNUP, payload: {...response, isSignupIn: true}});
    }
  } catch (error) {
    dispatch({type: SIGNUP, payload: {response: error}, isSignupIn: false});
  }
};
