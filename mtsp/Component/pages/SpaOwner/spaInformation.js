import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  AsyncStorage,
  YellowBox,
  TouchableHighlight,
  Keyboard,
  TextInput,
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text'
import {Avatar, Icon, ListItem} from 'react-native-elements';
import {TextField} from 'react-native-material-textfield';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import styles from './style';
import {connect} from 'react-redux';
import {fetchSpaDetails, spaEdit} from './redux/SpaOwnerAction';

import ImagePicker from 'react-native-image-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import _ from 'lodash';

var radio_props = [
  {label: 'Not Available', value: 0},
  {label: 'Available', value: 1},
];

class spaInformation extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      spa_name: '',
      spa_name_error: '',
      email: '',
      email_error: '',
      phone: '',
      phone_error: '',
      address: '',
      address_error: '',
      description: '',
      no_of_bed: '',
      no_of_bed_error: '',
      airCondition: null,
      airCondition_error: '',
      wifi: null,
      wifi_error: '',
      car_parking: null,
      car_parking_error: '',
      basic_service_price_per_hour: '',
      basic_service_price_per_hour_error: '',
      logo_image: '',

      spa_owner_id: '',
      image_source: '',
      spinner: true,
      visible1: false,
      popupMsg: '',
      latitude: '',
      longitude: '',
      locationPredictions: [],
      place_id: '',
    };

    this.onChangeDestinationDebounced = _.debounce(
      this.onChangeDestination,
      1000,
    );
    this.fetchSpaDetails();
  }

  //----FETCH SPA PROILE DETAILS IN REDUX----//
  async fetchSpaDetails() {
    // YellowBox.ignoreWarnings(['VirtualizedLists should never be nested inside plain ScrollViews with the same orientation - use another VirtualizedList-backed container instead.']);
    try {
      const value = await AsyncStorage.getItem('login_id');
      console.log('loginId in myaccount page', value);
      if (value) {
        this.setState({spa_owner_id: value});
      }
    } catch (error) {
      console.log(error);
    }
    this.setState({spinner: false})
    let userDetails = new FormData();
    userDetails.append('spa_owner_id', this.state.spa_owner_id);
    this.props.fetchSpaDetails(userDetails);
    console.log('props in componentdidmount', this.props.spadetails);
    this.setState({
      spa_name: this.props.spadetails.name,
      email: this.props.spadetails.email,
      phone: this.props.spadetails.phone,
      address: this.props.spadetails.address,
      latitude: this.props.spadetails.lattitude,
      longitude: this.props.spadetails.longitude,
      no_of_bed: this.props.spadetails.no_of_bed,
      airCondition: this.props.spadetails.is_aircondition_available,
      wifi: this.props.spadetails.is_wifi_available,
      car_parking: this.props.spadetails.is_parking_available,
      basic_service_price_per_hour: this.props.spadetails
        .basic_service_price_per_hour,
      image_source: this.props.spadetails.logo_image,
    });
  }

  componentDidUpdate(prevProps) {
    console.log('componentDidUpdate', this.props);
    if (prevProps.spaeditinformation !== this.props.spaeditinformation) {
      this.setState({spinner: false});
      const addDetails = this.props.spaeditinformation;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({visible1: true, popupMsg: addDetails.msg});
        }
       // this.setState({spinner: false});
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1,
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg,
            });
          }
        } else {
          // this.setState({
          //   spinner: false,
          // });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: 'Something went wrong. Please try again later.',
            });
          }
        }
      }
    }
  }
  /// autocomplete api calling
  async onChangeDestination(destination) {
    console.log('onChangeDestination---------------------');

    let apiKey = 'AIzaSyBAggkFJrHx1AkzMXjaAPbJc7JN3B3vuK4';
    let latitude = 0;
    let longitude = 0;
    this.setState({destination});
    const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apiKey}&input={${destination}}&location=${latitude},${longitude}&radius=2000`;
    const result = await fetch(apiUrl);
    console.log('result spi address prediction-----', result);

    const jsonResult = await result.json();
    console.log('result spi address prediction-----2222', jsonResult);

    this.setState({
      locationPredictions: jsonResult.predictions,
      place_id: jsonResult.predictions[0].place_id,
    });
    console.log(
      '+++++++++++++++++++++++++++++++++',
      jsonResult.predictions[0].place_id,
    );

    this.fetchlatlong();
  }

  async fetchlatlong() {
    this.setState({latitude: '', longitude: ''});
    console.log('fetchlatlong place_id', this.state.place_id);

    //     var placeIdOrigin = this.state.place_id
    //     apiClient.getRequest(`https://maps.googleapis.com/maps/api/place/details/json?place_id=${this.state.place_id}&fields=geometry&key=${'AIzaSyD604gSo5WlrM3JbYwSG0pxnepzZkdh6HY'}`)
    //     .then(({data})=> {
    //       console.log("kkkkkk",data);

    // })

    let apiKey = 'AIzaSyDEyGzl0LMw3Hu51V97K58-vegjynrUtRU';
    let latitude = 0;
    let longitude = 0;

    const apiUrl = `https://maps.googleapis.com/maps/api/place/details/json?place_id=${this.state.place_id}&fields=geometry&key=${apiKey}`;
    const result = await fetch(apiUrl);
    const jsonResult = await result.json();    
    this.setState({
      address_error: '',
      latitude: jsonResult.result.geometry.location.lat,
      longitude: jsonResult.result.geometry.location.lng,
    });
    console.log('888888888888888888888', jsonResult);
  }

  async pressedPrediction(prediction) {
    let apiKey = 'AIzaSyDEyGzl0LMw3Hu51V97K58-vegjynrUtRU';
    console.log('============================== ', prediction.place_id);
    let placeid = prediction.place_id;
    Keyboard.dismiss();
    this.setState({
      locationPredictions: [],
      destination: prediction.description,
      place_id: prediction.place_id,
    });
    Keyboard;
    this.fetchlatlong();
  }
  handlechoosePhoto = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // let source = {uri: response.uri};

        this.setState({
          image_source: response.uri,
          logo_image: response.data,
        });
      }
    });
  };

  validate = (value, type) => {
    console.log('execute spa_name handler', this.state.spa_name);
    console.log('execute type handler', type);
    console.log('execute value handler', value);
    if (type === 'spa_name') {
      this.setState({spa_name: value, spa_name_error: ''});
    }
    if (type === 'email') {
      this.setState({email: value});
      this.forceUpdate();
      var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (regex.test(value)) {
        this.setState({email_error: ''});
      } else {
        this.setState({
          email_error: 'You should provide proper email address.',
        });
      }
    }
    if (type === 'phone') {
      this.setState({phone: value , phone_error: ''});
      this.forceUpdate();
      if( value.length < 14 ){
        this.setState({phone_error: 'Please enter proper phone number'});
      }
    }

    if (type === 'address') {
      this.setState({address: value});
      this.forceUpdate();
    }

    if (type === 'description') {
      this.setState({description: value});
      this.forceUpdate();
    }

    if (type === 'no_of_bed') {
      this.setState({no_of_bed: value});
      this.forceUpdate();
      var regex = /^[0-9]+(\.[0-9]{1,2})?$/;
      if (regex.test(value)) {
        this.setState({no_of_bed_error: ''});
      } else {
        this.setState({
          no_of_bed_error: 'You should provide proper number of bed',
        });
      }
    }
    if (type === 'aircondition') {
      this.setState({airCondition: String(value)});
    }
    if (type === 'wifi') {
      this.setState({wifi: String(value)});
    }
    if (type === 'car_parking') {
      this.setState({car_parking: String(value)});
    }
    if (type === 'basic_service_price_per_hour') {
      this.setState({basic_service_price_per_hour: value});
      this.forceUpdate();
      var regex = /^[0-9]+(\.[0-9]{1,2})?$/;
      if (regex.test(value)) {
        this.setState({basic_service_price_per_hour_error: ''});
      } else {
        this.setState({
          basic_service_price_per_hour_error:
            'Provide the basic service cost of your spa',
        });
      }
    }
  };

  closePopupbox = () => {
    this.setState({
      visible1: false,
    });
    console.log('status', this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate('spaAccount');
    }
  };

  submit = () => {
    console.log('states', this.state.address);

    if (this.state.spa_name == '') {
      this.setState({
        spa_name_error: 'Enter Spa Name',
        popupMsg: 'Enter Spa Name',
        visible1: true,
      });
    } else if (this.state.spa_name_error != '') {
      this.setState({
        popupMsg: 'Enter Spa Name correcyly',
        visible1: true,
      });
    } else if (this.state.email == '') {
      this.setState({
        email_error: 'Enter contact email',
        popupMsg: 'Enter contact email',
        visible1: true,
      });
    } else if (this.state.email_error != '') {
      this.setState({
        popupMsg: 'Enter contact email',
        visible1: true,
      });
    } else if (this.state.phone == '') {
      this.setState({
        phone_error: 'Enter contact number',
        popupMsg: 'Enter contact number',
        visible1: true,
      });
    } else if (this.state.phone_error != '') {
      this.setState({
        popupMsg: 'Enter correct contact number',
        visible1: true,
      });
    } else if (this.state.latitude == '') {
      this.setState({
        address_error: 'Select Proper Spa address',
        popupMsg: 'Select Proper Spa address',
        visible1: true,
      });
    } else if (this.state.longitude == '') {
      this.setState({
        address_error: 'Select Proper Spa address',
        popupMsg: 'Select Proper Spa address',
        visible1: true,
      });
    } else if (this.state.address_error != '') {
      this.setState({
        popupMsg: 'Enter Spa address correctly',
        visible1: true,
      });
    } else if (this.state.no_of_bed == '') {
      this.setState({
        no_of_bed_error: 'Enter no of bed in spa',
        popupMsg: 'Enter no of bed in spa',
        visible1: true,
      });
    } else if (this.state.no_of_bed_error != '') {
      this.setState({
        popupMsg: 'Enter no of bed correcyly',
        visible1: true,
      });
    } else if (this.state.basic_service_price_per_hour == '') {
      this.setState({
        basic_service_price_per_hour_error: 'Enter basic service cost',
        popupMsg: 'Enter car parking cost',
        visible1: true,
      });
    } else if (this.state.basic_service_price_per_hour_error != '') {
      this.setState({
        popupMsg: 'Enter basic service cost correcyly',
        visible1: true,
      });
    } else {
      console.log('this.state in edited spa data', this.state);

      let userDetails = new FormData();
      userDetails.append('spa_owner_id', this.state.spa_owner_id);
      userDetails.append(
        'logo_image',
        this.state.logo_image ? this.state.logo_image : '',
      );
      userDetails.append(
        'name',
        this.state.spa_name ? this.state.spa_name : this.props.spadetails.name,
      );
      userDetails.append(
        'email',
        this.state.email ? this.state.email : this.props.spadetails.email,
      );
      userDetails.append(
        'phone',
        this.state.phone ? this.state.phone : this.props.spadetails.phone,
      );
      userDetails.append(
        'address',
        this.state.destination
          ? this.state.destination
          : this.props.spadetails.address,
      );
      userDetails.append(
        'description',
        this.state.description
          ? this.state.description
          : this.props.spadetails.description,
      );
      userDetails.append(
        'no_of_bed',
        this.state.no_of_bed
          ? this.state.no_of_bed
          : this.props.spadetails.no_of_bed,
      );
      userDetails.append(
        'is_aircondition_available',
        this.state.airCondition == null
          ? this.props.spadetails.is_aircondition_available
          : this.state.airCondition,
      );
      userDetails.append(
        'is_wifi_available',
        this.state.wifi == null
          ? this.props.spadetails.is_wifi_available
          : this.state.wifi,
      );
      userDetails.append(
        'is_parking_available',
        this.state.car_parking == null
          ? this.props.spadetails.is_parking_available
          : this.state.car_parking,
      );
      userDetails.append(
        'basic_service_price_per_hour',
        this.state.basic_service_price_per_hour
          ? this.state.basic_service_price_per_hour
          : this.props.spadetails.basic_service_price_per_hour,
      );
      userDetails.append('lattitude', this.state.latitude);
      userDetails.append('longitude', this.state.longitude);
      console.log('send edited spa data', userDetails);
      this.setState({spinner: true});
      this.props.spaEdit(userDetails);
    }
  };

  render() {
    console.log('states in render', this.state);
    console.log('PROPS in render', this.props);
    var logo;

    {
      this.state.image_source
        ? (logo = this.state.image_source)
        : (logo = this.props.spadetails.logo_image);
    }
    const locationPredictions = this.state.locationPredictions.map(
      prediction => (
        <TouchableHighlight
          key={prediction.id}
          onPress={() => this.pressedPrediction(prediction)}>
          <Text style={styles.locationSuggestion}>
            {prediction.description}
          </Text>
        </TouchableHighlight>
      ),
    );
    // var selected_car_parking = parseInt(
    //   this.props.spadetails.is_parking_available,
    // );
    // console.log(selected_car_parking);
    // //console.log(this.props.spadetails.logo_image);

    const {goBack} = this.props.navigation;

    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Spa Details</Text>
          </View>
        </View>

        <ScrollView style={{flex: 1}}>
          <View style={{flex: 1, paddingBottom: 60}}>
            <ImageBackground
              source={require('../../assets/images/pbg.png')}
              style={styles.tprofile}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingBottom: 70,
                }}>
                <View style={{position: 'relative', top: 25}}>
                  <Text style={[styles.h3, {color: '#fff'}]}>
                    {this.props.spadetails.name}
                  </Text>
                </View>

                <View style={styles.custsec}>
                  {logo ? (
                    <Avatar
                      rounded
                      source={{uri: `${logo}`}}
                      onPress={this.handlechoosePhoto}
                      size={90}
                      containerStyle={styles.custavtr}
                    />
                  ) : (
                    <Avatar
                      rounded
                      source={require('../../assets/images/download.png')}
                      onPress={this.handlechoosePhoto}
                      size={90}
                      containerStyle={styles.custavtr}
                    />
                  )}
                </View>
              </View>
            </ImageBackground>
            <Avatar
              rounded
              source={require('../../assets/images/download.png')}
              onPress={this.handlechoosePhoto}
              size={90}
              containerStyle={styles.custavtr}
            />

            <Spinner
              visible={this.state.spinner}
              textContent={'Loading...'}
              textStyle={styles.spinnerTextStyle}
            />
            <Dialog
              visible={this.state.visible1}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: 'bottom',
                })
              }
              onTouchOutside={() => {
                this.closePopupbox();
              }}
              dialogStyle={{width: '80%'}}
              footer={
                <DialogFooter>
                  <DialogButton
                    textStyle={{
                      fontSize: 14,
                      color: '#333',
                      fontWeight: '700',
                    }}
                    text="OK"
                    onPress={() => {
                      this.closePopupbox();
                    }}
                  />
                </DialogFooter>
              }>
              <DialogContent>
                <Text style={styles.popupText}>{this.state.popupMsg}</Text>
              </DialogContent>
            </Dialog>

            <View style={[styles.graycontainer, styles.formgrey]}>
              <View style={styles.graycard}>
                <TextField
                  label="Spa Name"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  // editable={false}
                  defaultValue={this.props.spadetails.name}
                  onChangeText={value => this.validate(value, 'spa_name')}
                />
                <Text style={styles.error}>{this.state.spa_name_error}</Text>

                <TextField
                  label="Email"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  defaultValue={this.props.spadetails.email}
                  keyboardType={'email-address'}
                  onChangeText={value => this.validate(value, 'email')}
                />
                <Text style={styles.error}>{this.state.email_error}</Text>

                <View style={{borderBottomColor: '#838389' , borderBottomWidth: 1, paddingBottom :8}}>
                <Text style={{paddingBottom: 8, color: '#838389', fontSize: 18 }}>Phone</Text>
                <TextInputMask
                  type={'cel-phone'}
                  options={{
                    maskType: 'BRL',
                    withDDD: true,
                    dddMask: '(999) 999-9999 '
                  }}
                  maxLength={14}
                  style={{color: '#838389'}}
                  value={this.state.phone}
                  onChangeText={value => this.validate(value, 'phone')}
                />
                </View>
               
                <Text style={styles.error}>{this.state.phone_error}</Text>

                <Text style={{color: '#838389', fontSize: 16}}>
                  Business Location
                </Text>
                <TextInput
                  label="Address"
                  //textColor={'#838389'}
                  //baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  placeholder="Enter location.."
                  style={styles.destinationInput}
                  defaultValue={this.props.spadetails.address}
                  onChangeText={destination => {
                    this.setState({destination});
                    this.onChangeDestinationDebounced(destination);
                  }}
                  value={this.state.destination}
                />
                {locationPredictions}
                <Text style={styles.error}>{this.state.address_error}</Text>

                <TextField
                  label="Spa Description"
                  multiline={true}
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  defaultValue={this.props.spadetails.description}
                  onChangeText={value => this.validate(value, 'description')}
                />

                <TextField
                  label="Number of Beds"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  keyboardType={'numeric'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  defaultValue={this.props.spadetails.no_of_bed}
                  onChangeText={value => this.validate(value, 'no_of_bed')}
                />
                <Text style={styles.error}>{this.state.no_of_bed_error}</Text>

                <View style={styles.custtxt}>
                  <Text style={styles.h3}>Air Conditioning</Text>
                  <View style={{marginTop: 20}}>
                    <RadioForm
                      radio_props={radio_props}
                      initial={Number(
                        this.props.spadetails.is_aircondition_available,
                      )}
                      formHorizontal={true}
                      labelHorizontal={true}
                      buttonColor={'#3e3e49'}
                      buttonOuterColor={'#3e3e49'}
                      selectedButtonColor={'#e25cff'}
                      labelStyle={{
                        color: '#fefefe',
                        fontSize: 15,
                        marginRight: 15,
                      }}
                      buttonStyle={{
                        borderWidth: 1,
                      }}
                      buttonSize={10}
                      animation={true}
                      onPress={value => {
                        this.validate(value, 'aircondition');
                      }}
                    />
                  </View>
                </View>

                <Text style={styles.error}>
                  {this.state.aircondition_error}
                </Text>

                <View style={styles.custtxt}>
                  <Text style={styles.h3}>Wifi Connection</Text>
                  <View style={{marginTop: 20}}>
                    <RadioForm
                      radio_props={radio_props}
                      initial={Number(this.props.spadetails.is_wifi_available)}
                      formHorizontal={true}
                      labelHorizontal={true}
                      buttonColor={'#3e3e49'}
                      buttonOuterColor={'#3e3e49'}
                      selectedButtonColor={'#e25cff'}
                      labelStyle={{
                        color: '#fefefe',
                        fontSize: 15,
                        marginRight: 15,
                      }}
                      buttonStyle={{
                        borderWidth: 1,
                      }}
                      buttonSize={10}
                      animation={true}
                      onPress={value => {
                        this.validate(value, 'wifi');
                      }}
                    />
                  </View>
                </View>

                <Text style={styles.error}>{this.state.wifi_error}</Text>

                <View style={styles.custtxt}>
                  <Text style={styles.h3}>Car Parking</Text>
                  <View style={{marginTop: 20}}>
                    <RadioForm
                      radio_props={radio_props}
                      initial={Number(
                        this.props.spadetails.is_parking_available,
                      )}
                      formHorizontal={true}
                      labelHorizontal={true}
                      buttonColor={'#3e3e49'}
                      buttonOuterColor={'#3e3e49'}
                      selectedButtonColor={'#e25cff'}
                      labelStyle={{
                        color: '#fefefe',
                        fontSize: 15,
                        marginRight: 15,
                      }}
                      buttonStyle={{
                        borderWidth: 1,
                      }}
                      buttonSize={10}
                      animation={true}
                      onPress={value => {
                        this.validate(value, 'car_parking');
                      }}
                    />
                  </View>
                </View>

                <Text style={styles.error}>{this.state.car_parking_error}</Text>

                <View>
                  <TextField
                    label="Basic Service Cost Per Hour"
                    textColor={'#838389'}
                    baseColor={'#838389'}
                    labelFontSize={18}
                    keyboardType={'numeric'}
                    activeLineWidth={1}
                    defaultValue={
                      this.props.spadetails.basic_service_price_per_hour
                    }
                    onChangeText={value =>
                      this.validate(value, 'basic_service_price_per_hour')
                    }
                  />

                  <Text style={styles.error}>
                    {this.state.basic_service_price_per_hour_error}
                  </Text>
                </View>
              </View>

              <TouchableOpacity
                style={[styles.pinkbtn,{marginTop: 80}]}
                onPress={() => this.submit()}>
                <Text style={styles.btntext}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner,
});
export default connect(mapStateToProps, {fetchSpaDetails, spaEdit})(
  spaInformation,
);
