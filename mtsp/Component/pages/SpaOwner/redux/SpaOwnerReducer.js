import {
  FETCH_SPA_DETAILS,
  FETCH_SPA_OWNER_DETAILS,
  EDIT_SPA_INFORMATION,
  EDIT_SPA_OWNER_INFORMATION,
  FETCH_TRANSACTION_HISTORY,
  FETCH_APPOINTMENT_HISTORY,
  FETCH_UPCOMMING_APPOINTMENT,
  FETCH_MASSAUSE,
  DELETE_MASSAUSE,
  FETCH_SERVICE_LISTING,
  FETCH_SELECTED_SERVICE,
  SELECTED_SERVICE_ARRAY,
  SPA_OWNER_UPDATE_SERVICE,
  EACH_MASSEUSE_DETAILS,
  EDIT_MASSEUSE,
  REGISTER_MASSEUSE,
  SPA_GALLARY_INSERT_IMAGES,
  SPA_GALLARY_FETCH_IMAGES,
  SPA_GALLARY_DELETE_IMAGE,
  FETCH_SPA_TIMING,
  SPA_TIMING_ARRAY,
  EDIT_SPA_TIME_SCHEDULE,
  ADD_SPA_HOLIDAY,
  STRIPE_ACCOUNT_DETAILS,
  COMPLETE_APPOINTMENT_LIST,
  CANCEL_BOOKING_LIST,
  COMPLETE_TRANSFER,
  INPUT_SPA_DISCOUNT,
  ADD_SPECIAL_DISCOUNT,
  COMPLETE_PAYMENT_CASH
} from '../../../../redux/actions/types';

const intialState = {
  error: '',
  transactionHistory: {},
  appointmentHistory: {},
  upcommingAppointment: {},
  massauseList: [],
  masseusedelete: {},
  spaownerdetails: {},
  spadetails: {},
  spaeditinformation: {},
  spaOwnerEdit: {},
  allServices: {},
  selectedService: {},
  selectedServiceDetails: {},
  spaOwnerUpdateServices: {},
  masseusedetails: {},
  masseuseEditDetails: {},
  registerMasseuse: {},
  spaImageList: {},
  spaImageInsert: {},
  deleteGalleryImage: {},
  spaTimeDetails: {},
  spaTimingArrayDetails: {},
  editSpaTimeSchedule: {},
  addSpaHoliday: {},
  stripeAddAcoount: {},
  cancelledAppointmentList: {},
  completeAppointmentList: {},
  completeTransfer: {},
  spadiscount: {},
  specialOffer: {},
  completePayCash: {}
};

export default function(state = intialState, action) {
  switch (action.type) {
    case INPUT_SPA_DISCOUNT:
      return {
        ...state,
        spadiscount: action.payload,
        error: action.payload.response,
      };
      case ADD_SPECIAL_DISCOUNT:
      return {
        ...state,
        specialOffer: action.payload,
        error: action.payload.response,
      };
    case FETCH_SPA_TIMING:
      return {
        ...state,
        spaTimeDetails: action.payload,
        error: action.payload.response,
      };
    case SPA_GALLARY_DELETE_IMAGE:
      return {
        ...state,
        deleteGalleryImage: action.payload,
        error: action.payload.response,
      };
    case SPA_GALLARY_INSERT_IMAGES:
      return {
        ...state,
        spaImageInsert: action.payload,
        error: action.payload.response,
      };
      case SPA_GALLARY_FETCH_IMAGES:
      return {
        ...state,
        spaImageList: action.payload,
        error: action.payload.response,
      };
    case REGISTER_MASSEUSE:
      return {
        ...state,
        registerMasseuse: action.payload,
        error: action.payload.response,
      };
    case EACH_MASSEUSE_DETAILS:
      return {
        ...state,
        masseusedetails: action.payload,
        error: action.payload.response,
      };
          
      case EDIT_MASSEUSE:
      return {
        ...state,
        masseuseEditDetails: action.payload,
        error: action.payload.response,
      };
    case SPA_OWNER_UPDATE_SERVICE:
      return {
        ...state,
        spaOwnerUpdateServices: action.payload,
      };
    case SELECTED_SERVICE_ARRAY:
      return {
        ...state,
        selectedServiceDetails: action.payload,
      };
      case SPA_TIMING_ARRAY:
      return {
        ...state,
        spaTimingArrayDetails: action.payload,
      };

    case FETCH_SPA_DETAILS:
      return {
        ...state,
        spadetails: action.payload,
        error: action.payload.response,
      };

    case FETCH_SERVICE_LISTING:
      return {
        ...state,
        allServices: action.payload,
        error: action.payload.response,
      };

    case FETCH_SELECTED_SERVICE:
      return {
        ...state,
        selectedService: action.payload,
        error: action.payload.response,
      };

    case FETCH_SPA_OWNER_DETAILS:
      return {
        ...state,
        spaownerdetails: action.payload,
        error: action.payload.response,
      };

    case EDIT_SPA_INFORMATION:
      return {
        ...state,
        spaeditinformation: action.payload,
        error: action.payload.response,
      };

    case EDIT_SPA_OWNER_INFORMATION:
      return {
        ...state,
        spaOwnerEdit: action.payload,
        error: action.payload.response,
      };

    case FETCH_TRANSACTION_HISTORY:
      return {
        ...state,
        transactionHistory: action.payload,
        error: action.payload.response,
      };

    case FETCH_APPOINTMENT_HISTORY:
      return {
        ...state,
        appointmentHistory: action.payload,
        error: action.payload.response,
      };

    case FETCH_UPCOMMING_APPOINTMENT:
      return {
        ...state,
        upcommingAppointment: action.payload,
        error: action.payload.response,
      };

      case COMPLETE_APPOINTMENT_LIST:
      return {
        ...state,
        completeAppointmentList: action.payload,
        error: action.payload.response,
      };

      case COMPLETE_TRANSFER:
      return {
        ...state,
        completeTransfer: action.payload,
        error: action.payload.response,
      };

      case COMPLETE_PAYMENT_CASH:
      return {
        ...state,
        completePayCash: action.payload,
        error: action.payload.response,
      };

      case CANCEL_BOOKING_LIST:
      return {
        ...state,
        cancelledAppointmentList: action.payload,
        error: action.payload.response,
      };

      case EDIT_SPA_TIME_SCHEDULE:
      return {
        ...state,
        editSpaTimeSchedule: action.payload,
        error: action.payload.response,
      };
      case ADD_SPA_HOLIDAY:
      return {
        ...state,
        addSpaHoliday: action.payload,
        error: action.payload.response,
      };

    //Massause
    case FETCH_MASSAUSE:
      return {
        ...state,
        massauseList: action.payload,
        error: action.payload.response,
      };

    case DELETE_MASSAUSE:
      return {
        ...state,
        masseusedelete: action.payload,
        error: action.payload.response,
      };

      case STRIPE_ACCOUNT_DETAILS:
      return {
        ...state,
        stripeAddAcoount: action.payload,
        error: action.payload.response,
      };

    default:
      return state;
  }
}
