import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert
} from "react-native";
import { Avatar, Icon, CheckBox } from "react-native-elements";
import styles from "./style";
import { connect } from "react-redux";
import { masseuseUpcommingAppointment } from "./redux/MasseuseAction";
import Spinner from "react-native-loading-spinner-overlay";
import moment from "moment";

class MasseuseUpcommingAppointment extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      masseuse_id: "",
      spinner: true
    };
  }

  //--FETCH UPCOMMING APPOINTMENT THROUGH REDUX--//
  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem("login_id");
      console.log("loginId in myaccount page", value);
      if (value) {
        this.setState({ masseuse_id: value });
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
    let todayDate = moment(new Date()).format("YYYY-MM-DD");
    let userDetails = new FormData();
    userDetails.append("masseuse_id", this.state.masseuse_id);
    userDetails.append("todayDate", todayDate);
    this.props.masseuseUpcommingAppointment(userDetails);
    console.log("userDetails",userDetails);
    
  }

  componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps", this.props);
    console.log("componentWillReceiveProps", nextProps);

    this.setState({ spinner: false });
  }

  render() {
    console.log("props in render in upcomming appointment", this.props);
    console.log(
      "props in render in upcomming appointment",
      this.props.upcommingAppointment
    );

    //const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("MasseuseAccount")}
            >
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Upcoming Appointments</Text>
          </View>
        </View>
        <ScrollView>
          {this.props.error && this.props.error !== undefined ? (
            Alert.alert("Something went wrong")
          ) : (
            <View style={styles.graycontainer}>
              {this.props.upcommingAppointment.length ? (
                this.props.upcommingAppointment.map((item, key) => (
                  <View key={key} style={styles.graycard}>
                    <View
                      style={[styles.media, { marginBottom: 0, width: "90%" }]}
                    >
                      {/* <Avatar
                        rounded
                        source={require("../../assets/images/avt3.png")}
                        size={60}
                        containerStyle={{
                          borderWidth: 3,
                          borderColor: "#676782",
                          padding: 3,
                          backgroundColor: "#fff"
                        }}
                      /> */}
                        {item.profile_image != "" ? (
                            <Avatar
                              rounded
                              source={{ uri: `${item.profile_image}` }}
                              size={60}
                              containerStyle={{
                                borderWidth: 3,
                                borderColor: "#676782",
                                padding: 3,
                                backgroundColor: "#fff"
                              }}
                            />
                          ) : (
                            <Avatar
                              rounded
                              source={require("../../assets/images/avt3.png")}
                              size={60}
                              containerStyle={{
                                borderWidth: 3,
                                borderColor: "#676782",
                                padding: 3,
                                backgroundColor: "#fff"
                              }}
                            />
                        )}
                      <View style={styles.mediabody}>
                        <Text style={styles.h3}>{item.spa_name}</Text>
                        <View
                          style={[
                            styles.justifyrow,
                            { marginTop: 5, flexWrap: "wrap" }
                          ]}
                        >
                          <View style={styles.justifyrow}>
                                <Image
                                  source={require("../../assets/images/name.png")}
                                  resizeMode="contain"
                                  style={{ width: 14, marginRight: 5 }}
                                />
                                <Text style={styles.mutetext2}>
                                  Customer Name: {item.customer_name}
                                </Text>
                          </View>
                          <View style={styles.justifyrow}>
                            <Image
                              source={require("../../assets/images/calender.png")}
                              resizeMode="contain"
                              style={{ width: 14, marginRight: 5 }}
                            />
                            <Text style={styles.mutetext2}>
                              {item.date}
                            </Text>
                          </View>
                          <View style={styles.justifyrow}>
                            <Image
                              source={require("../../assets/images/clock.png")}
                              resizeMode="contain"
                              style={{ width: 14, marginRight: 5 }}
                            />
                            <Text style={styles.mutetext2}>
                              {item.booking_start_time}
                            </Text>
                          </View>
                          <Spinner
                            visible={this.state.spinner}
                            textContent={"Loading..."}
                            textStyle={styles.spinnerTextStyle}
                          />
                          <View style={styles.justifyrow}>
                            <Image
                              source={require("../../assets/images/clock.png")}
                              resizeMode="contain"
                              style={{ width: 14, marginRight: 5 }}
                            />
                            <Text style={styles.mutetext2}>
                              Duration: {item.total_duration} min
                            </Text>
                          </View>
                          <View style={styles.justifyrow}>
                            <Image
                              source={require("../../assets/images/setting.png")}
                              resizeMode="contain"
                              style={{ width: 14, marginRight: 5 }}
                            />
                            <Text style={styles.mutetext2}>
                              {item.services}
                            </Text>
                          </View>
                          {item.tips >= 1 ? (
                            <View style={styles.justifyrow}>
                            <Image
                              source={require("../../assets/images/aicon6.png")}
                              resizeMode="contain"
                              style={{ width: 14, marginRight: 5 }}
                            />
                            <Text style={styles.mutetext2}>
                            Tips: ${item.tips}
                            </Text>
                          </View>
                          ) : null}
                          
                        </View>
                      </View>
                    </View>
                  </View>
                ))
              ) : (
                <View style={styles.justifyrow}>
                  <Text style={styles.backheading}>
                    No Upcomming Appointments Record Found
                  </Text>
                </View>
              )}
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.Masseuse
});
export default connect(mapStateToProps, { masseuseUpcommingAppointment })(
  MasseuseUpcommingAppointment
);
