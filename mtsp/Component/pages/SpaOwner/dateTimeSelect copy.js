import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  Alert,
  Picker,
  KeyboardAvoidingView,
  AsyncStorage,
} from 'react-native';
import Spinner from "react-native-loading-spinner-overlay";
import {Avatar, Icon, CheckBox, Input} from 'react-native-elements';
import styles from './style';
import {setSpaTimingArray, spaTimeScheduleEdit} from './redux/SpaOwnerAction';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import {connect} from 'react-redux';
import TimePicker from 'react-native-simple-time-picker';

class dateTimeSelect extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      spa_owner_id: '',
      time: '10:00',
      //   spinner: false,
      visible1: false,
      popupMsg: '',
    };
    console.log('constructor', this.props);
    //this.myArray();
  }
  // myArray = () => {
  //   console.log("myArray",this.props.spaTimingArrayDetails);
  //   debugger
  //   this.setState({
  //     day: this.props.spaTimingArrayDetails,
  //   })

  //   console.log("day",this.state.day);

  //   debugger
  // }
  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem('login_id');
      if (value) {
        this.setState({spa_owner_id: value});
      }
    } catch (error) {
      console.log(error);
    }
    console.log('spa_owner_id state', this.state.spa_owner_id);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.spaTimingArrayDetails) {
      return {
        day: props.spaTimingArrayDetails,
      };
    }
    return null;
  }

  componentDidUpdate(prevProps) {
    console.log('componentDidUpdate', this.props);
    if (prevProps.editSpaTimeSchedule !== this.props.editSpaTimeSchedule) {
      this.setState({spinner: false});
      const addDetails = this.props.editSpaTimeSchedule;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({visible1: true, popupMsg: addDetails.msg});
        }
        //this.setState({spinner: false});
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1,
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg,
            });
          }
        } else {
          // this.setState({
          //   spinner: false,
          // });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: 'Something went wrong. Please try again later.',
            });
          }
        }
      }
    }
  }

  closePopupbox = () => {
    this.setState({
      visible1: false,
    });
    console.log('status', this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate('spaAccount');
    }
  };

  submit = () => {
    console.log('submit', Object.keys(this.state.day).length);
    console.log("submit array",this.state.day);
    
    var i;
    var array = this.state.day;

    for (i = 0; i < Object.keys(this.state.day).length; i++) {
      console.log('array', i);
      if (array[i].is_check == '1') {
        if (array[i].selectedCloseHours == '00') {
          Alert.alert('Enter valid time for' + ' ' + array[i].dayName);
          break;
        }
        if (array[i].selectedOpenHours == '00') {
          Alert.alert('Enter valid time for' + ' ' + array[i].dayName);
          break;
        }
        if (array[i].selectedCloseHours <= array[i].selectedOpenHours) {
          Alert.alert('Enter valid time for' + ' ' + array[i].dayName);
          break;
        }
      }
    }
    console.log('array i', i);

    if (i == Object.keys(this.state.day).length) {
      let userDetails = new FormData();
      userDetails.append('spa_owner_id', this.state.spa_owner_id);
      userDetails.append('timing_array', JSON.stringify(this.state.day));
      this.setState({spinner: true})
      this.props.spaTimeScheduleEdit(userDetails);

      console.log('final array2', userDetails);
    }
  };

  checkboxCheck = (key, check) => {
    console.log('previous', this.state.day);
    console.log('check', check);

    var dayArrayUpdate = this.state.day;

    if (check == 1) {
      check = 0;
    } else {
      check = 1;
    }

    dayArrayUpdate[key].is_check = check;
    console.log('checked value', dayArrayUpdate[key].is_check);

    this.setState({
      day: dayArrayUpdate,
    });

    console.log('update', dayArrayUpdate);
  };

  openTiming = (hours, minutes, key) => {
    console.log('hours', hours);
    console.log('minutes', minutes);
    console.log('key', key);
    var dayArrayUpdate = this.state.day;
    dayArrayUpdate[key].selectedOpenHours = hours;
    dayArrayUpdate[key].selectedOpenMinutes = minutes;
    this.setState({
      day: dayArrayUpdate,
    });
  };

  closeTiming = (hours, minutes, key) => {
    console.log('hours', hours);
    console.log('minutes', minutes);
    console.log('key', key);
    var dayArrayUpdate = this.state.day;
    dayArrayUpdate[key].selectedCloseHours = hours;
    dayArrayUpdate[key].selectedCloseMinutes = minutes;
    this.setState({
      day: dayArrayUpdate,
    });
  };

  render() {
    console.log('props in render', this.props);
    console.log('render state time page', this.state.day);
    console.log('render state time page', Object.values(this.state.day));

    const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Add Spa Hours</Text>
          </View>
        </View>
        <Spinner
              visible={this.state.spinner}
              textContent={"Loading..."}
              textStyle={styles.spinnerTextStyle}
            />
        <Dialog
          visible={this.state.visible1}
          dialogAnimation={
            new SlideAnimation({
              slideFrom: 'bottom',
            })
          }
          onTouchOutside={() => {
            this.closePopupbox();
          }}
          dialogStyle={{width: '80%'}}
          footer={
            <DialogFooter>
              <DialogButton
                textStyle={{
                  fontSize: 14,
                  color: '#333',
                  fontWeight: '700',
                }}
                text="OK"
                onPress={() => {
                  this.closePopupbox();
                }}
              />
            </DialogFooter>
          }>
          <DialogContent>
            <Text style={styles.popupText}>{this.state.popupMsg}</Text>
          </DialogContent>
        </Dialog>
        <KeyboardAvoidingView enabled behavior="position">
          <ScrollView>
            <View style={[styles.graycontainer, {paddingBottom: 80}]}>
              {Object.values(this.state.day).length ? (
                <View style={styles.graycard}>
                  {Object.values(this.state.day).map((item, key) => {
                    console.log(item.is_check);
                    //console.log(key);

                    return (
                      <View style={styles.graycard}>
                        {item.is_check == 1 ? (
                          <View style={styles.graycard}>
                            <View
                              style={[styles.justifyrow, {marginBottom: 15}]}>
                              <CheckBox
                                title={item.dayName}
                                uncheckedIcon="check-square"
                                checkedIcon="check-square"
                                containerStyle={styles.chkcontainer}
                                textStyle={styles.h22}
                                checkedColor="#e25cff"
                                uncheckedColor="#e25cff"
                                onPress={() =>
                                  this.checkboxCheck(key, item.is_check)
                                }
                              />
                            </View>
                            <View style={{color: '#ffff', justifyContent: 'center', alignItems: 'center', marginBottom: 20}}>
                            <Text style={{color: '#ffff', color: '#e25cff', fontSize: 16, fontWeight: 'bold'}}>Opening Hour</Text>
                              <Text style={{color: '#ffff', fontSize: 20, fontWeight: 'bold', marginTop: 8}}>
                                {item.selectedOpenHours}:
                                {item.selectedOpenMinutes}
                              </Text>
                            </View>
                            
                            <View
                              style={[styles.justifyrow, {marginBottom: 15}]}>
                              <View style={[styles.containerTime, {height: 120}]}>
                              
                                <TimePicker
                                  selectedHours={item.selectedOpenHours}
                                  selectedMinutes={item.selectedOpenMinutes}
                                  onChange={(hours, minutes) =>
                                    this.openTiming(hours, minutes, key)
                                  }
                                />
                              </View>
                            </View>


                            <View style={{color: '#ffff', justifyContent: 'center', alignItems: 'center', marginBottom: 20}}>
                            <Text style={{color: '#ffff', color: '#e25cff', fontSize: 16, fontWeight: 'bold'}}>Closing Hour</Text>
                              <Text style={{color: '#ffff', fontSize: 20, fontWeight: 'bold', marginTop: 8}}>
                                {item.selectedCloseHours}:
                                {item.selectedCloseMinutes}
                              </Text>
                            </View>
                            <View
                              style={[styles.justifyrow, {marginBottom: 15}]}>
                              <View style={[styles.containerTime, {height: 120}]}>
                                <TimePicker
                                  selectedHours={item.selectedCloseHours}
                                  selectedMinutes={
                                    item.selectedCloseMinutes
                                  }
                                  onChange={(hours, minutes) =>
                                    this.closeTiming(hours, minutes, key)
                                  }
                                />
                              </View>
                            </View>
                          </View>
                        ) : (
                          <View style={[styles.justifyrow, {marginBottom: 15}]}>
                            <CheckBox
                              title={item.dayName}
                              containerStyle={styles.chkcontainer}
                              textStyle={styles.h22}
                              checkedColor="#e25cff"
                              uncheckedColor="#e25cff"
                              onPress={() =>
                                this.checkboxCheck(key, item.is_check)
                              }
                            />
                          </View>
                        )}
                      </View>
                    );
                  })}
                </View>
              ) : null}

              <TouchableOpacity
                style={styles.pinkbtn}
                onPress={() => this.submit()}>
                <Text style={styles.btntext}>Submit</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner,
});
export default connect(mapStateToProps, {
  setSpaTimingArray,
  spaTimeScheduleEdit,
})(dateTimeSelect);
