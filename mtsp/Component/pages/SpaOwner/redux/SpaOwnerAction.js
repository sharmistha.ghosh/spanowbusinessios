import {
  FETCH_SPA_DETAILS,
  FETCH_SPA_OWNER_DETAILS,
  EDIT_SPA_INFORMATION,
  EDIT_SPA_OWNER_INFORMATION,
  FETCH_TRANSACTION_HISTORY,
  FETCH_APPOINTMENT_HISTORY,
  FETCH_UPCOMMING_APPOINTMENT,
  FETCH_MASSAUSE,
  DELETE_MASSAUSE,
  FETCH_SERVICE_LISTING,
  FETCH_SELECTED_SERVICE,
  SELECTED_SERVICE_ARRAY,
  SPA_OWNER_UPDATE_SERVICE,
  EACH_MASSEUSE_DETAILS,
  EDIT_MASSEUSE,
  REGISTER_MASSEUSE,
  SPA_GALLARY_INSERT_IMAGES,
  SPA_GALLARY_FETCH_IMAGES,
  SPA_GALLARY_DELETE_IMAGE,
  FETCH_SPA_TIMING,
  SPA_TIMING_ARRAY,
  EDIT_SPA_TIME_SCHEDULE,
  ADD_SPA_HOLIDAY,
  STRIPE_ACCOUNT_DETAILS,
  CANCEL_BOOKING_LIST,
  COMPLETE_APPOINTMENT_LIST,
  COMPLETE_TRANSFER,
  INPUT_SPA_DISCOUNT,
  ADD_SPECIAL_DISCOUNT,
  COMPLETE_PAYMENT_CASH
} from '../../../../redux/actions/types';
import Api from '../../../../Api/Api';

//----Input discount----//
export const inputDiscount = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/addDiscount',
      userDetails,
    );

    if (response) {
      console.log("spa_owner_tiinputDiscountming",response);
      dispatch({type: INPUT_SPA_DISCOUNT, payload: response});
    }
  } catch (error) {
    console.log("spa_owner_timing_ERROR",error);
    dispatch({type: INPUT_SPA_DISCOUNT, payload: {response: error}});
  }
};

//----add special discount or offer----//
export const addSpecialDiscount = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/addSpecialDiscount',
      userDetails,
    );

    if (response) {
      console.log("addSpecialDiscount action",response);
      dispatch({type: ADD_SPECIAL_DISCOUNT, payload: response});
    }
  } catch (error) {
    console.log("spa_owner_timing_ERROR",error);
    dispatch({type: ADD_SPECIAL_DISCOUNT, payload: {response: error}});
  }
};

//----FETCH SPA TIMING----//
export const fetchSpaTiming = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/spasTiming',
      userDetails,
    );

    if (response) {
      console.log("spa_owner_timing",response);
      dispatch({type: FETCH_SPA_TIMING, payload: response.spa_timing});
    }
  } catch (error) {
    console.log("spa_owner_timing_ERROR",error);
    dispatch({type: FETCH_SPA_TIMING, payload: {response: error}});
  }
};

//----FETCH SPA DETAILS----//
export const fetchSpaDetails = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/spaProfileView',
      userDetails,
    );

    if (response) {
      console.log("spa_owner_details",response);
      dispatch({type: FETCH_SPA_DETAILS, payload: response.profile_data});
    }
  } catch (error) {
    console.log("spa_owner_details_ERROR",error);
    dispatch({type: FETCH_SPA_DETAILS, payload: {response: error}});
  }
};

//----FETCH SPA OWNER DETAILS----//
export const fetchSpaOwnerDetails = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/userProfileView',
      userDetails,
    );

    if (response) {
      console.log("spa_owner_details",response);
      dispatch({type: FETCH_SPA_OWNER_DETAILS, payload: response.profile_data});
    }
  } catch (error) {
    console.log("spa_owner_details_ERROR",error);
    dispatch({type: FETCH_SPA_OWNER_DETAILS, payload: {response: error}});
  }
};

//----EDIT SPA INFORMATION----//
export const spaEdit = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/spaEditInformation',
      userDetails,
    );

    if (response) {
      console.log("spa_edit_information",response);
      dispatch({type: EDIT_SPA_INFORMATION, payload: response});
    }
  } catch (error) {
    console.log("spa_edit_information_ERROR",error);
    dispatch({type: EDIT_SPA_INFORMATION, payload: {response: error}});
  }
};

//----EDIT SPA OWNER INFORMATION----//
export const spaOwnerProfileEdit = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/userUpdateProfile',
      userDetails,
    );

    if (response) {
      console.log("spa_owner_edit_information",response);
      
      dispatch({type: EDIT_SPA_OWNER_INFORMATION, payload: response});
    }
  } catch (error) {
    console.log("spa_owner_edit_information_ERROR",error);
    dispatch({type: EDIT_SPA_OWNER_INFORMATION, payload: {response: error}});
  }
};

//----FETCH TRANSACTION HISTORY OF SPA----//
export const fetchTransactionHistory = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/payments/transactionHistory',
      userDetails,
    );

    if (response) {
      console.log("transaction_history",response);
      dispatch({type: FETCH_TRANSACTION_HISTORY, payload: response.transaction_history});
    }
  } catch (error) {
    console.log("transaction_history_ERROR",error);
    dispatch({type: FETCH_TRANSACTION_HISTORY, payload: {response: error}});
  }
};

//----FETCH APPOINTMENT HISTORY OF SPA----//
export const fetchAppointmentHistory = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/bookings/bookingSpaHistory',
      userDetails,
    );

    if (response) {
      console.log("Appointment_History",response.booking_history);
      dispatch({type: FETCH_APPOINTMENT_HISTORY, payload: response.booking_history});
    }
  } catch (error) {
    console.log("Appointment_History_ERROR",error);
    dispatch({type: FETCH_APPOINTMENT_HISTORY, payload: {response: error}});
  }
};

//----FETCH UPCOMMING APPOINTMENT OF SPA----//
export const fetchUpcommingAppointment = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/bookings/upcomingSpaBookings',
      userDetails,
    );

    if (response) {
      console.log("fetchUpcommingAppointment",response);
      
      dispatch({type: FETCH_UPCOMMING_APPOINTMENT, payload: response.upcoming_bookings});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: FETCH_UPCOMMING_APPOINTMENT, payload: {response: error}});
  }
};

//fetch cancelled appointment list//
export const fetchCancelledAppointments = userdata => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/bookings/spaCancelAppointmentHistory',
      userdata,
    );

    if (response) {
      console.log("cancelBookings",response);
      dispatch({type: CANCEL_BOOKING_LIST, payload: response});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: CANCEL_BOOKING_LIST_ERROR, payload: {response: error}});
  }
};

//fetch cancelled appointment list//
export const completeBookingList = userdata => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/bookings/spaCompleteAppointmentHistory',
      userdata,
    );

    if (response) {
      console.log("CompleteBookings",response);
      dispatch({type: COMPLETE_APPOINTMENT_LIST, payload: response});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: COMPLETE_APPOINTMENT_LIST_ERROR, payload: {response: error}});
  }
};

//complete booking and transfer amount via stripe//
export const bookingCompleteTransfer = userdata => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/payments/stripeConnectedCAccountTransfer',
      userdata,
    );

    if (response) {
      console.log("CompleteBookings",response);
      dispatch({type: COMPLETE_TRANSFER, payload: response});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: COMPLETE_TRANSFER_ERROR, payload: {response: error}});
  }
};

//complete payment in case of cash payment//
export const completePaymentCash = userdata => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/indirect_payment',
      userdata,
    );

    if (response) {
      console.log("Completepayment====",response);
      dispatch({type: COMPLETE_PAYMENT_CASH, payload: response});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: COMPLETE_PAYMENT_CASH, payload: {response: error}});
  }
};

//----FETCH ALL SERVICES ACTIVATED BY ADMIN----//
export const fetchServiceListing = () => async dispatch => {
  try {
    const response = await Api.postApi('webservice/users/getService', '');
    console.log("redux",response);
    if (response) {
      dispatch({type: FETCH_SERVICE_LISTING, payload: response.service_list});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: FETCH_SERVICE_LISTING_ERROR, payload: {response: error}});
  }
};

//----FETCH SELECTED SERVICES OF SPA IN SPA PORTAL----//
export const fetchSelectedService = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/getSelectedService',
      userDetails,
    );

    if (response) {
      console.log("in redux",response);
      
      dispatch({type: FETCH_SELECTED_SERVICE, payload: response.service_list});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: FETCH_SELECTED_SERVICE, payload: {response: error}});
  }
};

//----SET SELECTED SERVICES ARRAY IN SPA OWNER PORTAL----//
export const setSelectedServiceArray = serviceDetails => async dispatch => {
  dispatch({type:SELECTED_SERVICE_ARRAY,payload:{...serviceDetails}})
};

//----SET SPA TIMING ARRAY IN SPA OWNER PORTAL----//
export const setSpaTimingArray = serviceDetails => async dispatch => {
  dispatch({type:SPA_TIMING_ARRAY,payload:{...serviceDetails}})
};

//----SPA OWNER UPDATE SELECTED SERVICES IN SPA PORTAL----//
export const spaOwnerUpdateService = userDetails => async dispatch => {
  console.log("func");
  try {
    console.log("try");

    const response = await Api.postApi(
      'webservice/users/SpaOwnerEditService',
      userDetails,
    );

    if (response) {
      console.log("try_if");
      console.log("in redux",response);
      dispatch({type: SPA_OWNER_UPDATE_SERVICE, payload: response});
    }
  } catch (error) {
    console.log("error msg----",error);
    dispatch({type: SPA_OWNER_UPDATE_SERVICE, payload: {response: error}});
  }
};

//----SPA OWNER UPDATE SPA TIME SCHEDULE IN SPA PORTAL----//
export const spaTimeScheduleEdit = userDetails => async dispatch => {
  console.log("func");
  try {
    console.log("try");
    const response = await Api.postApi(
      'webservice/users/SpaTimeSchedule',
      userDetails,
    );

    if (response) {
      console.log("try_if");
      console.log("in redux",response);
      dispatch({type: EDIT_SPA_TIME_SCHEDULE, payload: response});
    }
  } catch (error) {
    console.log("error msg----",error);
    dispatch({type: EDIT_SPA_TIME_SCHEDULE, payload: {response: error}});
  }
};

//----HOLIDAY ADD IN SPA PORTAL----//
export const spaHolidayAdd = userDetails => async dispatch => {
  console.log("func");
  try {
    console.log("try");
    const response = await Api.postApi(
      'webservice/users/SpaHolidayAdd',
      userDetails,
    );

    if (response) {
      console.log("try_if");
      console.log("in redux",response);
      dispatch({type: ADD_SPA_HOLIDAY, payload: response});
    }
  } catch (error) {
    console.log("error msg----",error);
    dispatch({type: ADD_SPA_HOLIDAY, payload: {response: error}});
  }
};


//----MASSEUSE RAGISTRATION BY SPA OWNER----//
export const masseuseRagistration = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('users/signupMasseuse', userDetails);
    if (response) {
      console.log("action--------",response);
      dispatch({type: REGISTER_MASSEUSE, payload: {...response, isSignupIn: true}});
    }
  } catch (error) {
    dispatch({type: REGISTER_MASSEUSE, payload: {response: error}, isSignupIn: false});
  }
};

// fetch all Massause
export const fetchMassause = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/masseuse_list_per_spa',
      userDetails,
    );

    if (response) {
      dispatch({type: FETCH_MASSAUSE, payload: response.masseuse_list});
      //console.log("response",response);
    }
   } catch (error) {
     console.log("error", error);
     dispatch({type: FETCH_MASSAUSE, payload: {response: error}});
   }
};

export const deleteMassuse = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/delete_masseuse',
      userDetails,
    );
    if (response) {
      dispatch({type: DELETE_MASSAUSE, payload: response});
    }
   } catch (error) {
     console.log("error", error);
     dispatch({type: DELETE_MASSAUSE, payload: {response: error}});
   }
};

//----FETCH MASSEUSE DETAILS----//
export const masseuseDetails = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/userProfileView',
      userDetails,
    );

    if (response) {
      console.log("EACH_MASSEUSE_DETAILS",response);
      dispatch({type: EACH_MASSEUSE_DETAILS, payload: response.profile_data});
    }
  } catch (error) {
    console.log("EACH_MASSEUSE_DETAILS",error);
    dispatch({type: EACH_MASSEUSE_DETAILS, payload: {response: error}});
  }
};


//----EDIT MASSEUSE INFORMATION----//
export const masseuseEdit = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/userUpdateProfile',
      userDetails,
    );

    if (response) {
      console.log("EDIT_MASSEUSE",response);
      dispatch({type: EDIT_MASSEUSE, payload: response});
    }
  } catch (error) {
    console.log("EDIT_MASSEUSE",error);
    dispatch({type: EDIT_MASSEUSE, payload: {response: error}});
  }
};

//----SPA GALLARY IMAGE INSERT----//
export const spaGallaryInsertImages = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/spaOwnerImageInsert',
      userDetails,
    );

    if (response) {
      console.log("spa_owner_details",response);
      dispatch({type: SPA_GALLARY_INSERT_IMAGES, payload: response});
    }
  } catch (error) {
    console.log("spa_owner_details_ERROR",error);
    dispatch({type: SPA_GALLARY_INSERT_IMAGES, payload: {response: error}});
  }
};

//----FETCH SPA GALLARY IMAGES----//
export const spaGallaryFetchImages = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/fetchSpaOwnerImage',
      userDetails,
    );

    if (response) {
      console.log("image_details",response);
      dispatch({type: SPA_GALLARY_FETCH_IMAGES, payload: response});
    }
  } catch (error) {
    console.log("spa_owner_details_ERROR",error);
    dispatch({type: SPA_GALLARY_FETCH_IMAGES, payload: {response: error}});
  }
};

//----DELETE SPA GALLARY IMAGES----//
export const spaGallaryDeleteImages = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/deleteSpaGalleryImage',
      userDetails,
    );

    if (response) {
      console.log("image_delete",response);
      dispatch({type: SPA_GALLARY_DELETE_IMAGE, payload: response});
    }
  } catch (error) {
    console.log("image_delete_ERROR",error);
    dispatch({type: SPA_GALLARY_DELETE_IMAGE, payload: {response: error}});
  }
};

//----SEND STRIPE ACCOUNT DETAILS----//
export const stripeDate = stripeDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/payments/stripeVendorAccountAdd',
      stripeDetails,
    );

    if (response) {
      console.log("STRIPE_ACCOUNT_DETAILS",response);
      dispatch({type: STRIPE_ACCOUNT_DETAILS, payload: response});
    }
  } catch (error) {
    console.log("STRIPE_ACCOUNT_DETAILS_ERROR",error);
    dispatch({type: STRIPE_ACCOUNT_DETAILS, payload: {response: error}});
  }
};