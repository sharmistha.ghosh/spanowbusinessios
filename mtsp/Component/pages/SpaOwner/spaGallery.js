import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  AsyncStorage
} from "react-native";
import { Avatar, Icon, CheckBox } from "react-native-elements";
import styles from "./style";
import ImagePicker from "react-native-image-crop-picker";
import { connect } from "react-redux";
import Spinner from "react-native-loading-spinner-overlay";
import { spaGallaryInsertImages } from "./redux/SpaOwnerAction";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";

class spaGallery extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      photoPath: [],
      imageSource: [],
      spa_owner_id: "",
      imageArray: [],
      visible1: false,
      popupMsg: ""
    };
  }

  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem("login_id");
      console.log("loginId in spaGallery page", value);
      if (value) {
        this.setState({ spa_owner_id: value });
      }
    } catch (error) {
      console.log(error);
    }
  }

  componentDidUpdate(prevProps) {
    console.log("componentDidUpdate", this.props);
    if (prevProps.spaImageInsert !== this.props.spaImageInsert) {
      this.setState({ spinner: false });
      const addDetails = this.props.spaImageInsert;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({ visible1: true, popupMsg: addDetails.msg });
        }
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg
            });
          }
        } else {
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: "Something went wrong. Please try again later."
            });
          }
        }
      }
    }
  }

  uploadImage = () => {
    ImagePicker.openPicker({
      compressImageQuality: 0.57,
      compressImageMaxWidth: 500,
      compressImageMaxHeight: 500,
      multiple: true,
      waitAnimationEnd: false,
      includeBase64: true,
      forceJpg: true
    }).then(images => {
      //console.log('imageupload', images);
      // console.log('imageLength', images.length);
      let Imagedata = [];
      let Imagepath = [];

      for (let i = 0; i < images.length; i++) {
        if (images[i] != "") {
          Imagedata.push(images[i].data);
          Imagepath.push(images[i].path);
        }
      }

      //console.log('Imagedata1', Imagedata);
      //console.log('Imagepath1', Imagepath);

      this.setState({
        ImageSource: Imagedata,
        photoPath: Imagepath,
        imageArray: images
      });
    });
  };

  imageDelete = key => {
    //console.log('key', key);
    // console.log('imageArray', this.state.imageArray);

    for (let i = key; i < this.state.imageArray.length - 1; i++) {
      this.state.imageArray[i] = this.state.imageArray[i + 1];
    }
    //console.log('imageArrayModify', this.state.imageArray);

    let final_arr = [];
    for (let i = 0; i < this.state.imageArray.length - 1; i++) {
      final_arr.push(this.state.imageArray[i]);
    }

    //console.log('imageArrayModify', final_arr);

    let Imagedata = [];
    let Imagepath = [];
    for (let i = 0; i < final_arr.length; i++) {
      if (final_arr[i] != "") {
        Imagedata.push(final_arr[i].data);
        Imagepath.push(final_arr[i].path);
      }
    }
    this.setState({
      ImageSource: Imagedata,
      photoPath: Imagepath,
      imageArray: final_arr
    });
  };

  handleSubmit = () => {
    //console.log("ImageSource",this.state.ImageSource);
    //console.log("photoPath",this.state.photoPath);

    let userDetails = new FormData();
    userDetails.append("spa_owner_id", this.state.spa_owner_id);
    userDetails.append("images_data", JSON.stringify(this.state.ImageSource));
    console.log("userDetails", userDetails);

    this.setState({ spinner: true });
    this.props.spaGallaryInsertImages(userDetails);

    console.log("send image data", this.props.spaImageInsert);
  };

  closePopupbox = () => {
    this.setState({
      visible1: false
    });
    console.log("status", this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate("gallery");
    }
  };

  render() {
    const { goBack } = this.props.navigation;

    console.log("path in render", this.props);
    var img = "/emulated/0/Pictures/29c83f39-eeea-4a4f-bbf4-c84aa1260496.jpg";

    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Spa Gallery</Text>
          </View>
          <TouchableOpacity onPress={this.uploadImage}>
            <Icon
              name="md-add"
              type="ionicon"
              color="#e25cff"
              style={{ fontSize: 24 }}
            />
          </TouchableOpacity>
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={styles.spinnerTextStyle}
        />
        <Dialog
          visible={this.state.visible1}
          dialogAnimation={
            new SlideAnimation({
              slideFrom: "bottom"
            })
          }
          onTouchOutside={() => {
            this.closePopupbox();
          }}
          dialogStyle={{ width: "80%" }}
          footer={
            <DialogFooter>
              <DialogButton
                textStyle={{
                  fontSize: 14,
                  color: "#333",
                  fontWeight: "700"
                }}
                text="OK"
                onPress={() => {
                  this.closePopupbox();
                }}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <Text style={styles.popupText}>{this.state.popupMsg}</Text>
          </DialogContent>
        </Dialog>
        <ImageBackground
          source={require("../../assets/images/spagal.jpg")}
          resizeMode="cover"
          style={styles.spagal}
        >
          {!this.state.photoPath.length ? (
            <ScrollView>
              <TouchableOpacity
                style={styles.justifyadd}
                onPress={this.uploadImage}
              >
                <Avatar
                  rounded
                  source={require("../../assets/images/add.png")}
                  size={100}
                  containerStyle={styles.avtstyle}
                />
              </TouchableOpacity>
              <Text style={styles.uptext}>Please Upload Image</Text>
            </ScrollView>
          ) : (
            <ScrollView>
              <View style={[styles.justifyrow, { flexWrap: "wrap" }]}>
                {this.state.photoPath &&
                  this.state.photoPath.map((data, key) => {
                    return (
                      <View>
                        <Avatar
                          rounded
                          source={{ uri: data }}
                          size={100}
                          containerStyle={styles.avtstyle}
                        />
                        <TouchableOpacity
                          style={styles.custedit}
                          onPress={this.imageDelete.bind(this, key)}
                        >
                          <Image
                            source={require("../../assets/images/close.png")}
                            resizemode="contain"
                            style={{ width: 30, height: 30 }}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  })}
              </View>
            </ScrollView>
          )}

          <View style={styles.footer}>
            {this.state.photoPath.length ? (
              <TouchableOpacity
                style={styles.pinkbtn}
                onPress={this.handleSubmit}
              >
                <Text style={styles.btntext}>Submit</Text>
              </TouchableOpacity>
            ) : null}
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner
});
export default connect(mapStateToProps, { spaGallaryInsertImages })(spaGallery);
