import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert,
} from 'react-native';
import {Avatar, Icon, CheckBox} from 'react-native-elements';
import styles from './style';
import {connect} from 'react-redux';
import {
  completeBookingList,
  bookingCompleteTransfer,
} from './redux/SpaOwnerAction';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';

class completeAppointment extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      spa_owner_id: '',
      visible1: false,
      spinner: true,
      popupMsg: '',
    };
    //todayDate = moment(new Date()).format('YYYY-MM-DD');
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps', nextProps);
    this.setState({spinner: false});
  }

  //--FETCH TRANSACTION HOSTORY THROUGH REDUX--//
  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      console.log('focus', this.state.spa_owner_id);
      if (this.state.spa_owner_id) {
        let userDetails = new FormData();
        let todayDate = moment(new Date()).format('YYYY-MM-DD');
        userDetails.append('spa_owner_id', this.state.spa_owner_id);
        userDetails.append('todaysDate', todayDate);
        console.log('dfjhdsfjhdjfhjdj', userDetails);
        this.props.completeBookingList(userDetails);
      }
    });

    try {
      const value = await AsyncStorage.getItem('login_id');
      console.log('loginId in myaccount page', value);
      if (value) {
        this.setState({spa_owner_id: value});
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
    let todayDate = moment(new Date()).format('YYYY-MM-DD');
    let userDetails = new FormData();
    userDetails.append('spa_owner_id', this.state.spa_owner_id);
    userDetails.append('todaysDate', todayDate);
    console.log('dfjhdsfjhdjfhjdj', userDetails);

    this.props.completeBookingList(userDetails);
  }

  completeBookingId = bookingId => {
    console.log('completeBookingId');

    let completeData = new FormData();
    completeData.append('booking_Id', bookingId);
    console.log('completeData', completeData);

    this.props.bookingCompleteTransfer(completeData);
  };

  componentDidUpdate(prevProps) {
    console.log('componentDidUpdate', this.props);
    if (prevProps.completeTransfer !== this.props.completeTransfer) {
      const addDetails = this.props.completeTransfer;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({visible1: true, popupMsg: addDetails.msg});
        }
        this.setState({spinner: false});
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1,
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg,
            });
          }
          //setTimeout(()=>{this.props.navigation.navigate('myAccount')}, 1000);
        } else {
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: 'Something went wrong. Please try again later.',
            });
          }
        }
      }
    }
  }

  closePopupbox = () => {
    this.setState({
      visible1: false,
    });
    console.log('status', this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate('spaAccount');
    }
  };

  render() {
    console.log('props in render in complete page', this.props);
    console.log(
      'props in render in cancle Appointment page',
      this.props.completeAppointmentList.Complete_Appointment_History,
    );
    //const transaction = this.props.transactionHistory;
    //console.log('constant transaction', transaction);

    const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Completed Appointments</Text>
          </View>
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <ScrollView>
          {/* {this.props.error && this.props.error !== undefined ? (
            Alert.alert('Something went wrong')
          ) : ( */}
          <View style={styles.graybg}>
            {this.props.completeAppointmentList.Complete_Appointment_History &&
            this.props.completeAppointmentList.Complete_Appointment_History
              .length ? (
              this.props.completeAppointmentList.Complete_Appointment_History.map(
                (item, key) => (
                  <View key={key} style={styles.lightgry}>
                    <View style={[styles.roundcontainer,{paddingRight: 31}]}>
                      <View style={[styles.grbody,{marginRight: 31}]}>
                        <View
                          style={[
                            styles.justifyrow,
                            {alignItems: 'flex-start'},
                          ]}>
                          <Text style={styles.mutetexto}>Booking Id:</Text>
                          <Text style={styles.whitetexto}>{item.id}</Text>
                        </View>
                        <View
                          style={[
                            styles.justifyrow,
                            {alignItems: 'flex-start'},
                          ]}>
                          <Text style={styles.mutetexto}>Customer Id:</Text>
                          <Text style={styles.whitetexto}>
                            {item.customer_id}
                          </Text>
                        </View>
                        <View
                          style={[
                            styles.justifyrow,
                            {alignItems: 'flex-start'},
                          ]}>
                          <Text style={styles.mutetexto}>Booking Date: </Text>
                          <Text style={styles.whitetexto}>
                            {item.booking_date}
                          </Text>
                        </View>

                        <Dialog
                          visible={this.state.visible1}
                          dialogAnimation={
                            new SlideAnimation({
                              slideFrom: 'bottom',
                            })
                          }
                          onTouchOutside={() => {
                            this.closePopupbox();
                          }}
                          dialogStyle={{width: '80%'}}
                          footer={
                            <DialogFooter>
                              <DialogButton
                                textStyle={{
                                  fontSize: 14,
                                  color: '#333',
                                  fontWeight: '700',
                                }}
                                text="OK"
                                onPress={() => {
                                  this.closePopupbox();
                                }}
                              />
                            </DialogFooter>
                          }>
                          <DialogContent>
                            <Text style={styles.popupText}>
                              {this.state.popupMsg}
                            </Text>
                          </DialogContent>
                        </Dialog>

                        <View
                          style={[
                            styles.justifyrow,
                            {alignItems: 'flex-start'},
                          ]}>
                          <Text style={styles.mutetexto}>Start Time: </Text>
                          <Text style={styles.whitetexto}>
                            {item.booking_start_time}
                          </Text>
                        </View>
                        <View
                          style={[
                            styles.justifyrow,
                            {alignItems: 'flex-start'},
                          ]}>
                          <Text style={styles.mutetexto}>
                           
                            Booked Services:
                          </Text>
                          <Text style={styles.whitetexto}>{item.services}</Text>
                        </View>
                        <TouchableOpacity
                          onPress={this.completeBookingId.bind(this, item.id)}
                          style={[styles.smbtnn, {backgroundColor: '#ffffff'}]}>
                          <Text style={[styles.smtext, {color: '#e25cff'}]}>
                            Complete
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                ),
              )
            ) : (
              <Text style={styles.backheading}>
                No Completed Appointments Found
              </Text>
            )}
          </View>
          {/* )} */}
        </ScrollView>
      </SafeAreaView>
      //<View><Text style={styles.backheading}>Hello</Text></View>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner,
});
export default connect(mapStateToProps, {
  completeBookingList,
  bookingCompleteTransfer,
})(completeAppointment);
