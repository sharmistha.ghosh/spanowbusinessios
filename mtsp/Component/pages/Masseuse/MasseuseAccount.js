import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  AsyncStorage
} from "react-native";
import { Avatar, Icon, ListItem } from "react-native-elements";
import styles from "./style";
import { connect } from "react-redux";
import { fetchMasseuseDetails } from "./redux/MasseuseAction";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import { logOut2, logOut} from "../login/redux/authAction";

class MasseuseAccount extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      masseuse_id: "",
      visible1: false,
      popupMsg: "",
      status: "",
      logOut: 1
    };
  }

  //----fetching masseuse details using redux----//
  async componentDidMount() {
    console.log("componentDidMount in myaccount page");

    this.props.navigation.addListener("willFocus", () => {
      console.log("focus", this.state.masseuse_id);
      if (this.state.masseuse_id) {
        console.log("focused working");
        let userDetails = new FormData();
        userDetails.append("user_id", this.state.masseuse_id);
        this.props.fetchMasseuseDetails(userDetails);
        console.log("in focus");
      }
      // else {
      //   console.log('els', this.props.navigation.state.params.masseuse_id);
      //   this.setState({
      //     masseuse_id: this.props.navigation.state.params.masseuse_id,
      //   });
      //   let userDetails = new FormData();
      //   userDetails.append('user_id', this.props.navigation.state.params.masseuse_id);
      //   this.props.fetchMasseuseDetails(userDetails);
      // }
    });

    try {
      const value = await AsyncStorage.getItem("login_id");
      console.log("loginId in myaccount page", value);
      if (value) {
        this.setState({ masseuse_id: value });
        let userDetails = new FormData();
        userDetails.append("user_id", value);
        this.props.fetchMasseuseDetails(userDetails);
      } else {
        this.props.navigation.navigate("Login");
      }
    } catch (error) {
      console.log(error);
    }
  }

  closePopupbox = () => {
    this.setState({
      visible1: false
    });
    console.log("status", this.state.status);

    // if (this.state.status == 1) {
    //   this.props.navigation.navigate("Login");
    // }
  };

  remove = () => {
    console.log("RemoveAsyncstorage", this.state.masseuse_id);
      this.setState({
        visible1: false,
        status: 1,
      });
  
      AsyncStorage.removeItem("login_id");
      AsyncStorage.removeItem("userLoginType");

    let logout = this.state.logOut;

    this.props.logOut2({ logout });
    this.props.navigation.navigate("Login");
  };

  remove1 = () => {
    this.setState({
      visible1: true,
      popupMsg: "Are you sure you want to logout?"
    });
  };

  render() {
    const masseuseDetails = this.props.masseusedetails;
    console.log("render in masseuseaccount page", this.props.masseusedetails);

    const { goBack } = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        {/* <KeyboardAvoidingView enabled behavior="height"> */}
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Masseuse Account</Text>
          </View>
        </View>
        <ScrollView>
          <View>
            <ImageBackground
              source={require("../../assets/images/pbg.png")}
              style={styles.tprofile}
            >
              <View style={[styles.media, { marginBottom: 0 }]}>
                {/* <Avatar
                    rounded
                    source={require('../../assets/images/avt3.png')}
                    size={90}
                    containerStyle={{
                      borderWidth: 2,
                      borderColor: '#ed9bfe',
                      padding: 6,
                      backgroundColor: '#fff',
                    }}
                  /> */}
                {masseuseDetails.profile_image ? (
                  <Avatar
                    rounded
                    source={{
                      uri: `${masseuseDetails.profile_image}`
                    }}
                    size={90}
                    containerStyle={{
                      borderWidth: 2,
                      borderColor: "#ed9bfe",
                      padding: 6,
                      backgroundColor: "#fff"
                    }}
                  />
                ) : (
                  <Avatar
                    rounded
                    source={require("../../assets/images/download.png")}
                    size={90}
                    containerStyle={{
                      borderWidth: 2,
                      borderColor: "#ed9bfe",
                      padding: 6,
                      backgroundColor: "#fff"
                    }}
                  />
                )}
                <View style={styles.mediabody}>
                  <View style={{ position: "relative", top: 25 }}>
                    <Text style={[styles.h3, { color: "#fff" }]}>
                      {masseuseDetails.name}
                    </Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}>
                      {/* <Image
                          source={require('../../assets/images/locate.png')}
                          resizeMode="contain"
                          style={{width: 10, marginRight: 5}}
                        /> */}
                      <Text style={{ color: "#fadcff" }}>
                        {masseuseDetails.address}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </ImageBackground>
            <View style={styles.graycontainer}>
              {/* <View style={[styles.listitem, styles.selected]}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('spaInformation')
                    }
                    style={[styles.justifyrow, {marginBottom: 0}]}>
                    <Image
                      source={require('../../assets/images/aicon1.png')}
                      resizeMode="contain"
                      style={styles.picon}
                    />
                    <Text style={styles.menutext}>Spa Details</Text>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Icon
                      name="ios-arrow-forward"
                      type="ionicon"
                      color="#4a4a54"
                    />
                  </TouchableOpacity>
                </View>
                <View style={styles.listitem}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('gallery')}
                    style={[styles.justifyrow, {marginBottom: 0}]}>
                    <Image
                      source={require('../../assets/images/aicon1.png')}
                      resizeMode="contain"
                      style={styles.picon}
                    />
                    <Text style={styles.menutext}>Spa Gallery</Text>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Icon
                      name="ios-arrow-forward"
                      type="ionicon"
                      color="#4a4a54"
                    />
                  </TouchableOpacity>
                </View> */}

              {/* <View style={styles.listitem}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('service')}
                    style={[styles.justifyrow, {marginBottom: 0}]}>
                    <Image
                      source={require('../../assets/images/aicon2.png')}
                      resizeMode="contain"
                      style={[styles.picon, {width: 24}]}
                    />
                    <Text style={styles.menutext}>Set Service</Text>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Icon
                      name="ios-arrow-forward"
                      type="ionicon"
                      color="#4a4a54"
                    />
                  </TouchableOpacity>
                </View>
                <View style={styles.listitem}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('MasseuseList')
                    }
                    style={[styles.justifyrow, {marginBottom: 0}]}>
                    <Image
                      source={require('../../assets/images/aicon3.png')}
                      resizeMode="contain"
                      style={[styles.picon, {width: 24}]}
                    />
                    <Text style={styles.menutext}>Masseuse</Text>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Icon
                      name="ios-arrow-forward"
                      type="ionicon"
                      color="#4a4a54"
                    />
                  </TouchableOpacity>
                </View> */}
              <Dialog
                visible={this.state.visible1}
                dialogAnimation={
                  new SlideAnimation({
                    slideFrom: "bottom"
                  })
                }
                onTouchOutside={() => {
                  this.closePopupbox();
                }}
                dialogStyle={{ width: "80%" }}
                footer={
                  <DialogFooter>
                    <DialogButton
                      textStyle={{
                        fontSize: 14,
                        color: "#333",
                        fontWeight: "700"
                      }}
                      text="YES"
                      onPress={() => {
                        this.remove();
                      }}
                    />
                    <DialogButton
                      textStyle={{
                        fontSize: 14,
                        color: "#333",
                        fontWeight: "700"
                      }}
                      text="NO"
                      onPress={() => {
                        this.closePopupbox();
                      }}
                    />
                  </DialogFooter>
                }
              >
                <DialogContent>
                  <Text style={styles.popupText}>{this.state.popupMsg}</Text>
                </DialogContent>
              </Dialog>
              <View style={styles.listitem}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate(
                      "MasseuseUpcommingAppointment"
                    )
                  }
                  style={[styles.justifyrow, { marginBottom: 0 }]}
                >
                  <Image
                    source={require("../../assets/images/aicon4.png")}
                    resizeMode="contain"
                    style={[styles.picon, { width: 24 }]}
                  />
                  <Text style={styles.menutext}>Upcoming Bookings</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.listitem}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("MasseuseAppointmentHistory")
                  }
                  style={[styles.justifyrow, { marginBottom: 0 }]}
                >
                  <Image
                    source={require("../../assets/images/aicon5.png")}
                    resizeMode="contain"
                    style={[styles.picon, { width: 24 }]}
                  />
                  <Text style={styles.menutext}>Appointment History</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </TouchableOpacity>
              </View>
              {/* <View style={styles.listitem}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('Transection')
                    }
                    style={[styles.justifyrow, {marginBottom: 0}]}>
                    <Image
                      source={require('../../assets/images/aicon6.png')}
                      resizeMode="contain"
                      style={styles.picon}
                    />
                    <Text style={styles.menutext}>Transaction History</Text>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Icon
                      name="ios-arrow-forward"
                      type="ionicon"
                      color="#4a4a54"
                    />
                  </TouchableOpacity>
                </View> */}

              <View style={styles.listitem}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("MasseuseInformation")
                  }
                  style={[styles.justifyrow, { marginBottom: 0 }]}
                >
                  <Image
                    source={require("../../assets/images/man.png")}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={styles.menutext}>Personal Info</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </TouchableOpacity>
              </View>

              <View style={styles.listitem}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("changePassword")
                  }
                  style={[styles.justifyrow, { marginBottom: 0 }]}
                >
                  <Image
                    source={require("../../assets/images/picon2.png")}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={styles.menutext}>Security Settings</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </TouchableOpacity>
              </View>

              <View style={styles.listitem}>
                <TouchableOpacity
                  onPress={this.remove1}
                  style={[styles.justifyrow, { marginBottom: 0 }]}
                >
                  <Image
                    source={require("../../assets/images/aicon7.png")}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={styles.menutext}>Log Out</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
        {/* </KeyboardAvoidingView> */}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.Masseuse,
  ...state.auth
});

export default connect(mapStateToProps, { fetchMasseuseDetails, logOut2 })(
  MasseuseAccount,
 
);
