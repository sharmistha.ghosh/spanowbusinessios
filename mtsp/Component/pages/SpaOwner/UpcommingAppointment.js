import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert
} from "react-native";
import { Avatar, Icon, CheckBox } from "react-native-elements";
import styles from "./style";
import { connect } from "react-redux";
import {
  fetchUpcommingAppointment,
  completePaymentCash
} from "./redux/SpaOwnerAction";
import Calendar from "react-native-calendar";
import Spinner from "react-native-loading-spinner-overlay";
import moment from "moment";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';

class UpcommingAppointment extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      spa_owner_id: "",
      selectedStartDate: null,
      selectedDates: [],
      matched: "",
      index: "",
      first_date_choose: 0,
      selectedDate: new Date(),
      spinner: true,
      selectedDateBookings: [],
      visible1: false,
      popupMsg: '',
    };
    let todayDate = moment(new Date()).format("YYYY-MM-DD");
  }

  componentWillReceiveProps(nextProps) {
    var datesArray = [];
    console.log("componentWillReceiveProps", this.props.upcommingAppointment);
    console.log("componentWillReceiveProps", nextProps);

    this.setState({ spinner: false });
    if (
      nextProps.upcommingAppointment &&
      nextProps.upcommingAppointment.length
    ) {
      for (var i = 0; i < nextProps.upcommingAppointment.length; i++) {
        datesArray.push(nextProps.upcommingAppointment[i].booking_date);
      }
    }
    this.setState({ dateArray: datesArray });
  }

  //--FETCH UPCOMMING APPOINTMENT THROUGH REDUX--//
  async componentDidMount() {
    var todayDate = moment(new Date()).format("YYYY-MM-DD");
    let currenttime = moment(new Date()).format("HH:MM:ss");
    try {
      const value = await AsyncStorage.getItem("login_id");
      console.log("loginId in myaccount page", value);
      if (value) {
        this.setState({ spa_owner_id: value });
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
    let userDetails = new FormData();
    userDetails.append("spa_owner_id", this.state.spa_owner_id);
    userDetails.append("todaysDate", todayDate);
    userDetails.append("time", currenttime);
    console.log("dfjhdsfjhdjfhjdj", userDetails);

    this.props.fetchUpcommingAppointment(userDetails);
   // this.props.completePaymentCash(userDetails);
  }

  onDateSelect = date => {
    console.log("this.props.upcommingAppointment",this.props.upcommingAppointment);
    this.setState({ first_date_choose: 1, selectedDate: date, matched: 0 });

    var selectDate = moment(date).format("YYYY-MM-DD");
    var selectedDateBooking = [];
    for (var i = 0; i < this.props.upcommingAppointment.length; i++) {
      if (this.props.upcommingAppointment[i].booking_date == selectDate) {
        this.setState({ matched: 1 });
        selectedDateBooking.push({
          customer_id: this.props.upcommingAppointment[i].customer_id,
          customer_name: this.props.upcommingAppointment[i].customer_name,
          profile_image: this.props.upcommingAppointment[i].profile_image,
          id: this.props.upcommingAppointment[i].id,
          booking_date: this.props.upcommingAppointment[i].booking_date,
          booking_date_modified: this.props.upcommingAppointment[i]
            .booking_date_modified,
          booking_start_time: this.props.upcommingAppointment[i]
            .booking_start_time,
          services: this.props.upcommingAppointment[i].services,
          masseuse_name: this.props.upcommingAppointment[i].masseuse_name,
          payment_status: this.props.upcommingAppointment[i].payment_status,
          total_duration:this.props.upcommingAppointment[i].total_duration
        });
      }
    }
    console.log("selectedDateBooking", selectedDateBooking);
    this.setState({ selectedDateBookings: selectedDateBooking });
  };

  completePaymentNow = bookingId => {
    console.log("completeBookingId", bookingId);

    let completeData = new FormData();
    completeData.append("id", bookingId);
    console.log("completeData====", completeData);

    this.props.completePaymentCash(completeData);    
  };

  componentDidUpdate(prevProps) {
    console.log("componentDidUpdate===", this.props);
    if (prevProps.completePayCash !== this.props.completePayCash) {
      const addDetails = this.props.completePayCash;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({ visible1: true, popupMsg: addDetails.msg });
        }
        this.setState({ spinner: false });
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg
            });
          }
          //setTimeout(()=>{this.props.navigation.navigate('myAccount')}, 1000);
        } else {
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: "Something went wrong. Please try again later."
            });
          }
        }
      }
    }
  }

  closePopupbox = () => {
    this.setState({
      visible1: false
    });
    console.log("status", this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate("spaAccount");
    }
  };

  render() {
    console.log("datesArray", this.state.matched);

    //const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("spaAccount")}
            >
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Upcoming Appointments</Text>
          </View>
        </View>
        <Dialog
          visible={this.state.visible1}
          dialogAnimation={
            new SlideAnimation({
              slideFrom: "bottom"
            })
          }
          onTouchOutside={() => {
            this.closePopupbox();
          }}
          dialogStyle={{ width: "80%" }}
          footer={
            <DialogFooter>
              <DialogButton
                textStyle={{
                  fontSize: 14,
                  color: "#333",
                  fontWeight: "700"
                }}
                text="OK"
                onPress={() => {
                  this.closePopupbox();
                }}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <Text style={styles.popupText}>{this.state.popupMsg}</Text>
          </DialogContent>
        </Dialog>
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={styles.spinnerTextStyle}
        />
        <ScrollView>
          {this.state.dateArray && this.state.dateArray.length ? (
            <Calendar
              currentMonth={this.state.selectedDate}
              current={new Date()}
              eventDates={this.state.dateArray} // Optional array of moment() parseable dates that will show an event indicator
              nextButtonText={"Next"} // Text for next button. Default: 'Next'
              onDateSelect={this.onDateSelect} // Callback after date selection
              prevButtonText={"Prev"} // Text for previous button. Default: 'Prev'
              scrollEnabled={true} // False disables swiping. Default: False
              showControls={true} // False hides prev/next buttons. Default: False
              showEventIndicators={true} // False hides event indicators. Default:False
              titleFormat={"MMMM YYYY"} // Format for displaying current month. Default: 'MMMM YYYY'
              today={new Date()} // Defaults to today
            />
          ) : (
            <Text style={styles.backheading}>No Appointments</Text>
          )}
          {this.props.error && this.props.error !== undefined ? (
            Alert.alert("Something went wrong")
          ) : (
            <View style={styles.graycontainer}>
              {this.state.first_date_choose ? (
                <View>
                  {this.state.matched ? (
                    this.state.selectedDateBookings.map((item, key) => (
                      <View style={styles.graycard}>
                        <View
                          style={[
                            styles.media,
                            { marginBottom: 0, width: "90%" }
                          ]}
                        >
                          {item.profile_image != "" ? (
                            <Avatar
                              rounded
                              source={{ uri: `${item.profile_image}` }}
                              size={60}
                              containerStyle={{
                                borderWidth: 3,
                                borderColor: "#676782",
                                padding: 3,
                                backgroundColor: "#fff"
                              }}
                            />
                          ) : (
                            <Avatar
                              rounded
                              source={require("../../assets/images/avt3.png")}
                              size={60}
                              containerStyle={{
                                borderWidth: 3,
                                borderColor: "#676782",
                                padding: 3,
                                backgroundColor: "#fff"
                              }}
                            />
                          )}
                          <View style={styles.mediabody}>
                            <Text style={styles.h3}>
                              Customer Id: {item.customer_id}
                            </Text>
                            <View
                              style={[
                                styles.justifyrow,
                                { marginTop: 5, flexWrap: "wrap" }
                              ]}
                            >
                              <View style={styles.justifyrow}>
                                <Image
                                  source={require("../../assets/images/name.png")}
                                  resizeMode="contain"
                                  style={{ width: 14, marginRight: 5 }}
                                />
                                <Text style={styles.mutetext2}>
                                  Customer Name: {item.customer_name}
                                </Text>
                              </View>
                              <View style={styles.justifyrow}>
                                <Image
                                  source={require("../../assets/images/id.png")}
                                  resizeMode="contain"
                                  style={{ width: 14, marginRight: 5 }}
                                />
                                <Text style={styles.mutetext2}>
                                  Booking Id: {item.id}
                                </Text>
                              </View>
                              <View style={styles.justifyrow}>
                                <Image
                                  source={require("../../assets/images/calender.png")}
                                  resizeMode="contain"
                                  style={{ width: 14, marginRight: 5 }}
                                />
                                <Text style={styles.mutetext2}>
                                  {item.booking_date_modified}
                                </Text>
                              </View>
                              <View style={styles.justifyrow}>
                                <Image
                                  source={require("../../assets/images/clock.png")}
                                  resizeMode="contain"
                                  style={{ width: 14, marginRight: 5 }}
                                />
                                <Text style={styles.mutetext2}>
                                  {item.booking_start_time}
                                </Text>
                              </View>
                              <View style={styles.justifyrow}>
                                <Image
                                  source={require("../../assets/images/clock.png")}
                                  resizeMode="contain"
                                  style={{ width: 14, marginRight: 5 }}
                                />
                                <Text style={styles.mutetext2}>
                                  Duration: {item.total_duration} min
                                </Text>
                              </View>
                              <View style={styles.justifyrow}>
                                <Image
                                  source={require("../../assets/images/setting.png")}
                                  resizeMode="contain"
                                  style={{ width: 14, marginRight: 5 }}
                                />
                                <Text style={styles.mutetext2}>
                                  {item.services}
                                </Text>
                              </View>
                              <View style={styles.justifyrow}>
                                <Image
                                  source={require("../../assets/images/aicon3.png")}
                                  resizeMode="contain"
                                  style={{ width: 14, marginRight: 5 }}
                                />
                                <Text style={styles.mutetext2}>
                                  Masseuse: {item.masseuse_name}
                                </Text>
                              </View>
                              {item.payment_status == 1 ? null : (
                                <View style={styles.justifyrow}>
                                  <TouchableOpacity
                                    onPress={this.completePaymentNow.bind(
                                      this,
                                      item.id
                                    )}
                                    style={[
                                      styles.smbtnn,
                                      { backgroundColor: "#e25cff" }
                                    ]}
                                  >
                                    <Text
                                      style={[
                                        styles.smtext,
                                        { color: "#ffffff" }
                                      ]}
                                    >
                                      Payment Due
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              )}
                            </View>
                          </View>
                        </View>
                      </View>
                    ))
                  ) : (
                    <View style={styles.justifyrow}>
                      <Text style={styles.backheading}>No Appointments</Text>
                    </View>
                  )}
                </View>
              ) : null}
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner
});
export default connect(mapStateToProps, {
  fetchUpcommingAppointment,
  completePaymentCash
})(UpcommingAppointment);
