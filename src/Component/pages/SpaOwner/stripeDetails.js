import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  AsyncStorage,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import styles from './style';
import {Icon} from 'react-native-elements';
import {TextField} from 'react-native-material-textfield';
import {connect} from 'react-redux';
import {stripeDate, fetchSpaDetails} from './redux/SpaOwnerAction';

class stripeDetails extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      spa_Id: '',
      account_holder_name: '',
      account_holder_name_error: '',
      account_number: '',
      account_number_error: '',
      routing_number: '',
      routing_number_error: '',
      visible1: false,
      popupMsg: '',
      status: '',
      spa_owner_id: '',
    };
  }

  async componentDidMount() {
    //console.log('componentDidMount in myaccount page',this.props.navigator.getScreen());

    //this.locationName();
    // this.latLongdata();

    this.props.navigation.addListener('willFocus', () => {
      console.log('focus stripe', this.state.spa_owner_id);
      if (this.state.spa_owner_id) {
        console.log('focused working stripe');

        let userDetails2 = new FormData();
        userDetails2.append('spa_owner_id', this.state.spa_owner_id);
        this.props.fetchSpaDetails(userDetails2);
      }
    });

    try {
      const value = await AsyncStorage.getItem('login_id');
      console.log('loginId in stripe page', value);
      if (value) {
        this.setState({spa_owner_id: value});
        let userDetails2 = new FormData();
        userDetails2.append('spa_owner_id', this.state.spa_owner_id);
        this.props.fetchSpaDetails(userDetails2);
      }
    } catch (error) {
      console.log(error);
    }
  }

  componentDidUpdate(prevProps) {
    console.log('stripe componentDidUpdate',this.props);

    if (this.props.spadetails.stripe_account_id != null) {
      this.props.navigation.navigate('spaAccount');
    }

    if (prevProps.stripeAddAcoount !== this.props.stripeAddAcoount) {
      const addDetails = this.props.stripeAddAcoount;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({visible1: true, popupMsg: "Something went wrong. Try again with proper bank account details"});
        }
      } else {
        if (addDetails.Ack === 1) {
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg,
              status: 1,
            });
          }
        } else {
          if (this.state.visible1 === false) {
            this.setState({visible1: true, popupMsg: addDetails.msg});
          }
        }
      }
    }
  }

  //--CLOSE POPUP BOX--//
  closePopupbox = () => {
    this.setState({
      visible1: false,
    });
    if (this.state.status == 1) {
      this.props.navigation.navigate('spaAccount');
      let userDetails2 = new FormData();
      userDetails2.append('spa_owner_id', this.state.spa_owner_id);
      this.props.fetchSpaDetails(userDetails2);
    }
  };

  validate = (value, type) => {
    if (type === 'account_holder_name') {
      this.setState({
        account_holder_name: value,
        account_holder_name_error: '',
      });
      this.forceUpdate();
    }
    if (type === 'account_number') {
      this.setState({account_number: value, account_number_error: ''});
      this.forceUpdate();
    }
    if (type === 'routing_number') {
      this.setState({routing_number: value, routing_number_error: ''});
      this.forceUpdate();
    }
  };

  submit = () => {
    console.log('submit');

    if (this.state.account_holder_name === '') {
      this.setState({
        account_holder_name_error: 'Enter Account Holder Name',
      });
    } else if (this.state.account_number === '') {
      this.setState({
        account_number_error: 'Enter Account Number',
      });
    } else if (this.state.routing_number === '') {
      this.setState({
        routing_number_error: 'Enter Routing Number',
      });
    } else {
      let stripeDate = new FormData();
      stripeDate.append('spa_Id', this.props.spadetails.id);
      stripeDate.append('account_holder_name', this.state.account_holder_name);
      stripeDate.append('account_number', this.state.account_number);
      stripeDate.append('routing_number', this.state.routing_number);
      console.log('stripeDate', stripeDate);
      this.props.stripeDate(stripeDate);
    }
  };

  render() {
    console.log('render in stripedetails page', this.props);

    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <KeyboardAvoidingView enabled behavior="height">
          <View style={[styles.topbar]}>
            <View style={styles.justifyrow}>
              <Text style={styles.backheading}>Bank Account Details</Text>
            </View>
          </View>
          <ScrollView>
            <View style={{position: 'relative', paddingBottom: 40}}>
              <View style={styles.graycontainer}>
                <View style={styles.graycard}>
                  <Text style={styles.comment}>
                    Please fill the Bank Account details.
                  </Text>
                  <Text style={styles.comment}>
                    Your payment will be credited in the below mentioned Bank
                    Account.
                  </Text>
                  <Text style={styles.comment}>
                    * Please Remember This is mandatory to provide the Bank
                    details otherwise you will not be able to move forward.{' '}
                  </Text>
                  <TextField
                    label="Account Holder Name"
                    textColor={'#8f8f92'}
                    baseColor={'#8f8f92'}
                    labelFontSize={18}
                    activeLineWidth={1}
                    value={this.state.account_holder_name}
                    onChangeText={account_holder_name =>
                      this.validate(account_holder_name, 'account_holder_name')
                    }
                  />
                  <Text style={styles.error}>
                    {this.state.account_holder_name_error}
                  </Text>
                  <TextField
                    label="Account Number"
                    textColor={'#8f8f92'}
                    baseColor={'#8f8f92'}
                    labelFontSize={18}
                    activeLineWidth={1}
                    value={this.state.account_number}
                    onChangeText={account_number =>
                      this.validate(account_number, 'account_number')
                    }
                    secureTextEntry={false}
                  />
                  <Text style={styles.error}>
                    {this.state.account_number_error}
                  </Text>
                  <View style={styles.container}>
                    <Dialog
                      visible={this.state.visible1}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: 'bottom',
                        })
                      }
                      onTouchOutside={() => {
                        this.closePopupbox();
                      }}
                      dialogStyle={{width: '80%'}}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: '#333',
                              fontWeight: '700',
                            }}
                            text="OK"
                            onPress={() => {
                              this.closePopupbox();
                            }}
                          />
                        </DialogFooter>
                      }>
                      <DialogContent>
                        <Text style={styles.popupText}>
                          {this.state.popupMsg}
                        </Text>
                      </DialogContent>
                    </Dialog>
                  </View>
                  <TextField
                    label="Routing Number"
                    textColor={'#8f8f92'}
                    baseColor={'#8f8f92'}
                    labelFontSize={18}
                    activeLineWidth={1}
                    value={this.state.routing_number}
                    onChangeText={routing_number =>
                      this.validate(routing_number, 'routing_number')
                    }
                    secureTextEntry={false}
                  />
                  <Text style={styles.error}>
                    {this.state.routing_number_error}
                  </Text>
                </View>

                <TouchableOpacity
                  style={[styles.pinkbtn, {marginTop: 30}]}
                  onPress={() => this.submit()}>
                  <Text style={styles.btntext}>Submit</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner,
});
export default connect(mapStateToProps, {stripeDate, fetchSpaDetails})(
  stripeDetails,
);
