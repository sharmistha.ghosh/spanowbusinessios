import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  TextInput,
  AsyncStorage
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import styles from "./style";
import Spinner from "react-native-loading-spinner-overlay";
import { connect } from "react-redux";
import { login, logOut, logout2 } from "./redux/authAction";
//import AsyncStorage from '@react-native-community/async-storage';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import { Button, CheckBox } from "react-native-elements";

class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      email_error: "",
      password_error: "",
      login_userid: "",
      spinner: false,
      visible1: false,
      popupMsg: "",
      checkbox: false
    };
    //  this.remember();
  }

  componentDidMount() {
    this.props.navigation.addListener("willFocus", () => {
      console.log("focus in login", this.props);
      if (this.state.checkbox == false) {
        this.setState({
          email: "",
          password: ""
        });
      }

      this.storagecall();
    });
  }

  storagecall = async () => {
    console.log("storagecall");
    let usertype = await AsyncStorage.getItem("userLoginType");
    if (usertype == "M") {
      this.props.navigation.navigate("MasseuseAccount");
    } else if (usertype == "SO") {
      this.props.navigation.navigate("spaAccount");
    }
  };

  emailHandler = email => {
    this.setState({ email: email });
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!reg.test(email)) {
      this.setState({
        email_error: "Enter valid email id"
      });
    } else {
      this.setState({
        email_error: ""
      });
    }
  };

  passwordHandler = password => {
    this.setState({
      password: password
    });
  };

  handleSubmit = () => {
    if (this.state.email === "") {
      this.setState({
        email_error: "Enter email id"
      });
    }
    if (this.state.password === "") {
      this.setState({
        password_error: "Enter password"
      });
    } else {
      this.setState({ spinner: true });
      let userDetails = new FormData();
      userDetails.append("email", this.state.email);
      userDetails.append("password", this.state.password);
      this.props.login(userDetails);
      console.log("userDetails",userDetails);
      
    }
  };

  async componentDidUpdate(prevProps) {
    if (prevProps.userDetails !== this.props.userDetails) {
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({
            visible1: true,
            popupMsg: "Invalid email or password"
          });
        }
        this.setState({ spinner: false });
      } else {
        if (this.props.isLoggedIn && this.props.userDetails.ack === 1) {
          this.setState({ spinner: false });
          if (
            this.props.userDetails.userdetail &&
            this.props.userDetails.userdetail.user_type == "M"
          ) {
            await AsyncStorage.setItem(
              "login_id",
              String(this.props.userDetails.userId)
            );
            let userLoginType = "M";
            await AsyncStorage.setItem("userLoginType", userLoginType);
            this.props.navigation.navigate("MasseuseAccount");
          } else if (
            this.props.userDetails.userdetail &&
            this.props.userDetails.userdetail.user_type == "SO"
          ) {
            await AsyncStorage.setItem(
              "login_id",
              String(this.props.userDetails.userId)
            );
            let userLoginType = "SO";
            await AsyncStorage.setItem("userLoginType", userLoginType);
            this.props.navigation.navigate("spaAccount");
          } else {
            this.setState({
              visible1: true,
              popupMsg:
                "Cannot login successfully as you are not a register spaowner or masseuse"
            });
          }
        } else {
          this.setState({ spinner: false });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: this.props.userDetails.msg
            });
          }
        }
      }
    }
  }

  checkboxHandler = () => {
    //setcheckbox(checkbox);
    if (this.state.checkbox == true) {
      //setcheckbox(false);
      this.setState({ checkbox: false });
      AsyncStorage.removeItem("loginEmail");
      AsyncStorage.removeItem("loginPassword");
    } else {
      // setcheckbox(true);
      this.setState({ checkbox: true });
      AsyncStorage.setItem("loginEmail", this.state.email);
      AsyncStorage.setItem("loginPassword", this.state.password);
    }
  };

  forgetPass = () => {
    this.props.navigation.navigate("ForgotPassword");
  };

  // async remember() {
  //   try {
  //     const emailAsyncStorage = await AsyncStorage.getItem("loginEmail");
  //     const passwordAsyncStorage = await AsyncStorage.getItem("loginPassword");

  //     console.log("loginId------", emailAsyncStorage);
  //     if (emailAsyncStorage == null) {
  //       console.log("null");
  //       this.setState({ checkbox: false });
  //     } else {
  //       console.log("checkbox");
  //       this.setState({ checkbox: true });
  //     }

  //     this.setState({
  //       email: emailAsyncStorage,
  //       password: passwordAsyncStorage
  //     });
  //   } catch (error) {
  //     console.log(error);
  //     // Error saving data
  //   }
  // }

  render() {
    console.log("render---", this.props);

    return (
      <SafeAreaView>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <ImageBackground
          source={require("../../assets/images/mainbg.jpg")}
          style={styles.bodymain}
        >
          <KeyboardAvoidingView enabled behavior="padding">
            <ScrollView>
              <View style={[styles.container, { paddingTop: 30 }]}>
                <Spinner
                  visible={this.state.spinner}
                  textContent={"Loading..."}
                  textStyle={styles.spinnerTextStyle}
                />
                <TouchableOpacity style={styles.logo}>
                  <Image
                    source={require("../../assets/images/logo.png")}
                    resizeMode="contain"
                    style={{ width: 280, height: 80 }}
                  />
                </TouchableOpacity>

                <View style={[styles.mainform, { marginTop: 50 }]}>
                  <Text style={styles.heading}>Login</Text>

                  <TextInput
                    type="email"
                    style={styles.forminput}
                    placeholder="Email"
                    placeholderTextColor="#9590a9"
                    keyboardtype="email"
                    value={this.state.email}
                    onChangeText={email => this.emailHandler(email)}
                  />
                  <Text style={styles.error}>{this.state.email_error}</Text>

                  <TextInput
                    style={[styles.forminput, { marginBottom: -20 }]}
                    placeholder="Password"
                    placeholderTextColor="#9590a9"
                    value={this.state.password}
                    onChangeText={password => this.passwordHandler(password)}
                    secureTextEntry={true}
                  />
                  <Text style={styles.error}>{this.state.password_error}</Text>

                  <View
                    style={{
                      flexDirection: "column",
                      // justifyContent: "flex-start",
                      // alignItems: "center",
                      // paddingLeft: 10,
                      paddingBottom: 16
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <CheckBox
                        style={{ color: "black" }}
                        checked={this.state.checkbox}
                        onPress={this.checkboxHandler}
                        checkedColor="#e25cff"
                      />
                      <Text
                        style={{
                          paddingTop: 15,
                          color: "#e25cff",
                          fontSize: 18,
                          right: 20
                        }}
                      >
                        Remember me
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={{
                        width: "100%",
                        justifyContent: "center",
                        alignItems: "center",
                        paddingTop: 20
                      }}
                      onPress={this.forgetPass}
                    >
                      <Text
                        style={{
                          color: "#e25cff",
                          fontSize: 18
                        }}
                      >
                        Forgot password ?{" "}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  {/* <TouchableOpacity
                    style={styles.forgetpass}
                    onPress={() =>
                      this.props.navigation.navigate('ForgotPassword')
                    }>
                    <Text style={styles.pinktext}>Forgot Password</Text>
                  </TouchableOpacity> */}
                  <TouchableOpacity
                    style={[styles.pinkbtn, { paddingBottom: -20 }]}
                    onPress={() => this.handleSubmit()}
                  >
                    <Text style={styles.btntext}>SIGN IN</Text>
                  </TouchableOpacity>

                  <View style={styles.container}>
                    <Dialog
                      visible={this.state.visible1}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      onTouchOutside={() => {
                        this.setState({ visible1: false });
                      }}
                      dialogStyle={{ width: "80%" }}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: "#333",
                              fontWeight: "700"
                            }}
                            text="OK"
                            onPress={() => {
                              this.setState({ visible1: false });
                            }}
                          />
                        </DialogFooter>
                      }
                    >
                      <DialogContent>
                        <Text style={styles.popupText}>
                          {this.state.popupMsg}
                        </Text>
                      </DialogContent>
                    </Dialog>
                  </View>

                  <View style={[styles.bttext, { paddingBottom: 2 }]}>
                    <Text style={styles.text}>New to Massaged Today?</Text>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("Signup")}
                    >
                      <Text style={styles.pinktext}> Register Now</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.auth
});

export default connect(mapStateToProps, { login, logOut, logout2 })(
  LoginScreen
);
