import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert,
} from 'react-native';
import moment from 'moment';
import {Avatar, Icon, CheckBox} from 'react-native-elements';
import styles from './style';
import {connect} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import {fetchAppointmentHistory} from './redux/SpaOwnerAction';

class AppointmentHistory extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      spa_owner_id: '',
      spinner: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps', nextProps);
    this.setState({spinner: false});
  }

  //--FETCH APPOINTMENT HOSTORY THROUGH REDUX--//
  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem('login_id');
      console.log('loginId in myaccount page', value);
      if (value) {
        this.setState({spa_owner_id: value});
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
    let todayDate = moment(new Date()).format('YYYY-MM-DD');
    let userDetails = new FormData();
    userDetails.append('spa_owner_id', this.state.spa_owner_id);
    userDetails.append('todayDate', todayDate);
    console.log("fetchAppointmentHistory payload",userDetails);
    this.props.fetchAppointmentHistory(userDetails);
  }

  render() {
    //console.log('props in render in appointment history page', this.props);
    console.log(
      'props in render in appointment history page',
      this.props.appointmentHistory,
    );

    //const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('spaAccount')}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Appointment History</Text>
          </View>
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <ScrollView>
          {this.props.error && this.props.error !== undefined ? (
            Alert.alert('Something went wrong')
          ) : (
            <View style={styles.graycontainer}>
              {this.props.appointmentHistory.length ? (
                this.props.appointmentHistory.map((item, key) => (
                  <View key={key} style={styles.graycard}>
                    <View
                      style={[styles.media, {marginBottom: 0, width: '90%'}]}>
                      {item.custome_profile_image == "" ? 
                      <Avatar
                        rounded
                        source={require('../../assets/images/avt3.png')}
                        size={60}
                        containerStyle={{
                          borderWidth: 3,
                          borderColor: '#676782',
                          padding: 3,
                          backgroundColor: '#fff',
                        }}
                      /> : 
                      <Avatar
                        rounded
                        source={{ uri: `${item.custome_profile_image}` }}
                        size={60}
                        containerStyle={{
                          borderWidth: 3,
                          borderColor: '#676782',
                          padding: 3,
                          backgroundColor: '#fff',
                        }}
                      />
                      }
                      <View style={styles.mediabody}>
                        <Text style={styles.h3}>Customer Id : {item.customer_id}</Text>
                        <View
                          style={[
                            styles.justifyrow,
                            {marginTop: 5, flexWrap: 'wrap'},
                          ]}>
                          <View style={styles.justifyrow}>
                            <Image
                              source={require('../../assets/images/calender.png')}
                              resizeMode="contain"
                              style={{width: 14, marginRight: 5}}
                            />
                            <Text style={styles.mutetext2}>
                              Customer Name: {item.customer_name}
                            </Text>
                          </View>
                          <View style={styles.justifyrow}>
                            <Image
                              source={require('../../assets/images/calender.png')}
                              resizeMode="contain"
                              style={{width: 14, marginRight: 5}}
                            />
                            <Text style={styles.mutetext2}>
                              Booking Id: {item.id}
                            </Text>
                          </View>
                          <View style={styles.justifyrow}>
                            <Image
                              source={require('../../assets/images/calender.png')}
                              resizeMode="contain"
                              style={{width: 14, marginRight: 5}}
                            />
                            <Text style={styles.mutetext2}>
                              {item.booking_date}
                            </Text>
                          </View>
                          <View style={styles.justifyrow}>
                            <Image
                              source={require('../../assets/images/clock.png')}
                              resizeMode="contain"
                              style={{width: 14, marginRight: 5}}
                            />
                            <Text style={styles.mutetext2}>
                              {item.booking_start_time}
                            </Text>
                          </View>
                          <View style={styles.justifyrow}>
                            <Image
                              source={require('../../assets/images/setting.png')}
                              resizeMode="contain"
                              style={{width: 14, marginRight: 5}}
                            />
                            <Text style={styles.mutetext2}>
                              {item.services}
                            </Text>
                          </View>
                          <View style={styles.justifyrow}>
                            <Image
                              source={require("../../assets/images/aicon3.png")}
                              resizeMode="contain"
                              style={{ width: 14, marginRight: 5 }}
                            />
                            <Text style={styles.mutetext2}>
                              Masseuse: {item.masseuse_name}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                ))
              ) : (
                <View style={styles.justifyrow}>
                  <Text style={styles.backheading}>
                    No Appointment History Found
                  </Text>
                </View>
              )}
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner,
});
export default connect(mapStateToProps, {fetchAppointmentHistory})(
  AppointmentHistory,
);
