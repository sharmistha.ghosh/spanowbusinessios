import {
  FETCH_MASSEUSE_DETAILS,
  MASSEUSE_APPOINTMENT_HISTORY,
  MASSEUSE_UPCOMMING_APPOINTMENT,
  EDIT_MASSEUSE_INFORMATION,
} from '../../../../redux/actions/types';

const intialState = {
  error: '',
  appointmentHistory: {},
  upcommingAppointment: {},
  masseusedetails: {},
  masseuseEdit: {},
};

export default function(state = intialState, action) {
  switch (action.type) {
    case FETCH_MASSEUSE_DETAILS:
      return {
        ...state,
        masseusedetails: action.payload,
        error: action.payload.response,
      };

    case EDIT_MASSEUSE_INFORMATION:
      return {
        ...state,
        masseuseEdit: action.payload,
        error: action.payload.response,
      };

    case MASSEUSE_APPOINTMENT_HISTORY:
      return {
        ...state,
        appointmentHistory: action.payload,
        error: action.payload.response,
      };

    case MASSEUSE_UPCOMMING_APPOINTMENT:
      return {
        ...state,
        upcommingAppointment: action.payload,
        error: action.payload.response,
      };

    default:
      return state;
  }
}
