import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  AsyncStorage,
  BackHandler,
} from 'react-native';
import {Avatar, Icon, ListItem} from 'react-native-elements';
import styles from './style';
import {connect} from 'react-redux';
import {
  fetchSpaOwnerDetails,
  fetchServiceListing,
  fetchSelectedService,
  setSelectedServiceArray,
  fetchSpaDetails,
  fetchSpaTiming,
  setSpaTimingArray,
} from './redux/SpaOwnerAction';
//import {getCurrentPosition} from 'react-native-geolocation-service';
//import Axios from "axios";
import Geocoder from 'react-native-geocoding';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import {logOut} from '../login/redux/authAction';

class spaAccount extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      spa_owner_id: '',
      locationName: '',
      visible1: false,
      logOut: 1,
      popupMsg: '',
      status: '',
      day: [
        {
          day: 'Mon',
          dayName: 'Monday',
          is_check: '0',
          selectedOpenHours: '00',
          selectedOpenMinutes: '00',
          selectedCloseHours: '00',
          selectedCloseMinutes: '00',
        },
        {
          day: 'Tue',
          dayName: 'Tuesday',
          is_check: '0',
          selectedOpenHours: '00',
          selectedOpenMinutes: '00',
          selectedCloseHours: '00',
          selectedCloseMinutes: '00',
        },
        {
          day: 'Wed',
          dayName: 'Wednesday',
          is_check: '0',
          selectedOpenHours: '00',
          selectedOpenMinutes: '00',
          selectedCloseHours: '00',
          selectedCloseMinutes: '00',
        },
        {
          day: 'Thu',
          dayName: 'Thursday',
          is_check: '0',
          selectedOpenHours: '00',
          selectedOpenMinutes: '00',
          selectedCloseHours: '00',
          selectedCloseMinutes: '00',
        },
        {
          day: 'Fri',
          dayName: 'Friday',
          is_check: '0',
          selectedOpenHours: '00',
          selectedOpenMinutes: '00',
          selectedCloseHours: '00',
          selectedCloseMinutes: '00',
        },
        {
          day: 'Sat',
          dayName: 'Saturday',
          is_check: '0',
          selectedOpenHours: '00',
          selectedOpenMinutes: '00',
          selectedCloseHours: '00',
          selectedCloseMinutes: '00',
        },
        {
          day: 'Sun',
          dayName: 'Sunday',
          is_check: '0',
          selectedOpenHours: '00',
          selectedOpenMinutes: '00',
          selectedCloseHours: '00',
          selectedCloseMinutes: '00',
        },
      ],
    };
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick1,
    );
  }

  //----fetching spa owner details using redux----//
  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      console.log('focus spaccount', this.state.spa_owner_id);
      if (this.state.spa_owner_id) {
        console.log('focused working');
        let userDetails = new FormData();
        userDetails.append('user_id', this.state.spa_owner_id);
        this.props.fetchSpaOwnerDetails(userDetails);
        console.log('in focus');

        let userDetails1 = new FormData();
        userDetails1.append('spa_owner_id', this.state.spa_owner_id);
        this.props.fetchSelectedService(userDetails1);
        this.props.fetchServiceListing();

        let userDetails2 = new FormData();
        userDetails2.append('spa_owner_id', this.state.spa_owner_id);
        this.props.fetchSpaDetails(userDetails2);

        let userDetails3 = new FormData();
        userDetails3.append('spa_owner_id', this.state.spa_owner_id);
        console.log('fetching timimg', userDetails3);
        this.props.fetchSpaTiming(userDetails3);
      }
    });

    try {
      const value = await AsyncStorage.getItem('login_id');
      console.log('loginId in spaccount page', value);
      if (value) {
        this.setState({spa_owner_id: value});
        let userDetails = new FormData();
        userDetails.append('user_id', value);

        this.props.fetchSpaOwnerDetails(userDetails);

        let userDetails1 = new FormData();
        userDetails1.append('spa_owner_id', value);
        this.props.fetchSelectedService(userDetails1);
        this.props.fetchServiceListing();
        let userDetails2 = new FormData();
        userDetails2.append('spa_owner_id', this.state.spa_owner_id);
        this.props.fetchSpaDetails(userDetails2);
        let userDetails3 = new FormData();
        userDetails3.append('spa_owner_id', this.state.spa_owner_id);
        console.log('fetching timimg', userDetails3);
        this.props.fetchSpaTiming(userDetails3);
      } else {
        this.props.navigation.navigate('Login');
      }
    } catch (error) {
      console.log(error);
    }
  }

  closePopupbox = () => {
    this.setState({
      visible1: false,
    });
    console.log('status', this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate('Login');
    }
  };

  remove = () => {
    console.log('RemoveAsyncstorage', this.state.masseuse_id);

    AsyncStorage.removeItem('login_id');

    let logout = this.state.logOut;

    this.props.logOut({logout});

    this.setState({
      visible1: true,
      popupMsg: 'Logout',
      status: 1,
      spa_owner_id: '',
    });
  };

  timingArray = () => {
    if (this.props.spaTimeDetails && this.props.spaTimeDetails.length) {
      var spaTiming = this.props.spaTimeDetails;
    } else {
      var spaTiming = this.state.day;
    }
    console.log('spatiming in spa accoun page', spaTiming);

    this.props.setSpaTimingArray(spaTiming);
    this.props.navigation.navigate('dateTimeSelect');
  };
  serviceArray = () => {
    var match = 0;
    var allServices = this.props.allServices;
    var selectedService = this.props.selectedService;
    var serviceArray = [];
    console.log('first', serviceArray);

    console.log('selected services function', selectedService);
    console.log('all services in function', allServices);

    if (selectedService.length && allServices.length) {
      for (var i = 0; i < selectedService.length; i++) {
        for (var j = 0; j < allServices.length; j++) {
          if (selectedService[i].id == allServices[j].id) {
            serviceArray.push({
              id: selectedService[i].id,
              name: selectedService[i].name,
              is_checked: selectedService[i].is_checked,
              price_duration: [
                {
                  price: selectedService[i].price,
                  duration: selectedService[i].duration,
                },
              ],
            });
            allServices[j].matched = 1;
          }
        }
      }

      for (var k = 0; k < allServices.length; k++) {
        if (allServices[k].matched == 1) {
          console.log('matched == 1');
        } else {
          console.log('add');
          serviceArray.push({
            id: allServices[k].id,
            name: allServices[k].name,
            is_checked: allServices[k].is_checked,
            price_duration: [],
          });
        }
      }
    } else {
      serviceArray = allServices;
    }

    console.log('new array', serviceArray);
    var test = [];

    for (var i = 0; i < serviceArray.length; i++) {
      if (test.length) {
        for (var j = 0; j < test.length; j++) {
          var unmatch = 1;
          if (test[j].id == serviceArray[i].id) {
            unmatch = 0;

            console.log('array id matched', test[j].name);

            var test1 = test[j].price_duration;
            test1.push({
              price: serviceArray[i].price_duration[0].price,
              duration: serviceArray[i].price_duration[0].duration,
            });
            console.log('tedhjjhfsjhsdf', test1);
            test[j].price_duration = test1;
          }
        }
        if (unmatch == 1) {
          test.push(serviceArray[i]);
        }
      } else {
        test.push(serviceArray[i]);
      }
    }
    console.log('est', test);

    this.props.setSelectedServiceArray(test);
    this.props.navigation.navigate('service');
  };

  handleBackButtonClick1 = () => {
    console.log(
      'handleBackButtonClick111111111111111111111111',
      this.props.navigation.state.routeName,
    );

    let logout = this.state.logOut;

    this.props.logOut();

    if (this.props.navigation.state.routeName === 'spaAccount') {
      BackHandler.exitApp();
    }
  };

  stripeChecking = () => {
    console.log('stripeChecking', this.props.spadetails.stripe_account_id);
    if (this.props.spadetails.stripe_account_id === null) {
      this.props.navigation.navigate('stripeDetails');
      console.log('!null', this.props.spadetails.stripe_account_id);
    } else {
      console.log('value');
    }
  };

  transactionPage = () => {
    this.props.navigation.navigate('Transection', {
      spa_id: this.props.spadetails.id,
    });
  };

  render() {
    this.stripeChecking();
    console.log('render in spaAccount page', this.props);
    if (this.props.spadetails && Object.keys(this.props.spadetails).length) {
      console.log('length', Object.keys(this.props.spadetails).length);
    }

    //const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            {/* <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity> */}
            <Text style={styles.backheading}>Spa Owner Account</Text>
          </View>
        </View>
        <ScrollView>
          <View>
            {this.props.spaownerdetails &&
            Object.keys(this.props.spaownerdetails).length ? (
              <ImageBackground
                source={require('../../assets/images/pbg.png')}
                style={styles.tprofile}>
                <View style={[styles.media, {marginBottom: 0}]}>
                  {/* <Avatar
                    rounded
                    source={require('../../assets/images/avt3.png')}
                    size={90}
                    containerStyle={{
                      borderWidth: 2,
                      borderColor: '#ed9bfe',
                      padding: 6,
                      backgroundColor: '#fff',
                    }}
                  /> */}
                  {this.props.spaownerdetails.profile_image ? (
                    <Avatar
                      rounded
                      source={{
                        uri: `${this.props.spaownerdetails.profile_image}`,
                      }}
                      size={90}
                      containerStyle={{
                        borderWidth: 2,
                        borderColor: '#ed9bfe',
                        padding: 6,
                        backgroundColor: '#fff',
                      }}
                    />
                  ) : (
                    <Avatar
                      rounded
                      source={require('../../assets/images/download.png')}
                      size={90}
                      containerStyle={{
                        borderWidth: 2,
                        borderColor: '#ed9bfe',
                        padding: 6,
                        backgroundColor: '#fff',
                      }}
                    />
                  )}
                  <View style={styles.mediabody}>
                    <View style={{position: 'relative', top: 25}}>
                      <Text style={[styles.h3, {color: '#fff'}]}>
                        {this.props.spaownerdetails.name}
                      </Text>
                      <TouchableOpacity style={{flexDirection: 'row'}}>
                        {/* <Image
                          source={require('../../assets/images/locate.png')}
                          resizeMode="contain"
                          style={{width: 10, marginRight: 5}}
                        /> */}
                        <Text style={{color: '#fadcff'}}>
                          {this.props.spaownerdetails.address}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </ImageBackground>
            ) : null}
            <View style={styles.graycontainer}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('spaInformation')}
                style={[{marginBottom: 0}]}>
                <View style={[styles.listitem]}>
                  <Image
                    source={require('../../assets/images/aicon1.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Spa Details
                  </Text>

                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('gallery')}
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/camera.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Spa Gallery
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('changePassword')}
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/picon2.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Security Settings
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Holiday')}
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/tree.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Spa Holidays
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <Dialog
                visible={this.state.visible1}
                dialogAnimation={
                  new SlideAnimation({
                    slideFrom: 'bottom',
                  })
                }
                onTouchOutside={() => {
                  this.closePopupbox();
                }}
                dialogStyle={{width: '80%'}}
                footer={
                  <DialogFooter>
                    <DialogButton
                      textStyle={{
                        fontSize: 14,
                        color: '#333',
                        fontWeight: '700',
                      }}
                      text="OK"
                      onPress={() => {
                        this.closePopupbox();
                      }}
                    />
                  </DialogFooter>
                }>
                <DialogContent>
                  <Text style={styles.popupText}>{this.state.popupMsg}</Text>
                </DialogContent>
              </Dialog>
              <TouchableOpacity
                onPress={this.serviceArray}
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/aicon2.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Spa Services
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('MasseuseList')}
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/aicon3.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Masseuse
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={this.timingArray}
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/clock1.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Spa Hours
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('AddSpaDiscount')}
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/discount.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                  Add Spa Discount
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('UpcommingAppointment')
                }
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/aicon4.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Upcoming Appointments
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('AppointmentHistory')
                }
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/aicon5.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Appointment History
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('completeAppointment')
                }
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/tick.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Completed Appointments
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('cancelAppointment')
                }
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/cross.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Cancelled Appointments
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={this.transactionPage}
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/dollar.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Transaction History
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('OwnerInformation')
                }
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/man.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Personal Info
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={this.remove}
                style={[{marginBottom: 0}]}>
                <View style={styles.listitem}>
                  <Image
                    source={require('../../assets/images/aicon7.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Log Out
                  </Text>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner,
  ...state.auth,
});
export default connect(mapStateToProps, {
  fetchSpaOwnerDetails,
  fetchServiceListing,
  fetchSelectedService,
  setSelectedServiceArray,
  fetchSpaDetails,
  fetchSpaTiming,
  setSpaTimingArray,
  logOut,
})(spaAccount);
