import { Dimensions } from 'react-native'
let ScreenHeight = Dimensions.get("window").height;
export default {    
    bodymain: {      
        flex:1,
        minHeight: ScreenHeight,
        paddingTop:30,
        alignSelf: 'stretch',
        height:'110%'     
    },
    container: {        
        paddingLeft:15,
        paddingRight:15,
        paddingBottom:15,    
    },
    logo: {
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
        marginTop:40,
        marginBottom:30
    },
    forgetPassword:{
        alignSelf: 'flex-start',
        marginLeft: 20,
    },
    heading: {
        fontSize:20,
        color:'#fff',
        textAlign:'center',
        marginBottom:30,       
    },
    forminput: {
        borderWidth:1,
        borderRadius:5,
        borderColor:'#934bb5',
        padding:8,
        height:45,
        width:'100%',
        color:'#fff',
        fontSize:15,
        marginBottom:15
    },
    pinktext: {
        color:'#e25cff',
        fontSize:18,
        textAlign:'center'
    },
    forgetpass: {
        padding:10,
        marginBottom:30,       
        height:30,
        alignItems:'center'
    },
    pinkbtn: {
        backgroundColor:'#e25cff',
        height:45,
        lineHeight:45,
        width:'100%',
        padding:10,
        marginBottom:20,
        justifyContent:'center',  
        borderRadius:5      
    },
    btntext: {
        fontSize:15,
        color:'#fff',
        textAlign:'center',
    },
    orline: {
        width:'100%',
        height:30,
        marginBottom:25,
    },
    googlebtn:{
        width:'45%',
        backgroundColor:'#e3411f',
        height:45,
        lineHeight:45,
        padding:10,        
        justifyContent:'center',
        borderRadius:5
    },
    fbbtn:{
        width:'45%',
        backgroundColor:'#1c4297',
        height:45,
        lineHeight:45,
        padding:10,        
        justifyContent:'center',
        borderRadius:5
    },
    jsbtn: {
        justifyContent:'space-between',
        alignItems:'center',
        marginBottom:25,
        flexDirection:'row',       
    },
    bttext: {
        justifyContent:'center',
        alignItems:'center',
        marginBottom:25,
        flexDirection:'row',   
    },
    text: {
        fontSize:18,
        color:'#fff'
    },
    // error: {
    //     fontSize:12,
    //     color:'#fff'
    // },
    error:{
        fontSize: 16,
        color: '#FF0000',
        paddingBottom: 22,
    }
};