import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import {Avatar, Icon} from 'react-native-elements';
import styles from './style';
import {fetchMassause, deleteMassuse} from './redux/SpaOwnerAction';
import Spinner from 'react-native-loading-spinner-overlay';
import {connect} from 'react-redux';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import AsyncStorage from '@react-native-community/async-storage';

class MasseuseList extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      visible1: false,
      msg: '',
      spinner: true,
      spa_id: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps', nextProps);
    this.setState({spinner: false});
  }

  async componentDidMount() {
    // this.props.navigation.addListener('willFocus', () => {
    //   console.log('focus', this.state.spa_id);
    //   if (this.state.spa_id) {
    //     console.log('focused working');
    //     let userDetails = new FormData();
    //     userDetails.append('spa_id', this.state.spa_id);
    //     this.props.fetchMassause(userDetails);
    //     console.log('in focus');
    //   }
    // });
    //const value = await AsyncStorage.getItem('spa_id');
    //console.log("this.props.spadetails.id",this.props.spadetails.id);
    
    let userDetails = new FormData();
    userDetails.append('spa_id', this.props.spadetails.id);
    console.log('userDetails', userDetails);

    this.props.fetchMassause(userDetails);
    this.setState({
      spa_id: value,
    });
  }

  componentDidUpdate(prevProps) {
    console.log('componentDidUpdate', this.props);
    if (prevProps.masseusedelete !== this.props.masseusedelete) {
      this.setState({spinner: false});
      const addDetails = this.props.masseusedelete;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({visible1: true, popupMsg: addDetails.msg});
        }
        //this.setState({spinner: false});
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1,
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg,
            });
          }
          // let userDetails = new FormData();
          // userDetails.append('spa_id', this.state.spa_id);
          // this.props.fetchMassause(userDetails);
        } else {
          // this.setState({
          //   spinner: false,
          // });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: 'Something went wrong. Please try again later.',
            });
          }
        }
      }
    }
  }

  closePopupbox = () => {
    this.setState({
      visible1: false,
    });
    console.log('status', this.state.status);

    if (this.state.status == 1) {
      let userDetails = new FormData();
      userDetails.append('spa_id', this.props.spadetails.id);
      this.props.fetchMassause(userDetails);
    }
  };

  deleteMassuse = id => {
    let userDetails = new FormData();
    userDetails.append('id', id);
    this.setState({spinner: true})
    this.props.deleteMassuse(userDetails);
    // this.props.navigation.navigate('MasseuseList');
    console.log('deleteMassuse function', this.props.masseusedelete);
    // if (this.props.masseusedelete.Ack) {
    //   if (this.props.masseusedelete.Ack == 1) {

    //     this.setState({
    //       status: 1,

    //       visible1: true,
    //       msg: this.props.masseusedelete.msg,
    //     });
    //   } else if (this.props.masseusedelete.Ack == 0) {
    //     this.setState({
    //       visible1: true,
    //       msg: this.props.masseusedelete.msg,
    //     });
    //   }
    // }
  };

  render() {
    console.log('Inside render masseuse list', this.props);
    const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Masseuse</Text>
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('addMasseuse')}>
            <Icon
              name="md-add"
              type="ionicon"
              color="#e25cff"
              style={{fontSize: 24}}
            />
          </TouchableOpacity>
        </View>

        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />

        <Dialog
          visible={this.state.visible1}
          dialogAnimation={
            new SlideAnimation({
              slideFrom: 'bottom',
            })
          }
          onTouchOutside={() => {
            this.closePopupbox();
          }}
          dialogStyle={{width: '80%'}}
          footer={
            <DialogFooter>
              <DialogButton
                textStyle={{
                  fontSize: 14,
                  color: '#333',
                  fontWeight: '700',
                }}
                text="OK"
                onPress={() => {
                  this.closePopupbox();
                }}
              />
            </DialogFooter>
          }>
          <DialogContent>
            <Text style={styles.popupText}>{this.state.popupMsg}</Text>
          </DialogContent>
        </Dialog>

        {/* <Spinner
            visible={this.state.spinner}
            textContent={'Loading...'}
            textStyle={styles.spinnerTextStyle}
            />  */}

        <ScrollView>
          <View style={styles.graycontainer}>
            {this.props.massauseList && this.props.massauseList.length ? (
              <View>
                {this.props.massauseList.map((item, i) => {
                  return (
                    <View style={styles.graycard} key={i}>
                      <View
                        style={[styles.media, {marginBottom: 0, width: '90%'}]}
                        key={i}>
                        {item.profile_image !== '' ? (
                          <Avatar
                            source={{uri: `${item.profile_image}`}}
                            rounded
                            size={60}
                            containerStyle={{
                              borderWidth: 3,
                              borderColor: '#676782',
                              padding: 3,
                              backgroundColor: '#fff',
                            }}
                          />
                        ) : (
                          <Avatar
                            rounded
                            source={require('../../assets/images/download.png')}
                            size={60}
                            containerStyle={{
                              borderWidth: 3,
                              borderColor: '#676782',
                              padding: 3,
                              backgroundColor: '#fff',
                            }}
                          />
                        )}

                        <View style={styles.mediabody}>
                          <Text style={styles.h3}>{item.name}</Text>
                          <View
                            style={[
                              styles.justifyrow,
                              {marginTop: 5, flexWrap: 'wrap'},
                            ]}>
                            <View style={styles.justifyrow}>
                              <Image
                                source={require('../../assets/images/envalop.png')}
                                resizeMode="contain"
                                style={{width: 14, marginRight: 5}}
                              />
                              <Text style={styles.mutetext2}>{item.email}</Text>
                            </View>
                            <View style={styles.justifyrow}>
                              <Image
                                source={require('../../assets/images/call.png')}
                                resizeMode="contain"
                                style={{width: 14, marginRight: 5}}
                              />
                              <Text style={styles.mutetext2}>{item.phone}</Text>
                            </View>
                          </View>

                          <View style={styles.justifycontent}>
                            <TouchableOpacity
                              onPress={() =>
                                this.props.navigation.navigate('ViewMasseuse', {
                                  masseuse_id: item.id,
                                })
                              }
                              style={[
                                styles.smbttn,
                                {backgroundColor: '#ffffff'},
                              ]}>
                              <Text style={[styles.smtext, {color: '#e25cff'}]}>
                                View
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              onPress={() =>
                                this.props.navigation.navigate('editMasseuse', {
                                  masseuse_id: item.id,
                                })
                              }
                              style={styles.smbttn}>
                              <Text style={styles.smtext}>Edit</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[
                                styles.smbttn,
                                {backgroundColor: '#e25cff'},
                              ]}
                              onPress={this.deleteMassuse.bind(this, item.id)}>
                              <Text style={styles.smtext}>Delete</Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    </View>
                  );
                })}
              </View>
            ) : (
              <View style={styles.justifyrow}>
                <Text style={styles.backheading}>No Masseuse Found</Text>
              </View>
            )}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner,
});

export default connect(mapStateToProps, {fetchMassause, deleteMassuse})(
  MasseuseList,
);
