import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  Alert,
  Picker,
  KeyboardAvoidingView,
  AsyncStorage
} from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import { Avatar, Icon, CheckBox, Input } from "react-native-elements";
import styles from "./style";
import { spaOwnerUpdateService } from "./redux/SpaOwnerAction";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";

import { connect } from "react-redux";
import { returnStatement } from "@babel/types";
import { array } from "prop-types";

class service extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      spa_owner_id: "",
      visible1: false,
      popupMsg: "",
      newDuration: "00"
    };
    console.log("constructor", this.props);
  }

  //--FETCHING SERVICES LISTING USING REDUX--//
  async componentDidMount() {
    console.log(
      "componentDidMount in service page---------------------------------------",
      this.props.selectedService
    );

    try {
      const value = await AsyncStorage.getItem("login_id");
      console.log("loginId", value);
      if (value) {
        this.setState({ spa_owner_id: value });
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }

    //this.props.setSelectedServiceArray();
    console.log("selectedService------", typeof this.props.selectedService);
  }

  static getDerivedStateFromProps(props, state) {
    console.log("getDerivedStateFromProps");

    if (props.spaTimingArrayDetails) {
      return {
        service_array: props.selectedServiceDetails
      };
    }
    return null;
  }

  componentDidUpdate(prevProps) {
    console.log("componentDidUpdate", this.props);
    if (
      prevProps.spaOwnerUpdateServices !== this.props.spaOwnerUpdateServices
    ) {
      this.setState({ spinner: false });
      const addDetails = this.props.spaOwnerUpdateServices;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({ visible1: true, popupMsg: addDetails.msg });
        }
        //this.setState({spinner: false});
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg
            });
          }
        } else {
          // this.setState({
          //   spinner: false,
          // });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: "Something went wrong. Please try again later."
            });
          }
        }
      }
    }
  }

  closePopupbox = () => {
    this.setState({
      visible1: false
    });
    console.log("status", this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate("spaAccount");
    }
  };

  //---Api call for updating the database through redux----//
  submit = () => {
    console.log("submit state array", this.state.service_array);

    var finalArray = this.state.service_array;

    let userDetails = new FormData();
    userDetails.append("spa_owner_id", this.state.spa_owner_id);
    userDetails.append("service_array", JSON.stringify(finalArray));
    this.setState({ spinner: true });
    this.props.spaOwnerUpdateService(userDetails);
    console.log("final array2", userDetails);
  };

  //----setstate the changed duration value----//
  durationChange = value => {
    console.log("duration1 in function", this.state.key);
    console.log("duration2 in function", value);
    this.setState({ newDuration: value });
  };

  //----setstate the changed price of the service----//
  priceChange = value => {
    console.log("price2 in function", value);
    console.log("key", this.state.key);
    this.setState({ newPrice: value });
  };

  //----updating the constructing the service array bt inserting the new service duration and price----//
  price_duration = () => {
    this.setState({ visible2: false });
    console.log("price", this.state.newPrice);
    console.log("duration", this.state.newDuration);
    console.log("array_index", this.state.key);

    console.log("array_service", this.state.service_array);

    var arrayService = this.state.service_array;

    if (arrayService[this.state.key].is_checked == 1) {
      var priceDuration = this.state.service_array[this.state.key]
        .price_duration;

      priceDuration.push({
        price: this.state.newPrice,
        duration: this.state.newDuration
      });
      arrayService[this.state.key].price_duration = priceDuration;
    } else {
      arrayService[this.state.key].is_checked = 1;
      var newPriceDuration = [];
      newPriceDuration.push({
        price: this.state.newPrice,
        duration: this.state.newDuration
      });
      arrayService[this.state.key].price_duration = newPriceDuration;
    }

    arrayService[this.state.key].is_checked = 1;

    this.setState({ service_array: arrayService });

    console.log("updated array services", this.state.service_array);
  };

  //----show popup for inserting new service duration and price----//
  addSlot = key => {
    this.setState({
      visible2: true,
      servicename: this.state.service_array[key].name
    });
    console.log("services array", this.state.service_array);

    console.log("iddddddddd", key);

    this.setState({ key: key, newPrice: "00", newDuration: "00" });
  };

  confirmDleteSlot = (key, key1) => {
    this.setState({ visible3: true, priceKey: key, priceDurationKey: key1 });
  };

  //----Delete any service slot----//
  deleteSlot = () => {
    console.log("services array", this.state.service_array);

    console.log("iddddddddd", this.state.priceKey);
    console.log("idddddddddkey1", this.state.priceDurationKey);

    this.setState({ visible3: false });

    var arrayService = this.state.service_array;

    var priceDuration = this.state.service_array[this.state.priceKey]
      .price_duration;
    console.log(priceDuration);

    priceDuration.splice(this.state.priceDurationKey, 1);
    arrayService[this.state.priceKey].price_duration = priceDuration;

    if (arrayService[this.state.priceKey].price_duration.length == 0) {
      arrayService[this.state.priceKey].is_checked = 0;
    }

    this.setState({ service_array: arrayService });

    console.log("arrayService", arrayService);

    console.log(
      "length array service",
      arrayService[this.state.priceKey].price_duration.length
    );

    console.log("updated array services", this.state.service_array);
  };

  closePopupbox2 = () => {
    this.setState({ visible2: false });
  };
  closePopupbox3 = () => {
    this.setState({ visible3: false });
  };

  render() {
    console.log("props in render", this.props);
    console.log("props in render", this.props.selectedServiceDetails);

    console.log(
      "state value in render-------",
      Object.values(this.state.service_array).length
    );

    const { goBack } = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>All Services</Text>
          </View>
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={styles.spinnerTextStyle}
        />
        <Dialog
          visible={this.state.visible1}
          dialogAnimation={
            new SlideAnimation({
              slideFrom: "bottom"
            })
          }
          onTouchOutside={() => {
            this.closePopupbox();
          }}
          dialogStyle={{ width: "80%" }}
          footer={
            <DialogFooter>
              <DialogButton
                textStyle={{
                  fontSize: 14,
                  color: "#333",
                  fontWeight: "700"
                }}
                text="OK"
                onPress={() => {
                  this.closePopupbox();
                }}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <Text style={styles.popupText}>{this.state.popupMsg}</Text>
          </DialogContent>
        </Dialog>

        <Dialog
          visible={this.state.visible2}
          dialogAnimation={
            new SlideAnimation({
              slideFrom: "bottom"
            })
          }
          onTouchOutside={() => {
            this.closePopupbox2();
          }}
          dialogStyle={{ width: "90%" }}
          footer={
            <DialogFooter>
              <DialogButton
                textStyle={{
                  fontSize: 14,
                  color: "#333",
                  fontWeight: "700"
                }}
                text="DONE"
                onPress={() => {
                  this.price_duration();
                }}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            {/* <Text style={styles.popupText}>{this.state.popupMsg}</Text> */}
            <View>
              {/* <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingTop: 20,
                }}>
                <Text
                  style={{color: '#e25cff', fontWeight: 'bold', fontSize: 20}}>
                  {this.state.servicename}
                </Text>
              </View> */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 20
                }}
              >
                <Text
                  style={{ fontSize: 15, fontWeight: "bold", paddingLeft: 20 }}
                >
                  Enter Price
                </Text>
                <Text
                  style={{ fontSize: 15, fontWeight: "bold", paddingRight: 20 }}
                >
                  Enter Duration
                </Text>
              </View>

              <View style={styles.justifycontent}>
                <View style={[styles.justifyrow, { width: "48%" }]}>
                  <Text style={styles.pinktext}>$</Text>
                  <Input
                    placeholder="00"
                    placeholderTextColor="black"
                    keyboardType={"numeric"}
                    defaultValue={this.state.newPrice}
                    containerStyle={{ width: "100%" }}
                    onChangeText={this.priceChange.bind(this)}
                  />
                </View>

                <View
                  style={[
                    styles.justifyrow,
                    {
                      width: "40%"
                      //borderBottomColor: '#4f4f5c',
                      //borderBottomWidth: 1,
                      //height: '-80%',
                      //backgroundColor: 'black'
                    }
                  ]}
                >
                  <Picker
                    selectedValue={this.state.newDuration}
                    style={{
                      height: 50,
                      width: "80%",
                      color: "black",
                      height: "-60%",
                      //backgroundColor: 'red',
                      marginBottom: -60,
                      marginTop: -60
                    }}
                    onValueChange={this.durationChange.bind(this)}
                  >
                    <Picker.Item color="black" label="00" value="00" />
                    <Picker.Item color="black" label="30" value="30" />
                    <Picker.Item color="black" label="35" value="35" />
                    <Picker.Item color="black" label="40" value="40" />
                    <Picker.Item color="black" label="45" value="45" />
                    <Picker.Item color="black" label="50" value="50" />
                    <Picker.Item color="black" label="55" value="55" />
                    <Picker.Item color="black" label="60" value="60" />
                    <Picker.Item color="black" label="65" value="65" />
                    <Picker.Item color="black" label="70" value="70" />
                    <Picker.Item color="black" label="75" value="75" />
                    <Picker.Item color="black" label="80" value="80" />
                    <Picker.Item color="black" label="85" value="85" />
                    <Picker.Item color="black" label="90" value="90" />
                  </Picker>
                  <Text style={styles.minutetext}>min</Text>
                </View>
              </View>
            </View>
          </DialogContent>
        </Dialog>

        <Dialog
          visible={this.state.visible3}
          dialogAnimation={
            new SlideAnimation({
              slideFrom: "bottom"
            })
          }
          onTouchOutside={() => {
            this.closePopupbox3();
          }}
          dialogStyle={{ width: "80%" }}
          footer={
            <DialogFooter>
              <DialogButton
                textStyle={{
                  fontSize: 14,
                  color: "#333",
                  fontWeight: "700"
                }}
                text="YES"
                onPress={() => {
                  this.deleteSlot();
                }}
              />
              <DialogButton
                textStyle={{
                  fontSize: 14,
                  color: "#333",
                  fontWeight: "700"
                }}
                text="NO"
                onPress={() => {
                  this.closePopupbox3();
                }}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <Text style={styles.popupText}>
              Are you sure that you what to delete the slot
            </Text>
          </DialogContent>
        </Dialog>
        <ScrollView>
          {Object.values(this.state.service_array).length ? (
            <View style={[styles.graycontainer, { paddingBottom: 80 }]}>
              {Object.values(this.state.service_array).map((item, key) => {
                console.log("1st", item.name);

                return (
                  <View style={styles.graycard}>
                    {item.is_checked == 1 ? (
                      <View style={styles.graycard}>
                        <View
                          style={[
                            styles.justifyrow,
                            {
                              marginBottom: 15,
                              justifyContent: "space-between"
                            }
                          ]}
                        >
                          <CheckBox
                            title={item.name}
                            uncheckedIcon="check-square"
                            checkedIcon="check-square"
                            containerStyle={styles.chkcontainer}
                            textStyle={styles.h22}
                            checkedColor="#e25cff"
                            uncheckedColor="#e25cff"
                          />
                          {/* <TouchableOpacity
                            style={[
                              styles.smbttn,
                              { backgroundColor: "#e25cff", width: 55 }
                            ]}
                            onPress={this.addSlot.bind(this, key)}
                          >
                            <Text style={styles.smtext}>Add</Text>
                          </TouchableOpacity> */}
                        </View>
                        {Object.values(item.price_duration).length ? (
                          <View style={styles.flexwrap}>
                            {Object.values(item.price_duration).map(
                              (item1, key1) => {
                                console.log("itwm", item1);
                                return (
                                  <View
                                    style={{
                                      flexDirection: "column",
                                      width: "100%"
                                    }}
                                  >
                                    <View
                                      style={{
                                        width: "100%",
                                        flexDirection: "row",
                                        justifyContent: "space-between"
                                      }}
                                    >
                                      <View
                                        style={{
                                          flexDirection: "row",
                                          marginTop: 20
                                        }}
                                      >
                                        <Text
                                          style={{
                                            color: "#EE82EE",
                                            fontSize: 20,
                                            paddingRight: 10
                                          }}
                                        >
                                          $
                                        </Text>

                                        <Text
                                          style={{
                                            color: "#ffff",
                                            fontSize: 20
                                          }}
                                        >
                                          {item1.price}
                                        </Text>
                                      </View>
                                      <View
                                        style={{
                                          flexDirection: "row",
                                          marginTop: 20
                                        }}
                                      >
                                        <Text
                                          style={{
                                            color: "#ffff",
                                            fontSize: 20,
                                            paddingRight: 10
                                          }}
                                        >
                                          {item1.duration}
                                        </Text>
                                        <Text
                                          style={{
                                            color: "#EE82EE",
                                            fontSize: 20
                                          }}
                                        >
                                          min
                                        </Text>
                                        <TouchableOpacity
                                          style={{ paddingLeft: 20 }}
                                          onPress={this.confirmDleteSlot.bind(
                                            this,
                                            key,
                                            key1
                                          )}
                                        >
                                          <Image
                                            source={require("../../assets/images/cross.png")}
                                            resizeMode="contain"
                                            style={{
                                              height: 15,
                                              width: 15,
                                              marginTop: 10
                                            }}
                                          />
                                        </TouchableOpacity>
                                      </View>
                                    </View>
                                  </View>
                                );
                              }
                            )}
                          </View>
                        ) : null}
                        <View style={{justifyContent :'center', alignItems:'center', marginTop: 20}}>
                        <TouchableOpacity
                          style={[
                            styles.smbttn,
                            { backgroundColor: "#e25cff", width: 62 }
                          ]}
                          onPress={this.addSlot.bind(this, key)}
                        >
                          <Text style={styles.smtext}>Add</Text>
                        </TouchableOpacity>
                        </View>
                      </View>
                    ) : (
                      <View
                        style={{
                       
                          
                            marginBottom: 15,
                            flexDirection: "column",
                          }
                        }
                      >
                        <CheckBox
                          title={item.name}
                          containerStyle={styles.chkcontainer}
                          textStyle={styles.h22}
                          checkedColor="#e25cff"
                          uncheckedColor="#e25cff"
                        />

                        <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
                          <TouchableOpacity
                            style={[
                              styles.smbttn,
                              { backgroundColor: "#e25cff",  width: 62  }
                            ]}
                            onPress={this.addSlot.bind(this, key)}
                          >
                            <Text style={styles.smtext}>Add</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    )}
                  </View>
                );
              })}
              <TouchableOpacity
                style={styles.pinkbtn}
                onPress={() => this.submit()}
              >
                <Text style={styles.btntext}>Submit</Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner
});
export default connect(mapStateToProps, {
  spaOwnerUpdateService
})(service);
