import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert
} from "react-native";
import { Avatar, Icon, CheckBox } from "react-native-elements";
import styles from "./style";
import { connect } from "react-redux";
import Spinner from "react-native-loading-spinner-overlay";
import { fetchTransactionHistory } from "./redux/SpaOwnerAction";

class Transection extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      spa_owner_id: "",
      spinner: true
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps", nextProps);
    this.setState({ spinner: false });
  }

  //--FETCH TRANSACTION HOSTORY THROUGH REDUX--//
  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem("login_id");
      console.log("loginId in myaccount page", value);
      if (value) {
        this.setState({ spa_owner_id: value });
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
    let userDetails = new FormData();
    userDetails.append("spa_id", this.props.navigation.state.params.spa_id);
    console.log("userDetails", userDetails);

    this.props.fetchTransactionHistory(userDetails);
  }

  render() {
    //console.log('props in render in transaction page', this.props);
    console.log(
      "props in render in transaction page",
      this.props.transactionHistory
    );
    const transaction = this.props.transactionHistory;
    console.log("constant transaction", transaction);

    //const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("spaAccount")}
            >
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Transaction History</Text>
          </View>
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={styles.spinnerTextStyle}
        />
        <ScrollView>
          {this.props.error && this.props.error !== undefined ? (
            Alert.alert("Something went wrong")
          ) : (
            <View style={styles.graybg}>
              {transaction.length ? (
                transaction.map((item, key) => (
                  <View key={key} style={styles.lightgry}>
                    <Text
                      style={[
                        styles.h3,
                        { color: "#9c9cac", marginBottom: 10 }
                      ]}
                    >
                      {item.date}
                    </Text>
                    <View style={styles.roundcontainer}>
                      <View style={styles.grbody}>
                        {item.payment_method == "Cash" ? null : (
                          <View
                            style={[
                              styles.justifyrow,
                              { alignItems: "flex-start", paddingRight: 40 }
                            ]}
                          >
                            <Text style={styles.mutetexto}>
                              Transaction Id :
                            </Text>
                            <Text style={styles.whitetexto}>
                              {item.stripe_tranfer_id}
                            </Text>
                          </View>
                        )}

                        <View
                          style={[
                            styles.justifyrow,
                            { alignItems: "flex-start" }
                          ]}
                        >
                          <Text style={styles.mutetexto}>Booking Id : </Text>
                          <Text style={styles.whitetexto}>
                            {item.booking_id}
                          </Text>
                        </View>
                        <View
                          style={[
                            styles.justifyrow,
                            { alignItems: "flex-start" }
                          ]}
                        >
                          <Text style={styles.mutetexto}>Customer Id : </Text>
                          <Text style={styles.whitetexto}>
                            {item.customer_id}
                          </Text>
                        </View>
                        <View
                          style={[
                            styles.justifyrow,
                            { alignItems: "flex-start" }
                          ]}
                        >
                          <Text style={styles.mutetexto}> Amount : </Text>
                          <Text style={styles.whitetexto}>
                            {item.total_amount}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                ))
              ) : (
                <View style={styles.justifyrow}>
                  <Text style={styles.backheading}>
                    No Transaction History Found
                  </Text>
                </View>
              )}
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner
});
export default connect(mapStateToProps, { fetchTransactionHistory })(
  Transection
);
