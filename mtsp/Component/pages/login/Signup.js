import React, {Component} from 'react';
import {
  Text,
  View,
  Button,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  TextInput,
  Alert,
} from 'react-native';
import styles from './style';
import {signUp} from './redux/authAction';
import {connect} from 'react-redux';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import _ from 'lodash';
import Spinner from 'react-native-loading-spinner-overlay';

class Signup extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      buttonDisabled: true,
      name: '',
      spaName_error: '',
      first_name: '',
      firstName_error: '',
      last_name: '',
      lastName_error: '',
      password: '',
      password_error: '',
      email: '',
      email_error: '',
      //visible: false,
      visible1: false,
      popupMsg: '',
      spinner: false,
      pin: ''
    };
    console.log('constructor');
  }

  // handleCancel = () => {
  //   this.setState({visible: false});
  //   this.props.navigation.navigate('Signup');
  // };

  // handleConfirm = () => {
  //   this.setState({visible: false});
  //   this.props.navigation.navigate('Login');
  // };

  validate = (text, type) => {
    if (type === 'name') {
      this.setState({name: text});
      this.forceUpdate();
      var regex = /^[^\W0-9_][a-zA-Z0-9\s]+$/;
      if (regex.test(this.state.name)) {
        this.setState({spaName_error: ''});
        this.setState({buttonDisabled: false});
        return true;
      } else {
        this.setState({spaName_error: 'special characters are not allowed.'});
        this.setState({buttonDisabled: true});
      }
    } else if (type === 'first_name') {
      this.setState({first_name: text});
      this.forceUpdate();
      var regex = /^[a-zA-Z ]{2,30}$/;
      if (regex.test(this.state.first_name)) {
        this.setState({firstName_error: ''});
        this.setState({buttonDisabled: false});
        return true;
      } else {
        this.setState({firstName_error: 'special characters are not allowed.'});
        this.setState({buttonDisabled: true});
      }
    } else if (type == 'last_name') {
      this.setState({last_name: text});
      var regex = /^[a-zA-Z ]{2,30}$/;
      if (regex.test(this.state.last_name)) {
        this.setState({lastName_error: ''});
        this.setState({buttonDisabled: false});
        return true;
      } else {
        this.setState({lastName_error: 'special characters are not allowed.'});
        this.setState({buttonDisabled: true});
      }
    } else if (type == 'email') {
      this.setState({email: text});
      var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (regex.test(this.state.email)) {
        this.setState({email_error: ''});
        this.setState({buttonDisabled: false});
        return true;
      } else {
        this.setState({
          email_error: 'You should provide proper email address.',
        });
        this.setState({buttonDisabled: true});
      }
    } else if (type == 'password') {
      this.setState({
        password_error: '',
        buttonDisabled: false,
        password: text,
      });
    }
  };

  submit = () => {
    console.log('submit');

    if (this.state.name === '') {
      this.setState({
        spaName_error: 'Enter Spa Name',
        buttonDisabled: true,
      });
    }
    if (this.state.email === '') {
      this.setState({
        email_error: 'Enter email id',
        buttonDisabled: true,
      });
    }
    if (this.state.first_name === '') {
      this.setState({
        firstName_error: 'Enter First Name',
        buttonDisabled: true,
      });
    }
    if (this.state.last_name === '') {
      this.setState({
        lastName_error: 'Enter Last Name',
        buttonDisabled: true,
      });
    }
    if (this.state.password === '') {
      this.setState({
        password_error: 'Enter Password',
        buttonDisabled: true,
      });
    }
    if (this.state.password !== '') {
      this.setState({
        buttonDisabled: false,
      });
    }
    if (
      this.state.name !== '' &&
      this.state.email !== '' &&
      this.state.first_name !== '' &&
      this.state.last_name !== '' &&
      this.state.password !== ''
    ) {
      let userDetails = new FormData();
      userDetails.append('name', this.state.name);
      userDetails.append('email', this.state.email);
      userDetails.append('first_name', this.state.first_name);
      userDetails.append('last_name', this.state.last_name);
      userDetails.append('password', this.state.password);
      userDetails.append('user_type', 'SO');

      console.log('userDetails', userDetails);

      if (this.props.signUp(userDetails)) {
        this.setState({
          spinner: true,
        });
      }
    }
  };

  componentDidUpdate(prevProps) {

    if (this.state.pin == 1) {
      this.props.navigation.navigate('Login');
    }

    if (prevProps.signUpDetails !== this.props.signUpDetails) {
      console.log('if1');

      const addDetails = this.props.signUpDetails;
      if (this.props.error && this.props.error !== undefined) {
        console.log('helolo here');
        if (this.state.visible1 === false) {
          this.setState({spinner: false});
          this.setState({visible1: true, popupMsg: addDetails.msg});
        }
        this.setState({spinner: false});
      } else {
        console.log('if ack');

        if (addDetails.ack === 1) {
          console.log('ifack');

          this.setState({spinner: false});
          
          if (this.state.visible1 === false) {
            console.log('msg');

            this.setState({
              visible1: true,
              popupMsg: addDetails.msg,
            });
          }
        } else {
          console.log('else');
          this.setState({
            spinner: false,
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: 'Something went wrong. Please try again later.',
            });
          }
        }
      }
    }
  }

  closePopupbox = () => {
    console.log('closePopupbox');

    this.setState({
      visible1: false,
      pin: 1
    });

    // if (this.state.status == 1) {
    //   this.props.navigation.navigate('Login');
    // }
  };

  render() {
    console.log('render', this.props);

    return (
      <SafeAreaView>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <ImageBackground
          source={require('../../assets/images/mainbg.jpg')}
          style={styles.bodymain}>
          <KeyboardAvoidingView enabled behavior="padding">
            <ScrollView>
              <View style={styles.container}>
                <TouchableOpacity style={styles.logo}>
                  <Image
                    source={require('../../assets/images/logo.png')}
                    resizeMode="contain"
                    style={{width: 280, height: 80}}
                  />
                </TouchableOpacity>
                <Spinner
                  visible={this.state.spinner}
                  textContent={'Loading...'}
                  textStyle={styles.spinnerTextStyle}
                />

                <View style={styles.mainform}>
                  <Text style={styles.heading}>Sign Up as a Spa Owner</Text>

                  <View style={styles.container}>
                    {/* <Dialog
                      visible={this.state.visible}
                      onTouchOutside={() => {
                        this.setState({visible: true});
                      }}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            text="Cancel"
                            onPress={this.handleCancel}
                          />
                          <DialogButton
                            text="OK"
                            onPress={this.handleConfirm}
                          />
                        </DialogFooter>
                      }>
                      <DialogContent>
                        <Text style={styles.dialogText}>{this.state.msg}</Text>
                      </DialogContent>
                    </Dialog> */}

                    {/* .<Dialog
                      visible={this.state.visible1}
                      onTouchOutside={() => {
                        this.setState({visible1: false});
                      }}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: '#333',
                              fontWeight: '700',
                            }}
                            text="DISMISS"
                            onPress={() => {
                              this.setState({visible1: false});
                            }}
                          />
                        </DialogFooter>
                      }>
                      <DialogContent>
                        <Text style={styles.dialogText}>{this.state.msg}</Text>
                      </DialogContent>
                    </Dialog> */}
                    <Dialog
                      visible={this.state.visible1}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: 'bottom',
                        })
                      }
                      onTouchOutside={() => {
                        this.closePopupbox();
                      }}
                      dialogStyle={{width: '80%'}}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: '#333',
                              fontWeight: '700',
                            }}
                            text="OK"
                            onPress={() => {
                              this.closePopupbox();
                            }}
                          />
                        </DialogFooter>
                      }>
                      <DialogContent>
                        <Text style={styles.popupText}>
                          {this.state.popupMsg}
                        </Text>
                      </DialogContent>
                    </Dialog>
                  </View>

                  <TextInput
                    style={styles.forminput}
                    name="name"
                    placeholder="Spa Name"
                    placeholderTextColor="#9590a9"
                    onChangeText={text => this.validate(text, 'name')}
                  />
                  <Text style={styles.error}>{this.state.spaName_error}</Text>
                  <TextInput
                    style={styles.forminput}
                    name="first_name"
                    placeholder="Spa Owner First Name"
                    placeholderTextColor="#9590a9"
                    onChangeText={text => this.validate(text, 'first_name')}
                  />
                  <Text style={styles.error}>{this.state.firstName_error}</Text>
                  <TextInput
                    style={styles.forminput}
                    name="last_name"
                    placeholder="Spa Owner Last Name"
                    placeholderTextColor="#9590a9"
                    onChangeText={text => this.validate(text, 'last_name')}
                  />
                  <Text style={styles.error}>{this.state.lastName_error}</Text>
                  <TextInput
                    style={styles.forminput}
                    name="email"
                    placeholder="Email"
                    placeholderTextColor="#9590a9"
                    onChangeText={text => this.validate(text, 'email')}
                  />
                  <Text style={styles.error}>{this.state.email_error}</Text>
                  <TextInput
                    style={styles.forminput}
                    name="password"
                    placeholder="Password"
                    placeholderTextColor="#9590a9"
                    onChangeText={text => this.validate(text, 'password')}
                    secureTextEntry
                  />
                  <Text style={styles.error}>{this.state.password_error}</Text>

                  <Button
                    style={[
                      {paddingBottom: 30},
                      {marginBottom: 30},
                      {marginTop: 30},
                    ]}
                    onPress={this.submit}
                    disabled={this.state.buttonDisabled}
                    title="SIGN UP"
                  />

                  {/* <TouchableOpacity style={styles.pinkbtn} onPress={this.submit} disabled={this.state.buttonDisabled}>
                                        <Text style={styles.btntext}                                        
                                        >SIGN Up</Text>
                                    </TouchableOpacity> */}

                  <View style={styles.bttext}>
                    <Text style={styles.text}>Already a Spa Owner?</Text>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('Login')}>
                      <Text style={styles.pinktext}>Login</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.auth,
});

export default connect(mapStateToProps, {signUp})(Signup);
