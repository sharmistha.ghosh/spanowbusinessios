import React, { Component } from 'react'
import { Text, View, StatusBar, SafeAreaView, TouchableOpacity, ScrollView, ImageBackground, Image, KeyboardAvoidingView } from 'react-native';
import { Avatar, Icon, } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import styles from './style'

export default class myAccount extends Component {
    static navigationOptions = {
        header: null
    }    
    render() {
        const { goBack } = this.props.navigation;
        return (
            <SafeAreaView style={styles.graybody}>
                <StatusBar barStyle="light-content" backgroundColor="transparent" />
                <KeyboardAvoidingView enabled behavior="height">
                <View style={[styles.topbar]}>
                    <View style={styles.justifyrow}>
                        <TouchableOpacity onPress={() => goBack()} >
                            <Icon name="md-arrow-back" type='ionicon' color="#fff" />
                        </TouchableOpacity>
                        <Text style={styles.backheading}>Edit Profile</Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={{position:'relative', paddingBottom:40}}>
                        <ImageBackground source={require('../../assets/images/probg.png')} style={styles.etprofile}></ImageBackground>
                        <View style={styles.graycontainer}>
                            <View style={[styles.graycard, {position:'relative',top:-70, marginBottom:-50}]}>                                                                  
                                    <View style={[styles.media, { marginBottom: 0,}]}>
                                        <Avatar rounded source={require('../../assets/images/avt3.png')} size={70} containerStyle={{ borderWidth: 3, borderColor: '#676782', padding: 4, backgroundColor:'#fff' }} />
                                        <View style={[styles.mediabody, {width:'50%'}]}>
                                            <Text style={[styles.h3, {marginTop:10}]}>John Doe</Text> 
                                            <View style={styles.justifyrow}>
                                                <View style={styles.justifyrow}>
                                                    <Image source={require('../../assets/images/marker.png')} resizeMode="contain" style={{ width: 16, marginRight: 5 }} />
                                                    <Text style={styles.mutetext}>EC1V 2NX</Text>
                                                </View>
                                                <View style={[styles.justifyrow, {marginLeft:20} ]}>
                                                    <Image source={require('../../assets/images/brif.png')} resizeMode="contain" style={{ width: 16, marginRight: 5 }} />
                                                    <Text style={styles.mutetext}>Web / Graphics Designer</Text>
                                                </View>
                                            </View>                                          
                                        </View>
                                    </View>
                                    <TouchableOpacity style={styles.editbtn}>
                                        <Icon name="md-create" type='ionicon' color="#e25cff" />
                                    </TouchableOpacity>
                            </View>
                            <View style={styles.graycard}>
                                <TextField  label='Name' defaultValue={"hhhh"}   textColor={'#8f8f92'} baseColor={'#fff'} labelFontSize={18} activeLineWidth={1}  />
                                <TextField  label='Email'   textColor={'#8f8f92'} baseColor={'#fff'} labelFontSize={18} activeLineWidth={1}  />
                                <TextField  label='Gender'  textColor={'#8f8f92'} baseColor={'#fff'} labelFontSize={18} activeLineWidth={1}  />
                                <TextField  label='Phone'   textColor={'#8f8f92'} baseColor={'#fff'} labelFontSize={18} activeLineWidth={1}  />
                               
                            </View>

                            <TouchableOpacity style={[styles.pinkbtn, {marginTop:30}]} onPress={() => this.props.navigation.navigate('')}>
                                <Text style={styles.btntext}>Submit</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}
