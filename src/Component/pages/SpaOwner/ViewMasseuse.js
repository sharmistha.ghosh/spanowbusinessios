import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
} from 'react-native';
import {Avatar, Icon, ListItem} from 'react-native-elements';
import {TextField} from 'react-native-material-textfield';
import RadioForm from 'react-native-simple-radio-button';
import styles from './style';
import {connect} from 'react-redux';
import {masseuseDetails} from './redux/SpaOwnerAction';
import Spinner from 'react-native-loading-spinner-overlay';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';

class ViewMasseuse extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      first_name: '',
      last_name: '',
      email: '',
      phone: '',

      profile_image: '',

      masseuse_id: '',
      image_source: '',
      
    };
  }
  componentDidMount() {
    const masseuseId = this.props.navigation.getParam('masseuse_id');
    console.log('masseuse_id', masseuseId);
    let userDetails = new FormData();
    userDetails.append('user_id', masseuseId);
    this.props.masseuseDetails(userDetails);
    console.log('props in componentdidmount', this.props.masseusedetails);
    this.setState({
      masseuse_id: masseuseId,
      first_name: this.props.masseusedetails.first_name,
      last_name: this.props.masseusedetails.last_name,
      email: this.props.masseusedetails.email,
      phone: this.props.masseusedetails.phone,
      image_source: this.props.masseusedetails.profile_image,
    });
  }

  

  render() {
    const {goBack} = this.props.navigation;
    var logo;

    {
      this.state.image_source
        ? (logo = this.state.image_source)
        : (logo = this.props.masseusedetails.profile_image);
    }
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Masseuse</Text>
          </View>
        </View>

        <ScrollView>
          <View style={{flex: 1, paddingBottom: 60}}>
            <ImageBackground
              source={require('../../assets/images/pbg.png')}
              style={styles.tprofile}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingBottom: 70,
                }}>
                <View style={{position: 'relative', top: 25}}>
                  <Text style={[styles.h3, {color: '#fff'}]}>
                    {this.props.masseusedetails.name}
                  </Text>
                </View>

                <View style={styles.custsec}>
                  {logo ? (
                    <Avatar
                      rounded
                      source={{uri: `${logo}`}}
                      size={90}
                      containerStyle={styles.custavtr}
                    />
                  ) : (
                    <Avatar
                      rounded
                      source={require('../../assets/images/download.png')}
                      size={90}
                      containerStyle={styles.custavtr}
                    />
                  )}
                </View>
              </View>
            </ImageBackground>
            <Avatar
              rounded
              source={require('../../assets/images/download.png')}
              size={90}
              containerStyle={styles.custavtr}
            />

           

            <View style={[styles.graycontainer, styles.formgrey]}>
              <View style={styles.graycard}>
                <TextField
                  label="First Name"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  editable={false}
                  defaultValue={this.props.masseusedetails.first_name}
                />

                <TextField
                  label="Last Name"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  editable={false}
                  defaultValue={this.props.masseusedetails.last_name}
                />

                <TextField
                  label="Email"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  editable={false}
                  activeLineWidth={1}
                  defaultValue={this.props.masseusedetails.email}
                  keyboardType={'email-address'}
                />

                <TextField
                  label="Phone"
                  textColor={'#838389'}
                  baseColor={'#838389'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  editable={false}
                  defaultValue={this.props.masseusedetails.phone}
                  keyboardType={'numeric'}
                />
              </View>

              
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.SpaOwner,
});
export default connect(
  mapStateToProps,
  {masseuseDetails},
)(ViewMasseuse);
